var originalLineDraw = Chart.controllers.line.prototype.draw;
Chart.helpers.extend(Chart.controllers.line.prototype, {
  draw: function() {
    originalLineDraw.apply(this, arguments);

    var chart = this.chart;
    var ctx = chart.chart.ctx;

    var index = chart.config.data.lineAtIndex;
    if (index) {
      var xaxis = chart.scales['x-axis-0'];
      var yaxis = chart.scales['y-axis-0'];

      ctx.save();
      ctx.beginPath();
      ctx.moveTo(xaxis.getPixelForValue(undefined, index), yaxis.top);
      ctx.strokeStyle = '#ff0000';
      ctx.lineTo(xaxis.getPixelForValue(undefined, index), yaxis.bottom);
      ctx.stroke();
      ctx.restore();
    }
  }
});

var config = {
  type: 'line',
  data: {
    labels: ["Pzt", "Sal", "Çarş (Minoset)", "Perş", "Cum"],
    datasets: [{
      label: ["Şikayet Sayısı"],
      data: [2, 0, 1, 5, 4, 0, 1],
      fill: false
    }],
    lineAtIndex: 2,
  }
};

var ctx = document.getElementById("myChart").getContext("2d");
new Chart(ctx, config);