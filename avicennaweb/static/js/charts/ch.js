$(document).ready(function(){
var chart = new Chart(ctx, {
   type: 'bar',
   data: {
      labels: ['Ocak', 'Subat', 'Mart', 'Nisan', 'Mayis', 'Haziran', 'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık' ], // responsible for how many bars are gonna show on the chart
      // create 12 datasets, since we have 12 items
      // data[0] = labels[0] (data for first bar - 'Standing costs') | data[1] = labels[1] (data for second bar - 'Running costs')
      // put 0, if there is no data for the particular bar
      datasets: [{
         label: 'Baş ağrısı',
         data: [0, 3, 1],
         backgroundColor: '#22aa99'
      }, {
         label: 'Halsizlik',
         data: [0, 2, 1],
         backgroundColor: '#994499'
      }, {
         label: 'Burun akıntısı',
         data: [0, 1, 3],
         backgroundColor: '#316395'
      }, {
         label: 'Yüksek tansiyon',
         data: [1, 2, 0],
         backgroundColor: '#b82e2e'
      },
      ]
   },
   options: {
      responsive: false,
      legend: {
         position: 'right' // place legend on the right side of chart
      },
      scales: {
         xAxes: [{
            stacked: true // this should be set to make the bars stacked
         }],
         yAxes: [{
            stacked: true // this also..

         }]
      }
   }
});
});