var count = 0
var flag = 0

var selectedDoc = ""
var selectedHour = ""
var selectedDocId = ""

var sex = ""

var errorMessage = ""

function onClickToDoctor(id){

    $("#demo1").hide();
    $("#demo2").hide();
    $("#demo3").hide();
    $("#demo4").hide();
    $("#demo" + id).show();

    selectedDoc = $("#doc" + id).text()
    selectedDocId = id

        //alert($("#select" + id).val())

}

$(document).ready(function () {
  //called when key is pressed in textbox
  $("#tc").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)){
        //display error message
               return false;
    }
    
    if ($("#tc").val().length > 10){
        return false;
    }
   });

  $("#cep").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)){
        //display error message
               return false;
    }
    
    if ($("#cep").val().length > 10){
        return false;
    }
   });

});


$(document).ready(function() {
    $('input[name="optradio"]').change(function(){ 
        var manageradiorel = $('input[name="optradio"]:checked').val();
        sex = manageradiorel
    });
});

$(document).ready(function() {
    
    $("#button").click(function(){
        
        var begin = "#begin" 
        var calendar = "#calendar"
        var doktorlar = "#doctors"
        var form = "#form"
        var title = "#title"
        var selectedDate = "#selectedDate"

        var name = "#name"
        var tc = "#tc"
        var cep = "#cep"
        var posta = "#posta"
        var dogum = "#dogum"
        var hastane = ''
        var servis = ''


        if (count == 0){

            hastane = $("#hastane").val()
            servis = $("#servis").val()

            if (hastane !== 'base' && (servis !== 'sec' || servis !== '')){
                console.log("boi")
                $(begin).hide()
                $("#takvim").show()
                $("#geri").show()
                count += 1
            }
            else{
                alert("Lütfen bir hastane ve servis seçiniz")
            }

            
        }
        else if (count == 1 && $(selectedDate).text() != " ") {
            $("#takvim").hide()
            $(doctors).show()
            $(title).html("Doktor Seçin")
            count += 1
        }
        else if (count == 2) {

            if (selectedDoc != ""){
                $(doctors).hide()
                $(form).show()
                $("#button").html("Randevu Al")
                $(title).html("Randevunuzu Tamamlayın")
                selectedHour = $("#select" + selectedDocId).val()
                count += 1
            }
            else{
                alert("Lütfen bir doktor ve saat seçin")
            }
        }
        else if (count == 3){

            var txtVal =  $('#dogum').val();

            if ($(name).val() != "" && 
                $(tc).val() != "" &&
                $(cep).val() != "" &&
                $(posta).val() != "" &&
                $(dogum).val() != "" &&
                sex != "") {

                if(!isDate(txtVal)){
                    alert("Lütfen doğum tarihinizi doğru formatta girdiğinizden emin olun")
                }else{
                    console.log("bitti")
                }
                
            }

            else{
                alert("Lütfen tüm alanları doldurduğunuzdan emin olun")
            }

        }
        else{
            alert("Lütfen bir tarih seçin")
        }
    
    }); 

    function isDate(txtDate){
    
        var currVal = txtDate;
    
        if(currVal == '')
            return false;
    
        var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
        var dtArray = currVal.match(rxDatePattern); // is format OK?
    
        if (dtArray == null) 
            return false;
    
        //Checks for mm/dd/yyyy format.
        dtMonth = dtArray[3];
        dtDay= dtArray[1];
        dtYear = dtArray[5];        
    
        if (dtMonth < 1 || dtMonth > 12) 
            return false;
        else if (dtDay < 1 || dtDay> 31) 
            return false;
        else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31) 
            return false;
        else if (dtMonth == 2) 
        {
            var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
            if (dtDay> 29 || (dtDay ==29 && !isleap)) 
                return false;
        }
        return true;
    }
});

$(document).ready(function() {

    $("#button2").click(function(){

        var calendar = "#" + "calendar"
        var doktorlar = "#" + "doctors"
        var form = "#" + "form"

        if (count == 3) {
            $(doctors).show()
            $(form).hide()
            $("#button").html("İleri")
            $(title).html("Doktor Seçin")
            count -= 1
        }
        else if (count == 2) {
            $(doctors).hide()
            $("#takvim").show()
            $(title).html("Tarih Seçin")
            count -= 1
        }
        else if(count == 1){
            $("#takvim").hide()
            $(begin).show()
            $("#geri").hide()
            count -= 1
        }
    }); 
});



$(document).ready(function() {

    var array = [
        { name: 'Adana', skills: ["Ağız Diş ve Çene Cerrahisi", "Çocuk Cerrahisi", "Genel Cerrahi", "Kardiyoloji", "Nöroloji"] },
        { name: 'Altunizade', skills: ["Ağız Diş ve Çene Cerrahisi", "Deri Hastalıkları", "Genel Cerrahi", "Meme Kliniği", "Nöroloji"] },
        { name: 'Ankara', skills: ["Ağız Diş ve Çene Cerrahisi", "Çocuk Cerrahisi", "Genel Cerrahi", "Meme Kliniği", "Nöroloji"] },
        { name: 'Atakent', skills: ["Ağız Diş ve Çene Cerrahisi", "Deri Hastalıkları", "Fizik Tedavi ve Rehabilitasyon", "Genel Cerrahi", "Meme Kliniği", "Kardiyoloji"] },
        { name: 'Bakırköy', skills: ["Çocuk Cerrahisi", "Fizik Tedavi ve Rehabilitasyon", "Genel Cerrahi", "Radyoloji"] },
        { name: 'Eskişehir', skills: ["Ağız Diş ve Çene Cerrahisi", "Deri Hastalıkları", "Fizik Tedavi ve Rehabilitasyon", "Genel Cerrahi", "Kardiyoloji", "Radyoloji"] },
        { name: 'Fulya', skills: ["Çocuk Cerrahisi", "Fizik Tedavi ve Rehabilitasyon", "Genel Cerrahi", "Meme Kliniği"] }
    ];


$("#hastane").change(function() {

    var dropdown = $('#hastane').val();

    var option = '';
    var key = dropdown
    var second = []

    console.log(key)

    $("#servis").empty()

    if (key === 'Adana'){
        second = array[0].skills
    }
    else if (key === 'Altunizade'){
        second = array[1].skills
    }
    else if (key === 'Ankara'){
        second = array[2].skills
    }
    else if (key === 'Atakent'){
        second = array[3].skills
    }
    else if (key === 'Bakırköy'){
        second = array[4].skills
    }
    else if (key === 'Eskişehir'){
        second = array[5].skills
    }
    else if (key === 'Fulya'){
        second = array[6].skills
    }

    for(var i = 0; i < second.length; i++){
        option += '<option value="' + second[i] + '">' + second[i] + '</option>';
        }
        $("#servis").append(option);
    
});
});



