# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models
from avicennarest.models import AvicennaUser,SymptomQuestions,SymptomQuestionSet
# Create your models here.

class LeniaNewAdvices(models.Model):

    newadvice_id = models.AutoField(primary_key=True)
    newadvice_title = models.CharField(default="",max_length=30)
    newadvice_content = models.TextField(default="")
    newadvice_imagelink = models.TextField(default="")

    def __str__(self):
        return self.newadvice_title



class LeniaAdvices(models.Model):

    newadvice_id = models.AutoField(primary_key=True)
    newsadice_organ = models.CharField(max_length=50,default="")
    newsadvice_affect = models.CharField(max_length=60,default="")
    newadvice_title = models.CharField(default="",max_length=30)
    newadvice_content = models.TextField(default="")
    newsadvice_title_en = models.CharField(default="",max_length=50)
    newsadvice_content_en = models.TextField(default="")
    newadvice_imagelink = models.TextField(default="")

    def __str__(self):
        return self.newadvice_title + " " + self.newsadice_organ + " " + self.newsadvice_affect

class AutoCompleteAdditional(models.Model):

    autocomplete_additionalid  = models.AutoField(primary_key=True)
    autocomplete_additionaltext = models.TextField(default="")

    def __str__(self):
        return self.autocomplete_additionaltext