# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.views.decorators.csrf import csrf_exempt

from avicennarest.models import AvicennaUser,AvicennaUserComplaints,HealthWikiPage,SymptomQuestions,SymptomAnswerKit,CookieUser,SymptomQuestionImage
from django.shortcuts import render,HttpResponse,get_object_or_404
from avicennarest.models import SymptomQuestionSet
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth import user_logged_in,authenticate,login
from rest_framework.response import Response
from rest_framework.decorators import api_view
import json
import datetime
from django.http import JsonResponse
import requests
from avicennarest import nlp_helper
from avicennarest.facebookmodels import FacebookNumbersCollected,BlockList,FacebookUserModel
from avicennarest.questionanswerserializers import FacebookNumbersCollectedSerializer,FacebookUserModelSerializer
from rest_framework.decorators import api_view
from django.db.models import Count
import psycopg2
from .models import LeniaAdvices

def loadNaturalAdvices(organ,affect):
    pass





def loadMainPage(request):
        return render(request, 'doctorMain.html',
                          {})


def loadMainPageEn(request):
        return render(request, 'mainPageEn.html',
                          {})



def loadTestPage(request):
    return render(request,'testPage.html',{})

@csrf_exempt
def extractSymptomPage(request):
        return render(request,'symptom.html',{})

def loadDatePicker(request):
    return render(request,'datepicker.html',{})

def loadDoctorPage(request):
    return render(request,'doctorpage.html',{})

def handler404(request):
    return render(request,'404.html',{})

def loadFacebookMain(request):
    return render(request,'facebookMain.html',{})
def loadUserPage(request):
    if request.method == "GET":
        username_of_request = request.GET.get( "user")
        try:
            current_user = AvicennaUser.objects.get(username=username_of_request)
            current_user_complaints = AvicennaUserComplaints.objects.filter\
                (complaint_user=current_user)

            return render(request,'userPage.html',{"user":current_user,
                                                   "complaints":current_user_complaints})

        except ObjectDoesNotExist:
            return HttpResponse("kullanici yok")
    return render(request,'index.html',{})

def loadDiagnosisPages(request):
    try:
        first_value = request.GET["1"]
        second_value = request.GET["2"]

        first = HealthWikiPage.objects.get(pk=first_value)
        second = HealthWikiPage.objects.filter(pk=second_value)

        my_list_of_diseases = [first,second]

        return render(request,'diagnosisPage.html',{'first':first,'second':second})



    except:
        return HttpResponse("no result")

    return HttpResponse(first_value + " " + second_value +  " "  + third_value)


def loadHealthWikiPage(request):
    page_no = request.GET["page_no"]
    first = HealthWikiPage.objects.get(pk=page_no)
    extreme_links = first.page_extra_link.all()
    return render(request,'wikiPage.html',{'first':first,'extremes':extreme_links})

def loadNeGelir(request):
    page_no = request.GET["sym_no"]
    first = SymptomQuestionSet.objects.get(pk=page_no)

    advices = LeniaAdvices.objects.filter(newsadice_organ=first.question_set_body_part,
                                       newsadvice_affect=first.question_set_body_affect)

    return render(request,'neiyigelir2.html',{'first':first,'advices':advices})


def loadAppointmentPage(request):
    return render(request,'randevu.html',{})

def loadDashboardPage(request):

    """

    current_question = SymptomQuestions.objects.get(pk=10)
    recordable_values = FacebookQuestionAnswerUserRecorded.objects.filter(answer_question=current_question).all()

    emin_degilim_id = SymptomAnswerKit.objects.get(pk=26)
    basariliyim = SymptomAnswerKit.objects.get(pk=27)
    hic = SymptomAnswerKit.objects.get(pk=28)

    recordable_values_emin = FacebookQuestionAnswerUserRecorded.objects.filter(answer_question=current_question,answer_answer=emin_degilim_id).count()
    recordable_values_basariliyim = FacebookQuestionAnswerUserRecorded.objects.filter(answer_question=current_question,answer_answer=basariliyim).count()
    recordable_values_hic = FacebookQuestionAnswerUserRecorded.objects.filter(answer_question=current_question,answer_answer=hic).count()
    toplam = FacebookQuestionAnswerUserRecorded.objects.all().count()

    recordable_values_emin_erkek = int(recordable_values_emin * 3/4)
    recordable_values_emin_kadin = recordable_values_emin - recordable_values_emin_erkek

    recordable_values_basariliyim_erkek = int(recordable_values_basariliyim * 3/4)
    recordable_values_basariliyim_kadin = recordable_values_basariliyim - recordable_values_basariliyim_erkek

    recordable_values_hic_erkek = int(recordable_values_hic * 3/4)
    recordable_values_hic_kadin = recordable_values_hic - recordable_values_hic_erkek





    return render(request,'dashboard.html',{"question_text":current_question.question_content,"emin_erkek":recordable_values_emin_erkek,
                                                                                                "emin_kadin":recordable_values_emin_kadin,
                                                                                               "basariliyim_erkek":recordable_values_basariliyim_erkek,
                                                                                               "basariliyim_kadin":recordable_values_basariliyim_kadin,
                                                                                               "hic_erkek":recordable_values_hic_erkek,
                                                                                               "hic_kadin":recordable_values_hic_kadin,
                                                                                               "toplam":toplam})
    """
    return render(request,'dashboard.html')


def loadStilop(request):
    response = render(request, 'untitled.html')
    if "user" not in request.COOKIES:
        response.set_cookie('user',datetime.datetime.now())
    return response

def createUserWithCookieIfNotExist(cookie):
    user_count = CookieUser.objects.filter(cookie_text=cookie).count()
    my_json = {}
    my_json["Userid"] = cookie
    my_json["responseTime"] = 0
    my_json["tabId"] = "deneme"
    my_json["responseFollow"] = []
    my_json["productsShown"] = []
    my_json["clickedProducts"] = []
    my_json["isMyBodyShapeRight"] = True
    if user_count<1:
        my_user = CookieUser()
        my_user.cookie_text = cookie
        my_user.cookie_user_set = SymptomQuestionSet.objects.get(pk=120)
        my_user.cookie_user_current_question = SymptomQuestions.objects.get(pk=607)
        my_user.cookie_user_json = json.dumps(my_json)
        my_user.save()
    return CookieUser.objects.get(cookie_text=cookie)


def checkCookieExist(request):
    my_data = json.loads(request.body.decode("utf-8"))

    return JsonResponse({"exist":CookieUser.objects.filter(cookie_text=my_data["cookie"]).count()>0})



def showNLPResult(request):
    my_data = json.loads(request.body.decode("utf-8"))

    return JsonResponse({"result":str(nlp_helper.getRawNLPDNER(my_data["sentence"]))})



def showNERResult(request):

    my_data = json.loads(request.body.decode("utf-8"))


    my_result = nlp_helper.getRawNLPDNER(my_data["sentence"])

    return JsonResponse({"result":str(my_result)})

@csrf_exempt
def cookieUserAnswerQuestion(request):
    #requestten soru idsi ve text cevap
    my_data = json.loads(request.body.decode("utf-8"))




    question_id = my_data["question_id"]
    question_answer_text = my_data["answer"]
    question_answer_cookie = my_data["cookie"]

    my_user = createUserWithCookieIfNotExist(question_answer_cookie)

    current_json = json.loads(my_user.cookie_user_json)


    current_answer = SymptomAnswerKit.objects.get(question_belongs__question_id=question_id,answer_content__icontains=question_answer_text)

    if current_answer.selected_que == None:
        return Response({"status":"bitti"})

    current_json["responseFollow"].append(

        {
            "question_id": question_id,
            "question_text": SymptomQuestions.objects.get(pk=question_id).question_content,
            "answer_id":current_answer.answer_id,
            "answer_content":current_answer.answer_content
        }

    )

    my_user.cookie_user_json = json.dumps(current_json)

    my_user.save()

    result_dict = {}


    next_question = current_answer.selected_que
    new_answers = next_question.answers.all()


    images = SymptomQuestionImage.objects.filter(question_belongs=next_question)
    if images.count()>0:
        result_dict["image"] = images.first().image_link

    result_dict["next_question"] = next_question.question_content
    result_dict["next_question_id"] = next_question.question_id



    result_dict["answers"] = []
    for element in new_answers:
        result_dict["answers"].append(element.answer_content)

    return JsonResponse(result_dict)

    #yeni request soru idsi ve cevaplar listesi

@csrf_exempt
def finishCookiePoint(request):
    my_data = json.loads(request.body.decode("utf-8"))
    chat_time = my_data["chat_time"]
    current_cookie = my_data["cookie"]
    bodyShape = my_data["body_shape"]
    isBodyShapeTrue = my_data["isBodyShapeTrue"]



    current_user = CookieUser.objects.get(cookie_text=current_cookie)
    my_current_json_dict = json.loads(current_user.cookie_user_json)
    if "bodyShape" not in my_current_json_dict:

        my_current_json_dict["responseTime"] = str(chat_time)
        my_current_json_dict["bodyShape"] = bodyShape
        my_current_json_dict["isMyBodyShapeRight"] = True if isBodyShapeTrue == "True" else False
        current_user.cookie_user_json = json.dumps(my_current_json_dict)

        r = requests.post("https://fvy7ien2d7.execute-api.us-east-1.amazonaws.com/stiop-dev/senduserinfo",json=my_current_json_dict)
        current_user.last_response = str(r.json())
        current_user.save()

        current_cookie_dict = {"Userid":current_cookie}
        r = requests.post("https://p5zry9b149.execute-api.us-east-1.amazonaws.com/dev-stilop/getuserproductlist",
                          json=current_cookie_dict)
        return JsonResponse(r.json(), safe=False)

    else:
        current_cookie_dict = {"Userid":current_cookie}
        r = requests.post("https://p5zry9b149.execute-api.us-east-1.amazonaws.com/dev-stilop/getuserproductlist",json=current_cookie_dict)
        current_user.last_response = str(r.json())
        current_user.save()
        return JsonResponse(r.json(), safe=False)

    return JsonResponse(r.json())



@api_view(['GET', 'POST', ])
def loadTelephoneNumber(request):
    password = request.GET.get("pass")
    if password == "lenia2019":
        all_numbers = FacebookNumbersCollected.objects.all().order_by("-number_date")
        my_serializer = FacebookNumbersCollectedSerializer(all_numbers,many=True)
        return Response(my_serializer.data)
    else:
        return Response({"hata":"hata"})



@api_view(['GET', 'POST', ])
def loadExistingSymptoms(request):
    result = SymptomQuestionSet.objects.values("question_set_name").annotate(c=Count('question_set_name')).order_by('-c')
    return HttpResponse(str(result))


@api_view(['GET', 'POST', ])
def applyToUser(request):
    my_data = json.loads(request.body.decode("utf-8"))
    if "pass" in my_data and my_data["pass"] == "lenia2019pasa":
        user_id_blocked = my_data["userid"]

        if "operation" in my_data and my_data["operation"] == "block":
            blocked_user = BlockList()
            blocked_user.blocklist_user = FacebookUserModel.objects.get(conversation_user=user_id_blocked)
            blocked_user.save()
        elif "operation" in my_data and my_data["operation"] == "delete":
            try:
                FacebookUserModel.objects.filter(conversation_user=user_id_blocked).delete()
                return Response({"status":"deleted"})
            except Exception as e:
                return Response({"status":str(e)})
        elif "operation" in my_data and my_data["operation"] == "reset":
            try:
                current_user = FacebookUserModel.objects.get(conversation_user=user_id_blocked)
                current_user.terminateEverything()

            except:
                pass



        return Response({"status":"ok"})
    else:
        return HttpResponse("fail")


@api_view(['GET', 'POST', ])
def listFacebookUsers(request):
    my_data = json.loads(request.body.decode("utf-8"))
    if "pass" in my_data and my_data["pass"] == "lenia2019pasa":
        username = my_data["name"]
        my_users = FacebookUserModel.objects.filter(name__icontains=username).all()
        my_serializer = FacebookUserModelSerializer(my_users,many=True)
        return Response(my_serializer.data)



