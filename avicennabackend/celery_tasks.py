import django
import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'avicennabackend.settings')
django.setup()

from celery import Celery

from avicennarest.facebookmodels import FacebookUserModel
from celery.schedules import crontab
import requests

from datetime import timedelta
from celery import shared_task
from celery.schedules import crontab
from celery.task import periodic_task
from avicennarest.facebookviews import prepareForDailyStructure,sendExtraInfoDaily
from avicennarest.facebookfunctions import endSetsNotResponseTaken
from avicennarest.views import notification_rest
from datetime import datetime
import pytz
import logging






app = Celery('avicennabackend')
app.config_from_object('django.conf:settings')



@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    CELERY_ENABLE_UTC = False
    TIME_ZONE = 'Europe/Madrid'
    CELERY_TIMEZONE = TIME_ZONE

    app.conf.timezone = TIME_ZONE
    sender.add_periodic_task(crontab(hour=15,minute=1) , askAllUsersDailyStatus.s(),
                             name='AskQuestion',
                             )
    #sender.add_periodic_task(crontab(hour=14,minute=1,day_of_week=7),sendNotificationForIOS.s(),name='sendNotification')

    sender.add_periodic_task(crontab(hour=14,minute=1),terminateSets.s(),name='sendNotification')


@app.task
def askAllUsersDailyStatus():
    print("aaa")
    prepareForDailyStructure()

@app.task
def terminateSets():
    endSetsNotResponseTaken()


@app.task
def sendExtra():
    sendExtraInfoDaily()


@app.task()
def sendNotificationForIOS():
    notification_rest.sendNotification()

"""
@app.task()
def sendNoAvaiable(sender_id_int):
    sendNoAnswer(sender_id_int)
"""


'''
app.conf.beat_schedule = {
    'add-every-30-seconds': {
        'task': 'celery_tasks.askAllUsersDailyStatus',
        'schedule': crontab(hour=20,minute=44,nowfun=nowfun),
        'args': ()
    },
}

'''

#askAllUsersDailyStatus()

#app.register_task(askAllUsersDailyStatus)
