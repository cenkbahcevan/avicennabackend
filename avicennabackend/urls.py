"""avicennabackend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from avicennarest import views as arv
from avicennarest import facebookviews as fviews
from avicennarest import androidview
from avicennaweb import views as awb
from rest_framework.authtoken import views
from . import settings
from django.conf.urls.static import static
import debug_toolbar
from django.conf.urls import include, url
from django.conf.urls import (
handler400, handler403, handler404, handler500
)
from django.views.decorators.csrf import csrf_exempt




urlpatterns = [
    url(r'^$',awb.loadMainPage),
    url(r'^en/',awb.loadMainPageEn),
    #url(r'^loadSemptomQuestions/',arv.loadQuestions),
    url(r'^loadTableData/',arv.LoadTableDataOfUser.as_view()),
    url(r'^loadSQuestions/',arv.loadSQuestions),
    url(r'^makeMyDiagnosis/',arv.MakeDiagnosisFromQuestions.as_view()),
    url(r'^datepicker/',awb.loadDatePicker),
    url(r'^semptom/',awb.extractSymptomPage),
    url(r'^sickness/',arv.LoadSicknessList.as_view()),
    url(r'^admin/', admin.site.urls),
    url(r'^login/', views.obtain_auth_token),
    url(r'^register/',arv.RegisterView.as_view()),
    url(r'^facebook/',fviews.FacebookView.as_view()),
    url(r'^userinfo/',arv.LoadUser.as_view()),
    url(r'^loadmedications/',arv.LoadMedicationsList.as_view()),
    url(r'^doctorpage$',awb.loadDoctorPage),
    url(r'^getQuestionSet',arv.LoadQuestionSet.as_view()),
    url(r'^loadAutoCompleteData',arv.LoadAutoCompleteData.as_view()),
    url(r'^loadMissingQuestions',arv.LoadMissingQuestionSet.as_view()),
    url(r'^recordComplaintStatus',arv.RecordComplaintStatus.as_view()),
    url(r'^loadUserPage',awb.loadUserPage),
    url(r'^loadDiagnosisPage',awb.loadDiagnosisPages),
    url(r'^loadWikiPage',awb.loadHealthWikiPage),
    url(r'^testview',arv.TestHealthWikiPage.as_view()),
    url(r'^loadUser',arv.LoadUser.as_view()),
    url(r'^loadHealthAutoComplete',arv.LoadHealthAutoCompelte.as_view()),
    url(r'^loadNews',arv.LoadNews.as_view()),
    url(r'^updateUser',arv.UpdateUser.as_view()),
    url(r'updatePassword',arv.UpdatePassword.as_view()),
    url(r'^updateCategory',arv.UpdateCategories.as_view()),
    url(r'^loadCategories',arv.LoadUserCategories.as_view()),
    url(r'^loadCategoryImage',arv.returnCategoryImage),
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^jet/', include('jet.urls', 'jet')),
    url(r'^makeRedirect',arv.makeRedirect),
    url(r'^loadWriterNews',arv.LoadYazarNews.as_view()),
    url(r'^testView',arv.testView),
    url(r'^loadMedicationDetail',arv.LoadMedicationDetail.as_view()),
    url(r'^randevu$',awb.loadAppointmentPage),
    url(r'^dashboard$',awb.loadDashboardPage),
    url(r'^facebookMain$',awb.loadFacebookMain),
    url(r'^appointmentPost',fviews.FacebookAppointmentRecord.as_view()),
    url(r'^neiyigelir',awb.loadNeGelir),
    url(r'^stilop',awb.loadStilop),
    url(r'recordUUID',arv.RecordUUID.as_view()),
    url(r'nextQuestion',csrf_exempt(awb.cookieUserAnswerQuestion)),
    url(r'sendToEndPoint',csrf_exempt(awb.finishCookiePoint)),
    url(r'testPage',csrf_exempt(awb.loadTestPage)),
    url(r'cookieExist',csrf_exempt(awb.checkCookieExist)),
    url(r'testnlp',csrf_exempt(awb.showNLPResult))  ,
    url(r'loadTel',csrf_exempt(awb.loadTelephoneNumber)),
    url(r'testNer',csrf_exempt(awb.showNERResult)),
    url(r'android',csrf_exempt(androidview.processAndroidUserInput)),
    url(r'__debug__/', include(debug_toolbar.urls)),
    url(r'^existingStat',csrf_exempt(awb.loadExistingSymptoms)),
    url(r'^applyToUser',csrf_exempt(awb.applyToUser)),
    url(r'^listFacebookUsers',csrf_exempt(awb.listFacebookUsers)),
    url(r'^autoneiyigelir',arv.loadAutoCompleteNeIyiGelir.as_view()),
    url(r'product',arv.loadCurrentProduct)
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


handler404 = awb.handler404
