import os
import socket


class InteractionWithNLP:
    def __init__(self,adress,port):
        self.adress = adress
        self.port = port
        self.socket  = socket.socket()
    def startConnection(self):
        self.socket.setsockopt(socket.SOL_SOCKET,socket.SO_KEEPALIVE,1)
        self.socket.connect((self.adress,self.port))
    def sendDataGetResponse(self,sentence):
        correctedVersion = sentence + "\n\n"
        byteVersion  = bytes(correctedVersion,"utf-8")
        self.socket.send(byteVersion)
        answer = self.socket.recv(2048)
        return answer.decode('utf-8')



#stemmer_connection = InteractionWithNLP('127.0.0.1',5200)


symptom_extraction_connection = InteractionWithNLP('127.0.0.1',1111)

symptom_extraction_connection.startConnection()

#stemmer_connection.startConnection()

#print(symptom_extraction_connection.sendDataGetResponse("Başım kanıyor"))