# Avicenna Arkayüz yazılımı


### Yazılımı Kurmak için


   Öncellike sunucumuzda yazılımımız bu yazılımların
    bu versiyonları kurulmalıdır.

    1. Sunucu işletim sistemi:Ubuntu 16.04.3 LTS
    2. Python versiyon:Python 3.6.3
    3. Postgresql versiyon:PostgreSQL 9.5.10
    4. RabbitMQ versiyon:3.5.7

Daha sonra Python modülleri pip ile kurulmalıdır.
```
pip install -r requirements
```


