from sklearn.feature_extraction.text import CountVectorizer,TfidfVectorizer
import numpy as np
import torch
import torch.nn as nn
from sklearn.utils import as_float_array

my_data = open("/Users/cenk/IdeaProjects/IDParser/training.csv",encoding="utf-8").read().split("\n")


training_X_Sentences = []
training_X_labels = []

labels_map = {}

transformer = CountVectorizer()
transformer_tfidf = TfidfVectorizer()

def vectorizeY(no):
    my_arr = [0 for el in labels_map]
    my_arr[no-1] = 1
    return my_arr


for line in my_data:
    line_splitted = line.split("::")
    if len(line_splitted)>1:
        training_X_Sentences.append(line_splitted[0])
        training_X_labels.append(line_splitted[1])
        if line_splitted[1] not in labels_map:
            labels_map[line_splitted[1]]  = len(labels_map) + 1



X = transformer.fit_transform(training_X_Sentences)
X_tfidf = transformer_tfidf.fit_transform(training_X_Sentences)
X = as_float_array(X.toarray())

X_tfidf = as_float_array(X_tfidf.toarray())

print(X.shape)

Y = []
for element in training_X_labels:
    Y.append(vectorizeY(labels_map[element]))
    
Y = np.array(Y,dtype=np.float32)


class NeuralNet(nn.Module):
    def __init__(self, input_size, hidden_size, num_classes):
        super(NeuralNet, self).__init__()
        self.fc1 = nn.Linear(hidden_size, 10)
        self.relu = nn.Softmax()
        self.fc2 = nn.Linear(10, num_classes)
    
    def forward(self, x):
        print(x.shape)
        #print(x[0])
        out = self.fc1(x)
        out = self.relu(out)
        out = self.fc2(out)
        return out
        



model = NeuralNet(53,107,10)

criterion = nn.CrossEntropyLoss()


optimizer = torch.optim.Adam(model.parameters(), lr=0.01)



inputs = torch.from_numpy(X_tfidf).type(torch.FloatTensor)
targets = torch.from_numpy(Y).type(torch.LongTensor)

for i in range(0,20):

    outputs = model(inputs)

    loss = criterion(outputs, torch.max(targets, 1)[1])
    print("Loss:{}".format(loss))
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()

def getMaximumIndex(arr):
    max_index = -400
    max_no = -400
    for i in range(0,len(arr)):
        if arr[i] > max_no:
            max_index = i
            max_no = arr[i]
    return max_index


labels_map_indexes = list(labels_map.keys())
def modelPredict(input):
    output = model(input)
    label_of_it = getMaximumIndex(output)
    print(labels_map_indexes[label_of_it])


modelPredict(inputs[1])
