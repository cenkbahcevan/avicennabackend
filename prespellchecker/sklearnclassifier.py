from sklearn.linear_model import LogisticRegression
import joblib
from sklearn.feature_extraction.text import TfidfVectorizer,CountVectorizer
from sklearn.model_selection import GridSearchCV
from stemmer import stemmer


class SklearnTextClassifier:

    def __init__(self,training_path,stemmer,delimeter_between_sentence="\n",delimeter_between_sentence_label="::",pretrained=False):

        self.trainingfile_content = open(training_path,encoding="utf-8").read()

        self.lines_list = self.trainingfile_content.split(delimeter_between_sentence)

        self.training_sentences = []
        self.training_labels = []

        self.vectorizer = None

        self.classifier  = None

        self.stemmer = stemmer


        for line in self.lines_list:
            
            line_splitted  = line.split(delimeter_between_sentence_label)
            
            if len(line_splitted) > 1:

                current_sentence = line_splitted[0]

                stemmed_current_sentence = stemmer.stemSentence(current_sentence)

                current_label = line_splitted[1]

                self.training_sentences.append(stemmed_current_sentence)
                self.training_labels.append(current_label)
        
    def train(self,vectorizer="tfidf",model="logistic"):

        if vectorizer == "tfidf":
            self.vectorizer = TfidfVectorizer()
        else:
            self.vectorizer = CountVectorizer()
        

        self.vectorizer.fit_transform(self.training_sentences)

        if model == "logistic":
            self.classifier = LogisticRegression()
            transformed_sentences = self.vectorizer.transform(self.training_sentences)
            self.classifier.fit(transformed_sentences, self.training_labels)
    def predict(self,sentence):

        sentence_transformed = self.vectorizer.transform([sentence])

        return self.classifier.predict(sentence_transformed)[0]





current_classifier = SklearnTextClassifier("training.csv",stemmer=stemmer)
current_classifier.train()









