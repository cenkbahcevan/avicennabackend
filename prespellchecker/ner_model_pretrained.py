from sklearn.svm import SVC
import numpy as np
from . import data_preparer
from prespellchecker import prespellchecker
from sklearn.tree import DecisionTreeClassifier
from sklearn.feature_extraction import DictVectorizer,FeatureHasher
from joblib import dump, load
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression,LogisticRegressionCV
from sklearn.ensemble import RandomForestClassifier
#from sklearn.qda import QDA
import sklearn.discriminant_analysis
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis



dict_vectorizer = DictVectorizer()


body_parts = ["baş","kemik","göz","mide","boyun","kalp","böbrek","kan","el"]

lexicon = open("/home/cenk/avicennabackend/prespellchecker/diseases_pr.txt",encoding="utf-8").read().split("\n")
stemmer = prespellchecker.stemmer

features_index = {
    "lexicon":0
    ,"wordposLexicon":1,
    "wiki_lexicon":2,
    "beginning":3,
    "wordPosunknown":4

}
#deneme

def extractFeatures(word,i,prev_label="beginning",prev_word="beg",next_word="no"):
        
        result_features = []
        result_features.append(prev_word)
        """
        if word_index == 0:
                result_features.append("beginning")
        elif word_index == all_length - 1 :
                result_features.append("end")
        else:
                result_features.append("middle")
        
        """

        stemmedWord = stemmer.stemWord(word)[0]

        if stemmedWord in body_parts:
                result_features.append("bodypart")


        stemmedPrev = stemmer.stemWord(prev_word)[0]

        if stemmedPrev in body_parts:
                result_features.append("prevbodypart")


        stemmedNext = stemmer.stemWord(next_word[4:])[0]

        result_features.append("nextstem" + stemmedNext)

        if len(stemmedNext) > 1:
                stemmedNextUpper = stemmedNext[0].upper() + stemmedNext[1:]
                if stemmedNext in lexicon  or stemmedNextUpper in lexicon:
                        result_features.append("nextlexicon")


        if stemmedPrev != "no":
                result_features.append("prevstem" + stemmedPrev)




        if stemmedWord == "fıtık":
                pass#print("Bak:{}".format(word))

        #result_features.append(next_word)


        word_pos_lexicon= False
        word_upper_form = word
        if len(word)>0:
                word_upper_form = word[0].upper() + word[1:]
        word_all_upper = word.upper()
        word_upper_i = word.replace("i","I").upper()


        if word in lexicon or word_upper_form in lexicon or word_upper_i in lexicon or word_all_upper in lexicon:
                result_features.append("lexicon")
                #result_features.append("wordposLexicon")




        word_stem_feature = "wordLemma" + stemmedWord

        #word_pos_feature = "wordPos" + stemmedWord.word_pos

        word_suffix = ""

        if len(word) > len(stemmedWord) - 2:
                word_suffix = word[len(stemmedWord):]

                #result_features.append("suffix" + word_suffix[len(word_suffix) - 2:])


        

        if stemmedWord in data_preparer.words_list_set or word in data_preparer.words_list_set:
                pass#result_features.append("wiki_lexicon")

        if word_all_upper in data_preparer.words_list_set or word_upper_i in data_preparer.words_list_set:
                pass#result_features.append("wiki_lexicon")

        word_one_upper = word
        if len(word_one_upper) > 1:
                word_one_upper = word[0].upper() + word[1:]

        word_stemmed_upper  = stemmedWord
        if len(word_stemmed_upper) > 1:
                word_stemmed_upper =  stemmedWord[0].upper() + stemmedWord[1:]

        if word_one_upper in data_preparer.words_list_set or word_stemmed_upper in data_preparer.words_list_set:
                pass#result_features.append("wiki_lexicon")

        elif word_one_upper in lexicon or word_stemmed_upper in lexicon:
                pass#esult_features.append("wiki_lexicon")
        


        if prev_label != "beginning" and prev_label != "beg":
                result_features.append(prev_label)

        
        if len(word)>3:
                miss1form = word[0:len(word)-1]
                result_features.append("1miss" + miss1form)
                if miss1form in lexicon or miss1form[0].upper() + miss1form[1:] in lexicon:
                        result_features.append("1wikilexicon")
        

        if len(word)>4:
                miss2form = word[0:len(word)-2]
                result_features.append("2miss" + word[0:len(word)-2])

                if miss2form in lexicon or miss2form[0].upper() + miss2form[1:] in lexicon:
                        result_features.append("2wikilexicon")

        
        if len(word)>5:
                result_features.append("3miss" + word[0:len(word)-3])
        
        if len(word)>6:
                result_features.append("4miss" + word[0:len(word)-4])
        if len(word)>7:
                result_features.append("5miss" + word[0:len(word)-5])


        """
        if prev_word != "beg" and prev_word != "beginning":
                result_features.append(prev_label)
        """     


        result_features.append(word_stem_feature)



        #if word_pos_lexicon == False:
                #result_features.append(word_pos_feature)
        



        result_features.append("raw"  + word.lower())

        return result_features

training_X = []
training_Y = []

def initModel():
       for i in range(0,len(data_preparer.all_sentences)):
            current_sentence = data_preparer.all_sentences[i]
            current_labels = data_preparer.all_labels[i]
            for j,word in enumerate(current_sentence):
                features = None
                if j==0 and j < len(current_sentence) - 1:
                        features = extractFeatures(word,j,"no","","next" + current_sentence[j+1])

                if j==0:  
                        features = extractFeatures(word,j)
                
                elif j < len(current_sentence) - 1:
                        features = extractFeatures(word,j,"prev" + current_labels[j-1], current_sentence[j - 1 ] ,"next" + current_sentence[j+1])

                else:
                        #print("elsedeyim")
                        features = extractFeatures(word,j,"prev" + current_labels[j-1],current_sentence[j - 1 ])
                training_X.append(features)
                training_Y.append(current_labels[j])
                for feature in features:
                    if feature not in features_index:
                        features_index[feature] = len(features_index)



def vectorizeFeatures(my_inp):
    result_arr = []
    for feature in my_inp:
        result_arr.append(features_index[feature])
    return np.pad(np.array(result_arr),(0,10), 'maximum')




def extractSentenceFeatures():
    pass


training_X_vector = []

initModel()


hasher = FeatureHasher(input_type='string')
X = hasher.fit_transform(training_X)

#svc_classifier = LogisticRegression()




#print("Egitim başladı")
#svc_classifier.fit(X.toarray(),training_Y)
#dump(svc_classifier,"logistic.joblib")
svc_classifier = load("/home/cenk/avicennabackend/prespellchecker/logistic.joblib")
#print("Egitim bitti")

def predictSentence(sentence):
        sentence_splitted = sentence.split(" ")
        results = []
        for i in range(0,len(sentence_splitted)):

                if i==0 and len(sentence_splitted)>1:
                        current_features = extractFeatures(sentence_splitted[i],0,"beg","no","next" + sentence_splitted[i+1])
                        #print("Current features{}".format(current_features))
                        transformed_features = hasher.transform([current_features])
                        current_prediction = svc_classifier.predict(transformed_features.toarray())
                        results.append(current_prediction)

                elif i==0:
                        current_features = extractFeatures(sentence_splitted[i],0)
                        #print("Current features{}".format(current_features))
                        transformed_features = hasher.transform([current_features])
                        current_prediction = svc_classifier.predict(transformed_features.toarray())
                        results.append(current_prediction)
                
                elif i < len(sentence_splitted) - 1:
                        current_features = extractFeatures(sentence_splitted[i],i,"prev" + results[i-1][0],sentence_splitted[i-1],"next" + sentence_splitted[i+1])
                        #print(current_features)
                        transformed_features = hasher.transform([current_features])
                        current_prediction = svc_classifier.predict(transformed_features.toarray())
                        results.append(current_prediction)


                else:
                        
                        current_features = extractFeatures(sentence_splitted[i],i,"prev" + results[i-1][0],sentence_splitted[i-1])
                        #print(current_features)
                        transformed_features = hasher.transform([current_features])
                        current_prediction = svc_classifier.predict(transformed_features.toarray())
                        results.append(current_prediction)
        return results



def prettyPredict(sentence):
        results = []
        sentence_splitted = sentence.split(" ")
        labels  = predictSentence(sentence)
        for i in range(0,len(sentence_splitted)):
                #print("Word:{} -> Label:{}".format(sentence_splitted[i],labels[i]))
                results.append((sentence_splitted[i],labels[i][0]))
        return results



def extractDiseasesFromSentence(sentence):
        results = prettyPredict(sentence)
        temp_start = False
        my_results = []
        for i  in range(0,len(results)):
                if results[i][1] != "O" and temp_start:
                        my_results[len(my_results) - 1] += " " + results[i][0] 
                elif results[i][1] != "O":
                        my_results.append(results[i][0])
                        temp_start = True
                else:
                        temp_start = False
        
        for disease in my_results:
                pass#print(disease)
        
        return my_results

        

"""
test_features = extractFeatures("zcopscpdoks",0)
print(svc_classifier.predict(hasher.transform([test_features])))
#dump(svc_classifier, 'svc.joblib') 

#print("başım şiddetli ağrıyor ve bel fıtığım var {}".format(predictSentence("Başım şiddetli ağrıyor ve bel fıtığım var")))


prettyPredict("Alkol almayı çok seviyorum acaba karaciğer muyum")

prettyPredict("milan efendi kuduz çıktı")

print("")
prettyPredict("ne hastalıklarını cevaplıyorsun")

prettyPredict("kemiklerimde erimem var")

prettyPredict("bel soğukluğunda şiddetli seviyeye ulaştım")

prettyPredict("bel soğukluğum şiddetli seviyeye ulaştım")

prettyPredict("böbreğimde taş nasıl geçer")

prettyPredict("karaciğer yağlanmam kötüdür")


prettyPredict("çok yaşlandım acaba aids mi oldum ve başım yağlandı")

prettyPredict("kalbim yanıyor acaba adenit durumu var mıdır, kalp krizinde geçirdim")

prettyPredict("ülserim var ne yapmalıyım")

prettyPredict("miyopluğa ne iyi gelir")

prettyPredict("midem kötü acaba mide tembelliği mi var, sinüzite yakalandım ve sivilcelenme var")


extractDiseasesFromSentence("benim karaciğer yağlanmam kötüdür ve kızamığım oldum")       
print()
extractDiseasesFromSentence("bacağımda ödem oluştu")

extractDiseasesFromSentence("böbrek taşı oluştu")

extractDiseasesFromSentence("olur tansiyon var bende")

extractDiseasesFromSentence("gözde bronşite yakalanmış ve umur fil hastalığı olmuş")

extractDiseasesFromSentence("kolesterol problemim var")

extractDiseasesFromSentence("milanın sivilceleri var ve gribi çok kötü oldu")

extractDiseasesFromSentence("prostat sorunuma bir çözüm lazım")

extractDiseasesFromSentence("benim prostatıma var")

extractDiseasesFromSentence("uçuk önerin çıktı")

extractDiseasesFromSentence("beni belim ağrıyor acaba bel fıtığı mıyım")


extractDiseasesFromSentence("benim uykum geldi problemim var ve saçkıranım hastalığı hakkında ne düşünüyorsun")

extractDiseasesFromSentence("eyüpün boyun fıtığına ne iyi gelir diye soruyor")



extractDiseasesFromSentence("kalbim acıyor ve emirhan frengim var")
"""