import time
import math
from . import candidategenerators
from . import distancefunctions
#from . import sklearnclassifier
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import TfidfVectorizer,CountVectorizer




my_disease_words = list(map(lambda x: x.lower(), open("/home/cenk/avicennabackend/prespellchecker/diseases_pr.txt", encoding="utf-8").read().split("\n")))
my_abbr_words = list(map(lambda x: x.lower(), open("/home/cenk/avicennabackend/prespellchecker/abbr.txt", encoding="utf-8").read().split("\n")))
slang_words = ""

context_root = ["ağrıyor", "baş", "kafa", "göz", "ağrı", "yan", "dön", "uyuş", "el", "ayak", "göğüs",
                "karın", "mide", "tansiyon", "acı", "boğaz", "boyun", "omuz", "penis", "parmak", "bilek", "şiş",
                "bulantı", "bula", "uyuş", "eklem", "geniz", "burnum", "randevu", "diz", "kaburga", "böbrek", "idrar",
                "abdest", "göz", "alt", "üst", "hafta", "cinsel", "kasık", "merhaba", "selam","boşal"]


disambg_map = {
    "yanı":"zzzzzzz",
    "yan":"zzzzz"
}

class MirketWord:
    def __init__(self, word_content, word_lemma1):
        self.word_content = word_content
        self.word_lemma1 = word_lemma1

        self.is_it_uppercase = word_content[0:1].isupper()

    def __str__(self):
        return self.word_lemma1


class MapStemmer:
    zargan_word_stem_list = None
    suffix_list = set()

    word_lemma_dict_tr = {}

    reverse_word_lemma_dict_tr = {}

    def __init__(self):
        self.zargan_word_stem_list = open("/home/cenk/avicennabackend/prespellchecker/word_forms_stems_and_frequencies_full.txt", encoding="utf-8").read().split(
            "\n")[7:]

        for line in self.zargan_word_stem_list:

            line_splitted = line.split("\t")
            if len(line_splitted) > 2:
                raw_word = line_splitted[0]
                word_lemma = line_splitted[1]
                word_suffix_status = line_splitted[2]

                word_raw_suffix = raw_word[len(word_lemma):]
                self.suffix_list.add(word_raw_suffix)

                current_mirket_word = MirketWord(raw_word, word_lemma)

                #my_inner_dict = {"word_lemma": word_lemma, word_suffix_status: word_suffix_status}

                self.word_lemma_dict_tr[raw_word] = current_mirket_word

                if word_lemma not in self.reverse_word_lemma_dict_tr:
                    self.reverse_word_lemma_dict_tr[word_lemma] = [raw_word]
                else:
                    self.reverse_word_lemma_dict_tr[word_lemma].append(raw_word)

    turkish_sign_operators = {
        "...": " ...",
        "!": " !",
        ",": " ,",
        "?": " ?",
        ":": " :",
        ";": "   ;",
        "-": " _",
        "\\": " \\",
        "@": ""
    }

    def white_space_tokenize(self, sentence):
        if not isinstance(sentence, str) or len(sentence) < 1:
            return "csewwporwwe"
        for operator in self.turkish_sign_operators:
            sentence = sentence.replace(operator, self.turkish_sign_operators[operator])
        sentence_splitted_form = sentence.split(" ")
        return sentence_splitted_form

    def stemWord(self, word):
        if word == "karın" or "karin" in word:
            return ["karın"]
        if word == "bulanıyor":
            return ["bulantı"]
        results = []
        if word in self.word_lemma_dict_tr:
            results.append(self.word_lemma_dict_tr[word].word_lemma1)
        for i in range(len(word) - 1, -1, -1):
            current_word = word[0:i]
            suffix_part = word[i:]
            if current_word in self.word_lemma_dict_tr and suffix_part in self.suffix_list:
                results.append(self.word_lemma_dict_tr[current_word].word_lemma1)
                break
        if len(results) < 1:
            results.append(word)
        return results

    def stemSentence(self, sentence):
        word_index =  0
        result_sentence = ""
        tokenized_version = self.white_space_tokenize(sentence)
        for word in tokenized_version:
            stemmed_form = self.stemWord(word)[0]
            if "mıyor" in word[len(stemmed_form):] or "müyor" in word[len(stemmed_form):]:
                result_sentence = result_sentence + stemmed_form + "NEG "
            else:
                result_sentence = result_sentence + stemmed_form + " "

            if word == "yok" and word_index !=0:
                result_sentence = result_sentence[0:len(result_sentence)-1] + "+NEG "
            word_index = word_index + 1


        return result_sentence[0:len(result_sentence) - 1]

    def contains(self, word):
        return word in self.word_lemma_dict_tr

    def suffixcontains(self, suffix_part):
        return suffix_part in self.suffix_list


def checkInt(word):
    try:
        int(word)
        return True
    except:
        return False


def checkIntChar(word):
    for c in word:
        if checkInt(word):
            return True
    return False


def fixSentence(stemmer, sentence):
    sentence_fixed = ""
    sentence_words = stemmer.white_space_tokenize(sentence)
    #print(sentence_words)
    # print("Sentence words:{}".format(sentence_words))

    index_word = 0
    for word in sentence_words:
        is_word_aabr = word not in my_abbr_words
        is_word_disease = word not in my_disease_words

        if word == "ariyor" or word == "arıyor" or word== "ariyo"       :
            sentence_fixed = sentence_fixed + "ağrıyor "


        if stemmer.contains(word):
            sentence_fixed = sentence_fixed + word + " "

        elif stemmer.contains(word) == False and checkInt(
                word) == False and word not in my_disease_words and is_word_aabr:

            word_fixed = ""
            if candidategenerators.check_candidate_is_suitable(word):
                next_word = ""
                if index_word < len(sentence_words) - 2:
                    next_word = sentence_words[index_word + 1]
                if index_word > 0:
                    word_fixed = fixWord(stemmer, word, sentence_words[index_word - 1], next_word)
                else:
                    word_fixed = fixWord(stemmer, word, "", next_word)


            else:
                #print("     sartlari saglamiyor")
                word_fixed = word

            sentence_fixed = sentence_fixed + word_fixed + " "

        elif is_word_aabr == False:
            sentence_fixed = sentence_fixed + word + " "
        elif is_word_disease == False:
            sentence_fixed = sentence_fixed + word + " "

        elif checkIntChar(word):
            sentence_fixed = sentence_fixed + word + " "
        else:

            word_stem = stemmer.stemWord(word)[0]
            word_suffix = word[len(word_stem):]
            if len(word_suffix) > 0:
                possible_word = candidategenerators.checkDusme(word_stem, word_suffix)
                if stemmer.contains(possible_word):
                    sentence_fixed = sentence_fixed + possible_word + " "
                else:
                    sentence_fixed = sentence_fixed + word + " "

            else:
                sentence_fixed = sentence_fixed + word + " "
        index_word = index_word + 1

    return sentence_fixed


def fixWord(stemmer, word, word_prev, word_next):
    if len(word)>21 or word == "corona" or word == "korona":
        return word
    temp_dist = 100
    temp_word = ""

    # my_all_words = my_organ_list + my_affect_list + my_adjectives + my_time


    if len(word) > 2:
        for i in range(len(word) - 1, 1, -1):
            suffix_part = word[i:]
            current_word = word[0:i]
            if stemmer.suffixcontains(suffix_part) and current_word in my_disease_words:
                return current_word

    my_all_words = candidategenerators.produce_all_candidates(word)
    if "arisi" in my_all_words:
        my_all_words.remove("arisi")

    #print("Word next:{} {}".format(word_next, word_prev))
    ngram_candidates = []

    if word_prev in candidategenerators.ngram_candidate_map:
        ngram_candidates += list(candidategenerators.ngram_candidate_map[word_prev]["next"].keys())

    if word_next in candidategenerators.ngram_candidate_map:
        ngram_candidates += list(candidategenerators.ngram_candidate_map[word_next]["prev"].keys())

    word_prev_stemmed = stemmer.stemWord(word_prev)[0]
    if word_prev_stemmed in candidategenerators.ngram_candidate_map:
        ngram_candidates += list(candidategenerators.ngram_candidate_map[word_prev_stemmed]["next"].keys())

    #print(ngram_candidates)

    my_all_words = my_all_words + ngram_candidates
    #print(ngram_candidates)
    #print(candidategenerators.returnNGramCandidates(word_prev, word_next, stemmer))

    # print(my_all_words)

    # print("all candidates:{}".format(my_all_words))

    # for possible_candidate_words in my_all_words:
    for lemma in my_all_words:
        # print(lemma)

        if stemmer.contains(lemma):

            score = distancefunctions.edit_distance(word, lemma)
            stemmed_first = stemmer.stemWord(lemma)[0]
            if stemmed_first in context_root:
                # print("oluyor\n\n{}".format(stemmed_first))
                score = score - 2

            if lemma in ngram_candidates:
                score = score - 1
            if score < temp_dist:
                # print("Word:{} Score:{}".format(lemma,score))
                temp_word = lemma
                temp_dist = score

    if temp_word != "":
        return temp_word
    return word


"""
İlk harfi kucuk ile yazmak lazım
"""
stemmer = MapStemmer()

turkish_lower_map = {"I": "ı"}

affect_words = ["ağrı","yan"]
affect_words_suffix = {
    "ağrı":"ağrı",
    "yan":"yanma"
}

def djangoFixSentence(sentence):
    lower_form = ""
    if sentence[0] == "I" and len(sentence) > 0:
        lower_form = "ı" + sentence[1:]

        lower_form = lower_form.replace(".","")
        lower_form = lower_form.replace(",","")
        return fixSentence(stemmer, lower_form)

    sentence = sentence.replace("İ","i")
    my_sentence = sentence[0].lower() + sentence[1:].lower()
    my_sentence = my_sentence.replace("sancı","ağrı")
    my_sentence = my_sentence.replace(".","")
    my_sentence = my_sentence.replace(",","")
    my_sentence = my_sentence.replace("-","")
    my_sentence = my_sentence.replace("\n"," ")
    my_sentence = my_sentence.replace("\r"," ")
    my_sentence = my_sentence.replace("\\n"," ")
    #print("my_sentence:{}".format(my_sentence))
    if my_sentence != "ggootfkptvhigyuguugho" and my_sentence != "uriiirioriirotuiogoto":
        return fixSentence(stemmer, my_sentence)
    else:
        return my_sentence


def stemSentence(sentence):
    affect_contain = ""
    stemmed_res =  stemmer.stemSentence(sentence.lower())
    for word in stemmed_res.split(" "):
        if word in affect_words:
            affect_contain = affect_words_suffix[word]

    if len(affect_contain)>1:
        affect_contain = affect_contain[0:1].upper() + affect_contain[1:]

    return stemmed_res , affect_contain





class SklearnTextClassifier:

    def __init__(self,training_path,stemmer,delimeter_between_sentence="\n",delimeter_between_sentence_label="::",pretrained=False):

        self.trainingfile_content = open(training_path,encoding="utf-8").read()

        self.lines_list = self.trainingfile_content.split(delimeter_between_sentence)

        self.training_sentences = []
        self.training_labels = []

        self.vectorizer = None

        self.classifier  = None

        self.stemmer = stemmer


        for line in self.lines_list:
            
            line_splitted  = line.split(delimeter_between_sentence_label)
            
            if len(line_splitted) > 1:

                current_sentence = line_splitted[0]

                stemmed_current_sentence = stemmer.stemSentence(current_sentence)

                current_label = line_splitted[1]

                self.training_sentences.append(stemmed_current_sentence)
                self.training_labels.append(current_label)
        
    def train(self,vectorizer="tfidf",model="logistic"):

        if vectorizer == "tfidf":
            self.vectorizer = TfidfVectorizer()
        else:
            self.vectorizer = CountVectorizer()
        

        self.vectorizer.fit_transform(self.training_sentences)

        if model == "logistic":
            self.classifier = LogisticRegression()
            transformed_sentences = self.vectorizer.transform(self.training_sentences)
            self.classifier.fit(transformed_sentences, self.training_labels)
    def predict(self,sentence):

        sentence_transformed = self.vectorizer.transform([sentence])

        return self.classifier.predict(sentence_transformed)[0]


current_classifier = SklearnTextClassifier("/home/cenk/avicennabackend/prespellchecker/training.csv",stemmer=stemmer)
current_classifier.train()