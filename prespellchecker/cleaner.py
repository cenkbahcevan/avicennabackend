
import os
import re

my_stop_words = open("/home/cenk/avicennabackend/prespellchecker/stopwords.txt",encoding="utf-8").read().split("\n")


PAT_ALPHABETIC = re.compile(r'(((?![\d])\w)+)', re.UNICODE)


def regexTokenize(sentence):
        new_sentence_tokenized = []

        for element in PAT_ALPHABETIC.finditer(sentence):

            current_word = element.group()

            new_sentence_tokenized.append(current_word)

        return new_sentence_tokenized


def removeStopWords(sentence):
    sentence_manipulated = regexTokenize(sentence)
    my_sentence = ""
    for word in sentence_manipulated:
        if word in my_stop_words:
            my_sentence = my_sentence + " "
        else:
            my_sentence = my_sentence + word + " "
    
    mysentence_whitespaces_filtered = re.sub('\s+', ' ', my_sentence).strip()
    return mysentence_whitespaces_filtered

