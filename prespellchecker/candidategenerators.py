
vovels = ["a" ,"ı" ,"o" ,"u" ,"e" ,"ü" ,"ö","i"]

thin_vowel = ["i" ,"e" ,"ö" ,"ü"]
bold_vowel = ["a" ,"o" ,"u" ,"ı"]

unvovel = ["b", "c" ,"ç", "d" ,"f", "g", "ğ" ,"h", "j" ,"k", "l", "m", "n" ,"p", "r" ,"s" ,"ş" ,"t" "v", "y", "z"]

change_vowel = [{"k" :"g"} ,{"g" :"k"} ,{"k" :"ğ"} ,{"ğ" :"k"},{"u":"o"}]

tight_vowel = ["ı" ,"i" ,"u" ,"ü"]

deascii_replacements = {"s" :"ş" ,"c" :"ç" ,"i" :"ı" ,"o" :"ö" ,"g" :"ğ" ,"u" :"ü","ı":"i"}
deascii_keys = ["s" ,"c" ,"i" ,"o" ,"g" ,"u","ı"]
deascii_no = 6

def addR(word):
    if word[len(word ) -1] == "o":
        return [word + "r"]
    return []

def check_candidate_is_suitable(word):
    if len(word) >= 3 and len(word) < 35:
        for i in range(0, len(word) - 3, 3):
            word_substr = word[i:i + 3]
            if word_substr[0] in unvovel and word_substr[1] in unvovel and word_substr[2] in unvovel:
                return False

            elif word_substr[0] in vovels and word_substr[1] in vovels and word_substr[2] in vovels:
                return False
    else:
        return False

    return True

def manipulateChar(word):
    candidates = []
    for i in range(0 ,len(word ) -1):
        first_char = word[i]
        next_char = word[ i +1]

        if first_char in unvovel and next_char in unvovel:
            modified_word_1 = word[0:i] + word[ i +1:]
            candidates.append(modified_word_1)
            if i+ 2 < len(word):
                candidates.append(word[0:i] + word[i + 2:])

    return candidates


def produceVowel(word):
    candidates = []
    for i in range(0, len(word) - 1):
        first_char = word[i]
        next_char = word[i + 1]

        if first_char in unvovel and next_char in unvovel:
            for vovel in vovels:
                modified_word_1 = word[0:i + 1] + vovel + word[i + 1:]
                candidates.append(modified_word_1)

    return candidates


def removeVowel(word):
    candidates = []
    for i in range(1, len(word)):
        if word[i] in vovels:
            candidate_word = word[0:i] + word[i + 1:]
            candidates.append(candidate_word)
    return candidates


def applyYumusama(word):
    candidates = []
    for i in range(1, len(word) - 1):
        if word[i] == "p":
            candidates.append(word[0:i] + "b" + word[i + 1:])
        if word[i] == "ç":
            candidates.append(word[0:i] + "c" + word[i + 1:])
        if word[i] == "t":
            candidates.append(word[0:i] + "d" + word[i + 1:])
        if word[i] == "k":
            candidates.append(word[0:i] + "ğ" + word[i + 1:])

    # print("Candidates:{}".format(candidates))
    return candidates


def replaceWord(word):
    candidates = []
    current_word = word
    for d in change_vowel:
        first_key = list(d.keys())[0]
        current_word_man = word.replace(first_key, d[first_key])
        candidates.append(current_word_man)
    return candidates


def deasciify(word, before):
    # print("before:{}".format(before))
    word_list = list(word)
    if len(word) < 1:
        return []
    elif word_list[0] in deascii_keys:
        before2 = before + word_list[0]
        word_list[0] = deascii_replacements[word_list[0]]
        before = before + word_list[0]
        return [before + "".join(word_list[1:]), before2 + "".join(word_list[1:])] + deasciify(word[1:],
                                                                                               before) + deasciify(
            word[1:], before2)
    before = before + word_list[0]
    return deasciify(word[1:], before)


def deasciify2(word,start_index):
        if len(word) == start_index:
            return []
        elif word[start_index] in deascii_replacements:

            word_manipulated = list(word)
            word_manipulated[start_index] = deascii_replacements[word[start_index]]
            word_manipulated = "".join(word_manipulated)

            return [word,word_manipulated] + deasciify2(word,start_index + 1) + deasciify2(word_manipulated,start_index + 1)

        return deasciify2(word,start_index+1)

def produce_all_candidates(word):
    word_used = word
    word_used2 = word
    deascii_candidates = deasciify2(word_used, 0)

    word_r_deascii = []
    if word_used[len(word_used) - 1] == "o":
        word_r_deascii = deasciify2(word_used + "r",0)

    passed_from_deascii = []

    man_char = manipulateChar(word)

    deascii_man = []
    for word in man_char:
        deascii_man = deascii_man + deasciify(word_used2, "")

    for candi in deascii_candidates:
        passed_from_deascii = passed_from_deascii + removeVowel(candi) + produceVowel(candi) + applyYumusama(
            candi) + replaceWord(word) + addR(word)

    return passed_from_deascii + produceVowel(word) + man_char + applyYumusama(word) + replaceWord(word) + deasciify(
        word, "") + deascii_man + addR(word) + word_r_deascii


def findWordLastTightVowelIndex(word):
    last_index = -1
    for i in range(0, len(word)):
        if word[i] in tight_vowel:
            last_index = i
    return last_index


def checkDusme(word_stem, word_suffix):
    if word_suffix[0] in vovels:
        last_index = findWordLastTightVowelIndex(word_stem)
        if last_index != -1:
            # print("return ettim {} {}".format(last_index,word_suffix))
            return word_stem[0:last_index] + word_stem[last_index + 1:] + word_suffix
    return word_stem + word_suffix


ngram_candidate_map = {}
vocab_set = set()


def addToMap(my_map, key, value):
    my_map[key] = value


my_file_readed = open("../my_sentences.txt", encoding="utf-8").read().split(".")

words_splitted_arr = []
for line in my_file_readed:
    words_splitted = line.split(" ")
    words_splitted_arr.append(words_splitted)
    [vocab_set.add(word) for word in words_splitted]
    [addToMap(ngram_candidate_map, word, {"prev": {}, "next": {}}) for word in words_splitted]

for sentence in words_splitted_arr:
    for i in range(0, len(sentence)):
        if i == 0 and len(sentence) > 1:
            current_word = sentence[i]
            next_word = sentence[1]

            if next_word in ngram_candidate_map[current_word]["next"]:
                ngram_candidate_map[current_word]["next"][next_word] += 1
            else:
                ngram_candidate_map[current_word]["next"][next_word] = 1

        elif i == len(sentence) - 1:
            prev_word = sentence[i - 1]
            current_word = sentence[i]
            if prev_word in ngram_candidate_map[current_word]["prev"]:
                ngram_candidate_map[current_word]["prev"][prev_word] += 1
            else:
                ngram_candidate_map[current_word]["prev"][prev_word] = 1

        else:
            prev_word = sentence[i - 1]
            current_word = sentence[i]
            next_word = sentence[i + 1]

            if next_word in ngram_candidate_map[current_word]["next"]:
                ngram_candidate_map[current_word]["next"][next_word] += 1
            else:
                ngram_candidate_map[current_word]["next"][next_word] = 1

            if prev_word in ngram_candidate_map[current_word]["prev"]:
                ngram_candidate_map[current_word]["prev"][prev_word] += 1
            else:
                ngram_candidate_map[current_word]["prev"][prev_word] = 1


if "" in ngram_candidate_map:
    del ngram_candidate_map[""]


def returnNGramCandidates(that_word, that_word2):
    try:
        return list(ngram_candidate_map[that_word]["next"].keys()) + list(
            ngram_candidate_map[that_word2]["prev"].keys())
    except:
        return []

