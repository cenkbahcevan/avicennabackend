# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
from jsonfield import JSONField
from datetime import datetime
from django.contrib.postgres.fields import ArrayField
from ckeditor.fields import RichTextField


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)

class AvicennaUser(User):
    age = models.IntegerField()
    weight = models.FloatField(default=0)
    height = models.FloatField(default=0)
    gender = models.CharField(max_length=15,default="",null=True)
    smoking = models.CharField(max_length=15,default="",null=True)
    last_sentence = models.TextField(default="",null=True)
    sickness = models.ManyToManyField("GeneralSickness",blank=True,default=None,null=True)
    categories = models.ManyToManyField('AvicennaNewsCategory',blank=True,null=True)
    dontunderstand_no = models.IntegerField(default=0)
    user_uuid = models.CharField(max_length=500,default="")
    last_disease_entered = models.CharField(max_length=600,default="")
    last_entry_date = models.DateTimeField(null=True)
    last_notification_sent_date = models.DateTimeField(default=datetime.now())
    newtypeuser = models.BooleanField(default=False)
    last_notification_seen = models.BooleanField(default=False)




class AvicennaNewsCategory(models.Model):
    category_id = models.AutoField(primary_key=True)
    category_name = models.CharField(max_length=300,null=False,default="")
    category_image = models.ImageField(null=True)

    def __str__(self):
        return str(self.category_id) + " " + self.category_name

class AvicennaSentencesEntered(models.Model):
    sentence_id = models.AutoField(primary_key=True)
    sentence_text = models.TextField(default="")

    def __str__(self):
        return self.sentence_text

class SymptomQuestionSet(models.Model):
    question_set_id = models.AutoField(primary_key=True)
    question_set_name = models.CharField(max_length=400,null=False,blank=False,default="")
    question_set_body_part = models.CharField(max_length=100,default="genel",blank="False")
    question_set_body_affect = models.CharField(max_length=100,default="genel",blank="False")
    question_set_full_name = models.CharField(max_length=150,default="",blank=False,null=False)
    question_set_suffix = models.CharField(max_length=10,default="",blank=False,null=False)
    question_set_whatdont = models.TextField(default="")
    question_set_whateatdrink = models.TextField(default="")
    question_set_whateatdrink_en = models.TextField(default="")
    set_types = (
        ('D', 'Daily'),
        ('N', 'Normal'),
    )
    question_set_type = models.CharField(choices=set_types, max_length=1, null=True, blank=True)
    question_set_require_diagnosis = models.BooleanField(default=True)
    question_set_require_advice = models.BooleanField(default=False)
    question_set_body_part_en = models.CharField(max_length=100,default="general",blank="False")
    question_set_body_affect_en = models.CharField(max_length=100,default="general",blank="False")
    question_set_visible_to_search = models.BooleanField(default=True)




    def __str__(self):
        if self.question_set_visible_to_search:
            return str(self.question_set_id) + " " + self.question_set_name + "Aramada görünüyor"
        return str(self.question_set_id) + " " + self.question_set_name 



class Question(models.Model):
    que_id = models.AutoField(primary_key=True)
    que_content = models.TextField(default="")

    def __str__(self):
        return self.que_content

class SymptomQuestions(models.Model):
    que = models.ForeignKey('Question',null=True,blank=True)
    question_id = models.AutoField(primary_key=True)
    question_content = models.TextField(null=False)
    question_explanation_text = models.TextField(default="",null=True,blank=True)
    question_organ = models.TextField(null=False,default="genel")
    question_affect = models.TextField(null=True)
    question_set = models.ForeignKey(SymptomQuestionSet,null=True,blank=True)
    answers = models.ManyToManyField('SymptomAnswerKit',null=True,blank=True)
    does_not_require_answer = models.BooleanField(default=False)
    question_type = (
        ('E','Affect'),
        ('D','Date'),
        ('F','Follow'),
        ('P','WhichPart '),
        ('FE','Feedback'),
        ('T','Telephone'),
        ('C','City'),
        ('A','Amount')
    )
    question_choices = models.CharField(choices=question_type,max_length=1,null=True,blank=True)
    question_informations = models.ManyToManyField('SymptomQuestionInfo',null=True,blank=True   )
    question_recordable = models.BooleanField(default=False)
    question_content_en = models.TextField(default="")
    def __str__(self):
        if self.question_set != None:

            en_is_done = self.question_content_en if self.question_content_en != "" else ""
            return  str(self.question_id) + " " + self.question_set.question_set_name + " " + self.question_content + "   --->  " + en_is_done
        else:
            return " "  + self.question_content


class SymptomQuestionInfo(models.Model):
    symptomquestioninfo_id = models.AutoField(primary_key=True)
    symptomquestioninfo_title = models.CharField(max_length=90,null=False,blank=False)
    info_type = (
        ('I', 'Image'),
        ('T', 'Text'),
    )
    symptomquestioninfo_type = models.CharField(choices=info_type,null=True,blank=True,max_length=1)
    symptomquestioninfo_value = models.TextField(default="",blank=False,null=False)


    def __str__(self):
        return self.symptomquestioninfo_title


class SymptomAnswerKit(models.Model):
    answer_id = models.AutoField(primary_key=True)
    question_belongs = models.ForeignKey(SymptomQuestions,related_name='question_id_for_current',verbose_name="Belongs")
    answer_content = models.TextField(null=False)
    selected_que = models.ForeignKey(SymptomQuestions,related_name='question_id_for_next',null=True,blank=True)
    int_value  = models.IntegerField(blank=True,null=True)
    activate = models.ManyToManyField('SymptomQuestionSet',null=True,blank=True)
    veriyfy_symptom = models.BooleanField(default=False)
    answer_content_en = models.TextField(default="")

    #null icin yaptim
    def save(self, *args, **kwargs):
        if not self.selected_que:
            self.selected_que = None
        super(SymptomAnswerKit, self).save(*args, **kwargs)

    def __str__(self):
        return  str(self.answer_id) + " " + self.question_belongs.question_set.question_set_name + " " +  str(self.question_belongs.question_content) + " " + self.answer_content


class GeneralSickness(models.Model):
    sickness_id = models.AutoField(primary_key=True)
    sickness_name = models.CharField(max_length=200,null=False,blank=False)
    sickness_explanation  = models.TextField(null=False,blank=False,default="")

    def __str__(self):
        return self.sickness_name




class AvicennaUserSickness(models.Model):
    sickness_user_id = models.AutoField(primary_key=True)
    sickness = models.ForeignKey('GeneralSickness',null=True)
    user = models.ForeignKey('AvicennaUser')


class AvicennaMedications(models.Model):
    avm_id = models.AutoField(primary_key=True)
    avm_name = models.CharField(max_length=200,null=False,blank=False)
    avm_why = models.TextField(null=False,default="",blank=False)
    avm_who = models.TextField(null=False,default="",blank=False)
    avm_side = models.TextField(null=False,default="",blank=False)
    avm_when = models.TextField(null=False,default="",blank=False)
    avm_content = models.TextField(null=False,blank=False,default="")

    def __str__(self):
        return self.avm_name

class Medications(models.Model):

    medication_id = models.AutoField(primary_key=True)
    medication_name = models.CharField(max_length=200,null=False,blank=False)
    medication_content = models.TextField(null=True,blank=False,default="")

    def __str__(self):
        return self.medication_name

class AvicennaUserComplaints(models.Model):
    complaints_id = models.AutoField(primary_key=True)
    complaint_type = models.ForeignKey('SymptomQuestionSet',blank=False,null=True,default=None)
    complaints_time = models.DateTimeField(null=True)
    complaints_end_time = models.DateTimeField(null=True)
    complaint_quantity = models.CharField(max_length=40,blank=False,null=False,default="")
    complaint_user = models.ForeignKey('AvicennaUser')

    def __str__(self):
        return self.complaint_user.username + " " + self.complaint_type.question_set_full_name + " "  + str(self.complaints_id)


class AvicennaUserComplaintSubDetails(models.Model):
    complaint_sub_id =  models.AutoField(primary_key=True)
    user_complaint = models.ForeignKey('AvicennaUserComplaints',null=False,blank=False)
    question_type = (
        (1, 'İyi'),
        (-1, 'Kötü'),
        (0, 'Değişmiyor'))
    complaint_situation_progress = models.CharField(max_length=200,default="",blank=True)



class AvicennaMedicationUse(models.Model):
    medication_use_id = models.AutoField(primary_key=True)
    used_medication = models.ForeignKey('Medications',blank=False,null=False)
    avicenna_user = models.ForeignKey('AvicennaUser',blank=False,null=False)
    medication_use_time = models.DateTimeField(null=False,blank=False,default=datetime.now())


class OrganInfo(models.Model):
    organ_name = models.CharField(max_length=60,default="")
    organ_synonym = models.CharField(max_length=60,default="")


class AffectInfo(models.Model):
    affect_name = models.CharField(max_length=60,default="")


class HealthWikiCategory(models.Model):
    category_id = models.AutoField(primary_key=True)
    category_name = models.CharField(max_length=150,default="",blank=True)

    def __str__(self):
        return self.category_name

class HealthWikiPage(models.Model):
    page_id = models.AutoField(primary_key=True)
    page_title = models.CharField(max_length=300,default="",null=False)
    page_title_en = models.CharField(default="",max_length=300)
    page_short_title = models.CharField(max_length=200,default="",null=False)
    page_summary = models.CharField(max_length=200,default="",null=False)
    page_symtpoms = models.TextField(default="")
    page_increasingrisk = models.TextField(default="")
    page_whentogodoctor = models.TextField(default="")
    page_diagnosis = models.TextField(default="")
    page_prevention = models.TextField(default="")
    page_content = RichTextField(default="")
    page_category = models.ForeignKey('HealthWikiCategory',null=True)
    page_extra_link = models.ManyToManyField('ExtraInfoPage',null=True,blank=True)
    visible_to_search = models.BooleanField(default=True)
    webmd_link = models.TextField(default="")
    page_symptoms_en = models.TextField(default="en")
    page_summary_en = models.TextField(default="en")
    page_prevention_en = models.TextField(default="en")

    def __str__(self):
        en_is_done = " İngilizcesi var" if self.webmd_link != "" else ""
        veri_girilmis = " Tukce veri girilmis" if len(self.page_prevention) > 10 else ""
        return self.page_title + "  " + en_is_done + "->" +  veri_girilmis



class SymptomForDiseases(models.Model):
    symptom_id = models.AutoField(primary_key=True)
    symptom_content = models.CharField(max_length=30,default="")
    symptom_title = models.CharField(max_length=30,default="")

    def __str__(self):
        return self.symptom_title

class Diseases(models.Model):
    disease_id = models.AutoField(primary_key=True)
    disease_name = models.CharField(max_length=30,default="")
    symptoms_list = models.ManyToManyField('SymptomForDiseases',null=True,blank=True)
    disease_wiki = models.ForeignKey('HealthWikiPage',null=True,blank=True)

    def __str__(self):
        en_wiki_exist = " Wiki var " if self.disease_wiki != None else ""
        return self.disease_name + en_wiki_exist

class AvicennaNews(models.Model):
    news_id = models.AutoField(primary_key=True)
    news_source = models.CharField(max_length=100,default="",null=False)
    news_summary = models.TextField(default="",null=True,blank=True)
    news_link = models.CharField(max_length=400,default="",null=False,unique=True)
    news_title = models.CharField(max_length=400,default="",null=False)
    news_category = models.ForeignKey('AvicennaNewsCategory',null=True)
    news_image_url = models.TextField(null=True,blank=True)

    def __str__(self):
        return str(self.news_id) + " " + self.news_source +  " "  + self.news_title

class AnswerScoreForDiagnosis(models.Model):
    asfd_id = models.AutoField(primary_key=True)
    answer_of_it = models.ForeignKey('SymptomAnswerKit')
    disease_id_of_it = models.ForeignKey('Diseases')
    point = models.IntegerField(default=1)

    def __str__(self):
        return self.answer_of_it.answer_content + " "  + self.disease_id_of_it.disease_name

class PriceTagForDepartment(models.Model):
    price_tag_for_department_id = models.AutoField(primary_key=True)
    price = models.FloatField(default=0.0)
    price_name = models.CharField(max_length=400,null=False,blank=False)


    def __str__(self):
        return self.price_name




class LocationOfHospital(models.Model):
    location_id = models.AutoField(primary_key=True)
    location_name = models.CharField(max_length=500,default=False)


class ExtraInfoPage(models.Model):
    page_id = models.AutoField(primary_key=True)
    page_title = models.CharField(max_length=250)
    page_link = models.CharField(max_length=500)

    def __str__(self):
        return self.page_title


class SetExtraInfo(models.Model):
    set_extra_id = models.AutoField(primary_key=True)
    symptom_set = models.ForeignKey(SymptomQuestionSet)
    extra_info_types_option = (
        ('T', 'text'),
        ('I', 'image'),
        ('V','video')
    )
    extra_info_types = models.CharField(max_length=1,default="T",choices=extra_info_types_option)
    extra_info_content = models.TextField(default="")

    def __str__(self):
        return self.symptom_set.question_set_name


class PreAnswers(models.Model):
    pre_answer_id = models.AutoField(primary_key=True)
    pre_answer_label = models.CharField(max_length=100)
    first_text = models.TextField(default="")
    second_text = models.TextField(default="")

    def __str__(self):
        return self.pre_answer_label



class SetVideos(models.Model):
    set_video_id = models.AutoField(primary_key=True)
    question_set = models.ForeignKey('SymptomQuestionSet')
    video_link = models.CharField(max_length=600,default="")

    def __str__(self):
        return self.video_link


class SimpleContextText(models.Model):
    simple_context_id = models.AutoField(primary_key=True)
    simple_context_match_text = models.TextField(default="")
    simple_context_match_text_en = models.TextField(default="")

    def __str__(self):
        return self.simple_context_match_text + "--->" + self.simple_context_match_text_en

class SampleText(models.Model):
    sample_text_id = models.AutoField(primary_key=True)
    sample_text = models.TextField(default="")
    simplecontexttype = models.ForeignKey(SimpleContextText)

    def __str__(self):
        return self.sample_text



class Doctor(models.Model):
    doctor_id = models.AutoField(primary_key=True)
    doctor_firstname = models.CharField(max_length=70,default="")
    doctor_lastname =  models.CharField(max_length=70,default="")
    doctor_tel = models.CharField(max_length=70,default="")
    doctor_website = models.CharField(max_length=300,default="")
    doctor_subtext = models.TextField(default="")
    doctor_image_path = models.TextField(default="")

    def __str__(self):
        return self.doctor_firstname + " " + self.doctor_lastname

class SymptomDoctor(models.Model):
    sd_id = models.AutoField(primary_key=True)
    symptom = models.ForeignKey(SymptomQuestionSet)
    thatdoc = models.ForeignKey(Doctor)

    def __str__(self):
        return self.thatdoc.doctor_firstname


class CookieUser(models.Model):
    cookie_user_id = models.AutoField(primary_key=True)
    cookie_text = models.TextField(default="")
    cookie_user_set = models.ForeignKey(SymptomQuestionSet)
    cookie_user_current_question = models.ForeignKey(SymptomQuestions)
    cookie_user_json = JSONField(default="{}")
    last_response = models.TextField(default="")

    def __str__(self):
        return self.cookie_text

class SymptomQuestionImage(models.Model):
    image_id = models.AutoField(primary_key=True)
    question_belongs = models.ForeignKey(SymptomQuestions)
    image_link = models.TextField(default="")

    def __str__(self):
        return self.question_belongs.question_content + " " + self.image_link


class GeneralText(models.Model):
    generaltext_id = models.AutoField(primary_key=True)
    generaltext_title = models.CharField(max_length=80,default="")
    generaltext_turkish_1 = models.TextField(default="")
    generaltext_turkish_2 = models.TextField(default="")
    generaltext_english_1 = models.TextField(default="")
    generaltext_english_2 = models.TextField(default="")

    def __str__(self):
        return self.generaltext_title


class TranslationText(models.Model):
    tt_id = models.AutoField(primary_key=True)
    tt_turkish = models.TextField(default="")
    tt_english = models.TextField(default="")

    def __str__(self):
        return self.tt_turkish + " |   ->" + self.tt_english


class FixedAnswers(models.Model):
    fixedanswer_id = models.AutoField(primary_key=True)
    fixedanswer_keyword = models.CharField(default="",max_length=30)
    fixedanswer_answercontent = models.TextField(default="")
    fixedanswer_answercontent_en = models.TextField(default="")

    def __str__(self):
        return self.fixedanswer_keyword 



class AndroidUser(models.Model):
    androiduser_id = models.AutoField(primary_key=True)
    androiduser_currentquestion = models.IntegerField(default=0)
    androiduser_currentset = models.IntegerField(default=0)

    def __str__(self):
        return self.androiduser_id


class Token(models.Model):
    token_id = models.AutoField(primary_key=True)
    token_type = models.CharField(max_length=150, default="")
    token_content = models.TextField(default="")

    
class LeniaProduct(models.Model):
    product_id = models.AutoField(primary_key = True)
    product_title = models.CharField(max_length=200,default = "")
    product_text = models.TextField(default="")
    product_link = models.TextField(default = "")
    product_image_link = models.TextField(default = "")

    def __str__(self):
        return self.product_title


class NotificationLastText(models.Model):
    notification_id = models.AutoField(primary_key = True)
    notification_date = models.DateTimeField(default=datetime.now())
    notification_text = models.TextField(default="")

    def __str__(self):
        return self.notification_text