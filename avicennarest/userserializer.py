from rest_framework import serializers

from .models import AvicennaUser


class AvicennaUserSerializer(serializers.ModelSerializer):

    class Meta:
        depth = 1
        model = AvicennaUser
        fields = ('weight','height','first_name',
                  'username','last_name','smoking','gender','email','last_name','age',
                  )
