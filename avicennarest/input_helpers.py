import json
import interaction
from prespellchecker import prespellchecker

def prepare_sentence_analyzed_json_form(data):

    my_data = json.loads(data)
    # SymptomQuestionSerializer(data=request.data)

    symptom_sentence = my_data["sentence"].replace("(","").replace(")","")
    user_token = my_data["token"]

    current_sentence_fixed = prespellchecker.djangoFixSentence(symptom_sentence)

    current_sentence_fixed,_ = prespellchecker.stemSentence(current_sentence_fixed)
    #print(user_token)

    #print(symptom_sentence)

    result = interaction.symptom_extraction_connection.sendDataGetResponse(current_sentence_fixed)

    #result = result.decode("utf-8")

    result_json = json.loads(result)

    return result_json,user_token,symptom_sentence


def generateResponse(jsonData,key):
    if len(jsonData[key])>0:
        affect = ""
        affect = jsonData[key][0]
        if jsonData[key][0] == "ağr":
            affect = affect + "ınızın"
        elif jsonData[key][0] == "yan" or jsonData[key][0] == "kana":
            affect = affect + "manız"
        elif jsonData[key][0] == "mor":
            affect = affect + "manız"

        return str(key).title() + str(" ") +  affect + str(" ")

    else:
        return ""


