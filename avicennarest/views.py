# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import interaction
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from django.shortcuts import HttpResponse, redirect
from .models import AvicennaUser
from decimal import *
from rest_framework.views import APIView
from django.contrib.auth import models
import json
from .models import SymptomQuestions, SymptomAnswerKit, SymptomQuestionSet, GeneralSickness, Medications, \
    AvicennaMedications, AvicennaUserComplaints, AvicennaMedicationUse,PreAnswers,SimpleContextText,SampleText
from .questionanswerserializers import SymptomQuestionSerializer, SymptomAnswerKitSerializer, GeneralSicknessSerializer, \
    AvicennaMedicationsSerializer, AvicennaUserComplaintSerializer, SymptomQuestionSetSerializer, LeniaProduct
from django.shortcuts import render
from .qamodels import HeatlhInfo, HealthAnswers
import collections
import requests
from .userserializer import AvicennaUserSerializer
from django.db.models import Q
from . import input_helpers
from django.core.exceptions import *
import collections
import time
import datetime
from .models import AvicennaUserComplaintSubDetails, HealthWikiPage, AvicennaNews, AvicennaNewsCategory, FixedAnswers
from .questionanswerserializers import HealthWikiPageSerializer, AvicennaNewSerializer, AvicennaCategorySerializer
import codecs
from . import Diagnoser
import itertools
from . import nlp_helper
from . import slackutils
from . import notification_rest
from django.contrib.auth.models import User
from prespellchecker import prespellchecker
from prespellchecker import sexualmatcher
from avicennaweb.models import AutoCompleteAdditional
from avicennarest.models import Token
import pandas as pd

"""
Kullanıcının girdigi şikayet cümlesine göre soruları yüklüyor.
Önce kullanıcınnı cümlesini alıyor, Java'ya yolluyor
Java'nın analizine göre ona soru seti döndürüyro

Java'ya yollanan -> Java'dan alınan sonuc
Başım ağrıyor -> {baş={ağr={body_parts=[], adjectives=[]}}, genel={}}

Burada kullanıcını basi agridigi icin baş, ağr içeren soru setini çekiyor

"""

"""
Var mı sorusu ise gec
"""


def returnCurrentText(wiki_id, current_section):
    if current_section == 0:
        return HealthWikiPage.objects.get(pk=wiki_id).page_summary
    elif current_section == 1:
        return HealthWikiPage.objects.get(pk=wiki_id).page_symtpoms
    elif current_section == 2:
        return HealthWikiPage.objects.get(pk=wiki_id).page_prevention
    elif current_section == 3:
        return HealthWikiPage.objects.get(pk=wiki_id).page_prevention


def loadCurrentProduct(request):

    lenia_products = pd.DataFrame(LeniaProduct.objects.all().values()).to_json(orient="records")

    return HttpResponse(lenia_products,content_type = 'application/json'    )




@api_view(['POST'])
@permission_classes((AllowAny,))
def loadSQuestions(request):
    # arkadan sağlam, hatta baya


    if request.method != "PUT":

        # burada cümleyi java ya yolluyor

        result_json = {}


        result_json, user_token, symptom_sentence = input_helpers.prepare_sentence_analyzed_json_form(
            json.dumps(request.data))


        classifier_result = prespellchecker.current_classifier.predict(symptom_sentence)

        tekli_keys = list(result_json["tekli"].keys())

            #facebookfunctions.sendMessage(classifier_result,current_user.conversation_user)
                                

                                

        if classifier_result == "adet":
                    del result_json["tekli"]
                    result_json["tekli"] = {classifier_result:{}}

        elif classifier_result in ["estetik","psikoloji","hakaret","sigara","cinsel","kanser","yardim"]:
                        result_json["genel"] = {classifier_result:{}}
        elif len(tekli_keys) > 0 and tekli_keys[0] in ["adet"] and tekli_keys != classifier_result:
                        
                        if classifier_result != "semptom":
                            result_json["tekli"] = {classifier_result:{}}
                        else:
                            result_json["tekli"] = {}


        



        slackutils.sendTextToChannel(str(result_json) + "->" + symptom_sentence,"#ios")

        query_output = SampleText.objects.filter(sample_text__icontains=symptom_sentence).all()

        if len(query_output) > 0:
            result_dict = {}
            result_dict["text"] = query_output[0].simplecontexttype.simple_context_match_text
            result_dict["type"] = "anlamadim"
            # result_dict["title"] = "anlamadim"
            return Response(result_dict)

        current_user = None

        if user_token != ".":

            current_user_avicenna = models.User.objects.get(auth_token=user_token)

            current_user_avicenna = AvicennaUser.objects.get(username=current_user_avicenna.username)

            current_user_avicenna.last_entry_date = datetime.datetime.today()
            current_user_avicenna.save()

            current_user_avicenna.last_sentence = symptom_sentence

            #slackutils.sendTextToChannel(symptom_sentence,"#ios")



            current_user_avicenna.save()

        # print(result_json)

        # eger java analizinden 1 tane key cikioyrsa, saglikli iligili bir sey yok
        if len(result_json) <= 1:

            # Anlamadım soru setini cekiyor

            my_sentence_before_ann = nlp_helper.getTrueHealthPage(symptom_sentence)

            result_dict = {}

            if len(my_sentence_before_ann[0]) > 0:
                for element in my_sentence_before_ann[0]:
                    if isinstance(element, HealthWikiPage):
                        if len(my_sentence_before_ann[1]) > 0:
                            current_part = list(my_sentence_before_ann[1])[0]
                            result_dict["text"] = returnCurrentText(element.page_id, current_part)
                            result_dict["link"] = "https://www.lenia.ist/loadWikiPage?page_no=" + str(element.page_id)
                            return Response(result_dict)
                        else:
                            result_dict["text"] = element.page_summary
                            result_dict["type"] = "disease"
                            result_dict["title"] = element.page_title
                            result_dict["link"] = "https://www.lenia.ist/loadWikiPage?page_no=" + str(element.page_id)

                            return Response(result_dict)

            else:

                my_sentence_before_ann = nlp_helper.getTrueHealthPage(symptom_sentence)

                result_dict = {}

                if len(my_sentence_before_ann[0]) > 0:
                    for element in my_sentence_before_ann[0]:
                        if isinstance(element, HealthWikiPage):
                            if len(my_sentence_before_ann[1]) > 0:
                                current_part = list(my_sentence_before_ann[1])[0]
                                result_dict["text"] = returnCurrentText(element.page_id, current_part)
                                result_dict["type"] = "disease"
                                result_dict["title"] = element.page_title
                                return Response(result_dict)
                            else:
                                result_dict["text"] = element.page_summary
                                result_dict["type"] = "disease"
                                result_dict["title"] = element.page_title
                                result_dict["link"] = "https://www.lenia.ist/loadWikiPage?page_no=" + str(
                                    element.page_id)
                                return Response(result_dict)
                else:

                    result_dict[
                        "text"] = "Anlamadım. Tekrarlar mısın? Bana başım ağrıyor gibi şikayetlerinizi söyleyip, " \
                                  "bu şikayetler veya hastalıklar hakkında bilgi alabilirsiniz ⌛😟"
                    result_dict["type"] = "anlamadim"
                    # result_dict["title"] = "anlamadim"
                    return Response(result_dict)

        result_dict = {}

        print("Result json:{}".format(result_json))
        if len(result_json) < 3 and len(result_json["tekli"])<1 and len(result_json["genel"])>0:


            try:
                first_element = list(result_json["genel"].keys())[0]
                #print("First element:{}".format(first_element))
                
                my_key = sexualmatcher.cinsel_similarity(symptom_sentence)
                sexual_answer_found = False
                if str(my_key) != "-1" and first_element == "cinsel":
        
                    try:
                        my_fixed_ans = FixedAnswers.objects.get(fixedanswer_keyword=my_key)
                        result_dict["text"] = my_fixed_ans.fixedanswer_answercontent
                        sexual_answer_found = True
                    except:
                        pass
                if sexual_answer_found == False:
                    result_dict["text"] = PreAnswers.objects.get(pre_answer_label=first_element).first_text
                result_dict["type"] = "anlamadim"
                # result_dict["title"] = "anlamadim"
                return Response(result_dict)
            except:
                result_dict["text"] = "Anlamadım. Tekrarlar mısın? Bana başım ağrıyor gibi şikayetlerinizi söyleyip, " \
                              "bu şikayetler veya hastalıklar hakkında bilgi alabilirsiniz ⌛😟"
                result_dict["type"] = "anlamadim"
                # result_dict["title"] = "anlamadim"
                return Response(result_dict)


        try:

            dont_exclude_que = []
            for res in result_json:
                for affect in result_json[res]:
                    temp_dict = [res, affect]
                    dont_exclude_que.append(temp_dict)
            answers_id = []

            for res in result_json:
                if len(result_json[res]) > 0 and res != "genel":
                    for affect in result_json[res]:

                        # print("aliyorum",res,affect)


                        if SymptomQuestionSet.objects.filter(question_set_body_part=res
                                , question_set_body_affect=affect).count() > 0:
                            question_set = SymptomQuestionSet.objects.get(question_set_body_part=res
                                                                          , question_set_body_affect=affect)
                        else:
                            question_set = SymptomQuestionSet.objects.get(question_set_body_part=res
                                                                          , question_set_body_affect=affect[
                                                                                                     0:len(affect) - 1])

                        print(question_set.question_set_id)

                        if user_token != ".":
                            temp_complaint = AvicennaUserComplaints()
                            temp_complaint.complaint_type = question_set
                            temp_complaint.complaint_user = current_user_avicenna
                            temp_complaint.save()

                        symptom_questions = SymptomQuestions.objects.filter(question_set=question_set)

                        """
                        for dont_exclude_element in dont_exclude_que:
                                if dont_exclude_element[0] != res:
                                    #print(dont_exclude_element)
                                    question_excluded = symptom_questions.filter(question_organ=dont_exclude_element[0],question_affect=dont_exclude_element[1])
                                    #print("question excluded:{}".format(question_excluded))
                                    if len(question_excluded)>0:
                                        for que in question_excluded:
                                            #print(que.question_id)
                                            #print("iceride giriyroum {}".format(question_excluded))
                                            se_que = symptom_questions.get(answers__selected_que_id=que.question_id)
                                            answers_connected_to_it = symptom_questions.get(answers__selected_que_id=que.question_id).answers.all()
                                            for answer in answers_connected_to_it:

                                                answers_id.append({se_que.question_id:answer.answer_id})
                                    print("answerler {}".format(answers_id))




                        for answer in answers_id:
                            for key in answer:
                                print("que:{} ans:{}".format(key,answer[key]))
                                print(symptom_questions.get(pk=key).answers.get(pk=answer[key]))


                                #new_question_linked = symptom_questions.get(pk=symptom_questions.get(pk=key).answers.get(pk=answer[key]).selected_que_id)
                                #new_que_new_adress = new_question_linked.answers.get(veriyfy_symptom=True)
                                symptom_questions.get(pk=key).answers.get(pk=answer[key]).selected_que = None
                                #symptom_questions.save()
                                print(symptom_questions.get(pk=key).answers.get(pk=answer[key]).selected_que)

                        answers_id = []
                        """

                        # symptom_questions.save()

                        # print("cokmedim 2")



                        symptom_questions_serializer = SymptomQuestionSerializer(symptom_questions.all(), many=True)

                        print(result_json[res][affect]["body_parts"])

                        if len(result_json[res][affect]["body_parts"]) > 0:

                            that_part = result_json[res][affect]["body_parts"][0]

                            for i in range(0, len(symptom_questions_serializer.data)):

                                current_que = symptom_questions_serializer.data[i]
                                print(current_que)
                                print("currrent_que bitti")
                                if i == 0 and current_que["question_choices"] == 'P':
                                    break
                                for j in range(0, len(symptom_questions_serializer.data[i]["answers"])):

                                    selected_que_of_it = None
                                    try:
                                        selected_que_of_it = SymptomQuestions.objects.get(
                                            pk=symptom_questions_serializer.data[i]["answers"][j]["selected_que"])
                                        print(selected_que_of_it.question_choices)
                                    except:
                                        print()
                                    if selected_que_of_it != None and selected_que_of_it.question_choices == 'P':
                                        answers_list = selected_que_of_it.answers.all().values_list('answer_content',
                                                                                                    flat=True)
                                        answer_found_flag = False
                                        answer_use_que_index = 0
                                        for element in answers_list:
                                            if that_part.lower() in element.lower():
                                                answer_found_flag = True
                                                answer_use_que_index = SymptomAnswerKit.objects.get(
                                                    answer_content=element).selected_que_id
                                                break

                                        if answer_found_flag:
                                            next_ans = selected_que_of_it.answers.all()[0]
                                            print("next ans:{}".format(next_ans))
                                            symptom_questions_serializer.data[i]["answers"][j][
                                                "selected_que"] = answer_use_que_index






                                            # print("bitti")

                                            # symptom_questions.save()
                                            # print("kaydederken cokmedim")
                                            # print(symptom_questions)
                                            # symptom_questions_serializer = SymptomQuestionSetSerializer(symptom_questions.all(),many=True)

                        for i in range(0, len(symptom_questions_serializer.data)):
                            question_organ = symptom_questions_serializer.data[i]["question_organ"]
                            question_affect = symptom_questions_serializer.data[i]["question_affect"]
                            if question_organ in result_json and question_affect in result_json[question_organ]:
                                print("question_organ:{} question_affect:{} res_json:{}".format(question_organ,
                                                                                                question_affect,
                                                                                                result_json))
                                next_question_no = SymptomAnswerKit.objects.get(
                                    question_belongs_id=symptom_questions_serializer.data[i]["question_id"],
                                    veriyfy_symptom=True).selected_que_id
                                print("burun akmanın nosu:{}".format(next_question_no))
                                symptom_questions_serializer.data[i]["pass"] = str(next_question_no)
                            else:

                                symptom_questions_serializer.data[i]["pass"] = None

                        result_dict[question_set.question_set_id] = symptom_questions_serializer.data

                        # print("cokmedim")


        except:
            print("excepteyim")

            result_dict = {}

            my_key = sexualmatcher.cinsel_similarity(symptom_sentence)
            if str(my_key) != "-1" and classifier_result == "cinsel":
        
                    try:
                        my_fixed_ans = FixedAnswers.objects.get(fixedanswer_keyword=my_key)
                        result_dict["text"] = my_fixed_ans.fixedanswer_answercontent
                        result_dict["type"] = "anlamadim"
                        return Response(result_dict)
                    except:
                        pass
                
            my_sentence_before_ann = nlp_helper.getTrueHealthPage(symptom_sentence)

            if len(my_sentence_before_ann[0]) > 0:
                for element in my_sentence_before_ann[0]:
                    if isinstance(element, HealthWikiPage):
                        if len(my_sentence_before_ann[1]) > 0:
                            current_part = list(my_sentence_before_ann[1])[0]
                            result_dict["text"] = returnCurrentText(element.page_id, current_part)
                            return Response(result_dict)
                        else:
                            result_dict["text"] = element.page_summary
                            result_dict["type"] = "disease"
                            result_dict["title"] = element.page_title
                            result_dict["link"] = "https://www.lenia.ist/loadWikiPage?page_no=" + str(element.page_id)
                            return Response(result_dict)
            else:

                result_dict[
                    "text"] = "Anlamadım. Tekrarlar mısın? Bana başım ağrıyor gibi şikayetlerinizi söyleyip, " \
                              "bu şikayetler veya hastalıklar hakkında bilgi alabilirsiniz ⌛😟"
                result_dict["type"] = "anlamadim"
                return Response(result_dict)

        if len(result_dict) < 1:

            my_sentence_before_ann = nlp_helper.getTrueHealthPage(symptom_sentence)
            result_dict = {}

            if len(my_sentence_before_ann[0]) > 0:
                for element in my_sentence_before_ann[0]:
                    if isinstance(element, HealthWikiPage):
                        if len(my_sentence_before_ann[1]) > 0:
                            current_part = list(my_sentence_before_ann[1])[0]
                            result_dict["text"] = returnCurrentText(element.page_id, current_part)
                            result_dict["type"] = "disease"
                            result_dict["title"] = element.page_title
                            result_dict["link"] = "https://www.lenia.ist/loadWikiPage?page_no=" + str(element.page_id)
                            return Response(result_dict)
                        else:
                            result_dict["text"] = element.page_summary
                            result_dict["type"] = "disease"
                            result_dict["title"] = element.page_title
                            result_dict["link"] = "https://www.lenia.ist/loadWikiPage?page_no=" + str(element.page_id)
                            return Response(result_dict)
            else:

                result_dict[
                    "text"] = "Anlamadım. Tekrarlar mısın? Bana başım ağrıyor gibi şikayetlerinizi söyleyip, " \
                              "bu şikayetler veya hastalıklar hakkında bilgi alabilirsiniz ⌛😟"
                result_dict["type"] = "anlamadim"
                # result_dict["title"] = "anlamadim"
                return Response(result_dict)

            # print(symptom_questions_serializer.data)
            return Response(result_dict)

        return Response(result_dict)

    result_dict = {}
    result_dict["text"] = "Anlamadım. Tekrarlar mısın? Bana başım ağrıyor gibi şikayetlerinizi söyleyip, " \
                          "bu şikayetler veya hastalıklar hakkında bilgi alabilirsiniz ⌛😟"
    result_dict["type"] = "anlamadim"
    return Response(result_dict)


class RegisterView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request, format=None):
        my_data = json.loads(request.body.decode('utf-8'))

        name = my_data["name"]
        email = my_data["email"]
        lastname = my_data["lastname"]
        username = my_data["username"]
        password = my_data["password"]
        #age = my_data["age"]
        #height = my_data["height"]
        #weight = my_data["weight"]
        gender = my_data["gender"]
        #smoking = my_data["smoking"]

        kronik = 0

        if "kronik" in my_data:
            kronik = my_data["kronik"]
            print(kronik)
            kronik = GeneralSickness.objects.get(sickness_id=kronik)

        currentUserRequest = AvicennaUser()

        print(username)
        #print("Password:{}\n Weight:{} Smoking{} Gender{} Height {}".format(password, weight, smoking, gender, height))

        try:
            try:
                currentUserRequest.first_name = name
                currentUserRequest.last_name = lastname
                currentUserRequest.username = username
            except:
                print("Hata ilk uclude")

            if AvicennaUser.objects.filter(username=username).count() or User.objects.filter(
                    username=username).count() > 0:
                return Response({"status": "kullanici"})

            try:
                currentUserRequest.set_password(password)
                currentUserRequest.age = 0
            except:
                print("Hata sifrede")
                return HttpResponse({"status": "sifre"})
            try:
                pass#currentUserRequest.height = Decimal(height)
                #currentUserRequest.weight = Decimal(weight)
            except:
                print("Boy kilo hatali")
                return HttpResponse({"status": "boy"})

            currentUserRequest.email = email
            if AvicennaUser.objects.filter(email=email).count() > 0:
                return Response({"status": "email"})

            #   currentUserRequest.smoking = smoking
            currentUserRequest.gender = gender
            currentUserRequest.save()

            return Response({"status": "ok"})
        except:
            return Response({"status": "hata"})
        return Response({"status:": "hata"})


# duz django kuralları ile yap
class LoadUser(APIView):
    permission_classes = (AllowAny,)

    def post(self, request, format=None):
        try:
            my_data = str(request.data).replace("'", "\"")
            token = json.loads(my_data)["Token"]
            print(token)
            if "" != None:
                current_user_avicenna = models.User.objects.get(auth_token=token)
                print("cokmedim 1")
                current_user_avicenna = AvicennaUser.objects.get(username=current_user_avicenna.username)
                current_user_serialized = AvicennaUserSerializer(current_user_avicenna)
                print("dogru yerdeyim")
                print(current_user_serialized.data)
                return Response(current_user_serialized.data)
        except:
            return Response({"status": "hata"})
        return Response("pushalmaya hazir ol")


class MakeDiagnosisFromQuestions(APIView):
    permission_classes = (AllowAny,)

    def post(self, request, format=None):

        # json to string
        my_data = json.loads(json.dumps(request.data))

        print(my_data)

        my_data_int = {}

        my_token = ""

        # acess token i aliyorum
        if "token" in my_data and my_data["token"] != ".":
            my_token = my_data["token"]

            print(my_token)

            # tokene gore kullaniciyi aliyorum
            current_user_avicenna = models.User.objects.get(auth_token=my_token)




            print(current_user_avicenna)

            # kullanicinin Avicenna profilini aliyorum
            current_user_avicenna = AvicennaUser.objects.get(username=current_user_avicenna.username)

            #son entry
            current_user_avicenna.last_entry_date = datetime.datetime.today()
            current_user_avicenna.save()



        my_current_dict = {}

        for key in my_data:
            if key != "token":
                current_my_que = SymptomQuestions.objects.get(pk=int(key))
                if current_my_que.question_set not in my_current_dict:
                    my_current_dict[current_my_que.question_set] = {}
                my_current_dict[current_my_que.question_set][current_my_que.question_id] = my_data[key]
        if my_data["token"] != ".":
            for my_set in my_current_dict:
                temp_complaint = AvicennaUserComplaints()
                temp_complaint.complaint_type = my_set
                temp_complaint.complaint_user = current_user_avicenna
                for question in my_current_dict[my_set]:
                    current_my_que = SymptomQuestions.objects.get(pk=int(question))
                    if current_my_que.question_choices == 'E':
                        temp_complaint.complaint_quantity = float(my_current_dict[my_set][question])
                    else:
                        my_current_answer = SymptomAnswerKit.objects.get(pk=int(my_current_dict[my_set][question]))

                        if current_my_que.question_choices == 'D':
                            temp_complaint.complaints_time = datetime.datetime.today() - datetime.timedelta \
                                (days=my_current_answer.int_value)

            # eger etki sorusuysa, yani siddet ise kullanicinin sectigi sikka direkt esitle

                temp_complaint.save()

        my_data.pop('token', None)
        my_data.pop('Token', None)

        results = Diagnoser.diagnoseUserBackend(my_data)
        print("results:{}".format(results))
        my_objects = None
        for element in list(results.keys()):
            print("element:{}".format(element))
            if element.disease_wiki != None:
                my_element_wiki = HealthWikiPage.objects.filter(pk=element.disease_wiki.page_id)
                if my_objects == None:
                    my_objects = my_element_wiki
                else:
                    my_objects = my_objects | my_element_wiki

        # print(list(my_objects))
        my_serialized_objects = HealthWikiPageSerializer(my_objects, many=True)
        return Response(my_serialized_objects.data)


class LoadSicknessList(APIView):
    permission_classes = (AllowAny,)

    def get(self, request, format=None):
        my_val = GeneralSickness.objects.all()
        my_val_serializer = GeneralSicknessSerializer(my_val, many=True)
        return Response(my_val_serializer.data)


class LoadMedicationsList(APIView):
    permission_classes = (AllowAny,)

    def get(self, request, format=None):

        parameter = request.GET["text"]

        medications = AvicennaMedications.objects.filter(avm_name__icontains=parameter).all()

        medications_serializer = AvicennaMedicationsSerializer(medications, many=True)

        return Response(medications_serializer.data)

    def post(self, request, format=None):

        print(request.data)

        my_data = json.loads(json.dumps(request.data))

        my_token = ""

        if "token" in my_data:
            print(my_data["token"])
            my_token = my_data["token"]

        current_user_avicenna = models.User.objects.get(auth_token=my_token)

        current_user_avicenna = AvicennaUser.objects.get(username=current_user_avicenna.username)

        for key in my_data["medications"]:
            if key != "token":
                temp_medication_use = AvicennaMedicationUse()
                temp_medication_use.avicenna_user = current_user_avicenna
                temp_medication_use.used_medication = Medications.objects.get(pk=key)
                print(temp_medication_use)
                temp_medication_use.save()

        return Response({"status": "ok"})


class LoadMedicationDetail(APIView):
    def get(self, request, format=None):
        medicine_id = request.GET["medicine_id"]
        try:
            current_medicine = AvicennaMedications.objects.get(pk=medicine_id)
            current_medicine_serialized = AvicennaMedicationsSerializer(current_medicine)
            return Response(current_medicine_serialized.data)
        except:
            return Response({"status": "hata"})


class LoadQuestionSet(APIView):
    def get(self, request, format=None):

        if "question_set" in request.query_params:

            question_set_no = request.query_params["question_set"]

            try:

                my_set = SymptomQuestionSet.objects.get(question_set_id=question_set_no)

                my_questions = SymptomQuestions.objects.filter(question_set=my_set)

                my_questions_serialized = SymptomQuestionSerializer(my_questions, many=True)

                for i in range(0, len(my_questions_serialized.data)):
                    my_questions_serialized.data[i]["pass"] = ""

                return Response(my_questions_serialized.data)

            except ObjectDoesNotExist:

                return Response({"error": "set yok"})

        else:
            return Response({"error": "hata"})


class LoadTableDataOfUser(APIView):
    def post(self, request, format=None):
        my_data = json.loads(json.dumps(request.data))

        my_token = ""

        if "token" in my_data:
            print(my_data["token"])
            my_token = my_data["token"]

        current_user_avicenna = models.User.objects.get(auth_token=my_token)

        current_user_avicenna = AvicennaUser.objects.get(username=current_user_avicenna.username)

        my_list = AvicennaUserComplaints.objects.filter(complaint_user=current_user_avicenna).all().order_by(
            'complaints_time')

        my_list_serialized = AvicennaUserComplaintSerializer(my_list, many=True)

        return Response(my_list_serialized.data)


class LoadAutoCompleteData(APIView):
    def get(self, request, format=None):

        try:
            names_list = SymptomQuestionSet.objects.filter(question_set_visible_to_search=True).all()[1:].values_list('question_set_name', flat=True)
            full_list = []
            autocomplete_ekstra = AutoCompleteAdditional.objects.filter().all().values_list('autocomplete_additionaltext', flat=True)

            for sentence in autocomplete_ekstra:
                full_list.append(sentence)


            
            for symptom_name in names_list:
                full_list.append(symptom_name)
                full_list.append("Bende " + symptom_name.lower() + " problemi var")
                full_list.append("Lenia " +  symptom_name.lower() + " problemim var")
                #full_list.append(symptom_name + " şikayetine ne iyi gelir🌱")

            return Response(full_list)
        except:
            return Response({"error": "hata"})


class LoadHealthAutoCompelte(APIView):
    def get(self, request, format=None):
        try:
            wiki_list = HealthWikiPage.objects.filter(visible_to_search=True).all()
            serialized_wiki_list = HealthWikiPageSerializer(wiki_list, many=True)
            print(serialized_wiki_list.data)
            # serialized_wiki_list.data.append("cenk")
            return Response(serialized_wiki_list.data)
        except:
            return Response({"status": "hata"})


class LoadMissingQuestionSet(APIView):
    def post(self, request, format=None):

        result_dict = {}

        my_data = json.loads(json.dumps(request.data))

        my_token = ""

        if "token" in my_data:
            print(my_data["token"])
            my_token = my_data["token"]

        print(my_token)

        current_user_avicenna = models.User.objects.get(auth_token=my_token)


        current_user_avicenna = AvicennaUser.objects.get(username=current_user_avicenna.username)



        current_user_sets_unfinished = AvicennaUserComplaints.objects.filter(complaints_end_time=None,
                                                                             complaint_user=current_user_avicenna).all()

        print(current_user_sets_unfinished)



        if current_user_avicenna.last_notification_seen == False:
            #notification_rest.sendNotification()

            daily_set = SymptomQuestionSet.objects.get(pk=6)
            daily_set_questions = SymptomQuestions.objects.filter(question_set=daily_set).order_by('question_id')
            current_set_questions_serialized = SymptomQuestionSerializer(daily_set_questions, many=True)
            result_dict[str(daily_set.question_set_id)] = current_set_questions_serialized.data
            current_user_avicenna.last_notification_seen = True
            current_user_avicenna.save()


        return Response(result_dict)


class RecordComplaintStatus(APIView):
    def post(self, request, format=None):

        my_data = json.loads(json.dumps(request.data))

        print(my_data)

        for complaint_no in my_data:
            print(complaint_no)
            current_user_complaint = AvicennaUserComplaints.objects.get(pk=int(complaint_no))
            current_sub_detail = AvicennaUserComplaintSubDetails()
            current_sub_detail.user_complaint = current_user_complaint
            for question_no in my_data[complaint_no]:
                # current_sub_detail.complaint_situation_progress = my_data[complaint_no][question_no]
                answer_content = SymptomAnswerKit.objects.get(pk=int(my_data[complaint_no][question_no])).answer_content
                current_sub_detail.complaint_situation_progress = answer_content
                print(answer_content)
                if "Bitti" in answer_content:
                    print(answer_content)
                    current_user_complaint.complaints_end_time = datetime.datetime.today()
                    current_user_complaint.save()

                # current_sub_detail.complaint_situation_progress = my_data[complaint_no][question_no]
                print(current_sub_detail.complaint_situation_progress)
                current_sub_detail.save()

        # print(my_data)

        return Response({})


class TestHealthWikiPage(APIView):
    def get(self, request, format=None):
        my_objects = HealthWikiPage.objects.all()
        print(my_objects)
        my_serialized_objects = HealthWikiPageSerializer(my_objects, many=True)
        return Response(my_serialized_objects.data)


class LoadNews(APIView):
    def get(self, request, format=None):
        #my_data = json.loads(request.body.decode("utf-8"))

        #current_user_avicenna = models.User.objects.get(auth_token=my_data["Token"])
        #current_user_avicenna = AvicennaUser.objects.get(username=current_user_avicenna.username)
        current_news = AvicennaNews.objects.filter().order_by('?')
        current_news_serialized = AvicennaNewSerializer(current_news, many=True)
        return Response(current_news_serialized.data)
        #else:
        #return Response({})


class LoadYazarNews(APIView):
    def post(self, request, format=None):
        current_news_yazar = AvicennaNews.objects.filter(news_category__category_id=4).order_by('?')[:3]
        current_news_yazar_serialized = AvicennaNewSerializer(current_news_yazar, many=True)
        return Response(current_news_yazar_serialized.data)


class UpdateUser(APIView):
    def post(self, request, format=None):
        my_data = json.loads(request.body.decode("utf-8"))
        print(my_data)
        if "token" in my_data:
            print("token var")
            my_token = my_data["token"]
            current_user_avicenna = models.User.objects.get(auth_token=my_token)
            current_user_avicenna = AvicennaUser.objects.get(username=current_user_avicenna.username)
            current_user_avicenna.smoking = my_data["smoking"]
            current_user_avicenna.gender = my_data["gender"]
            #current_user_avicenna.age = my_data["age"]

            current_user_avicenna.weight = my_data["weight"]
            current_user_avicenna.height = my_data["height"]
            full_name = my_data["name"].split(" ")
            first_name = " ".join(full_name[:len(full_name) - 1])
            last_name = full_name[len(full_name) - 1]
            current_user_avicenna.first_name = first_name
            current_user_avicenna.last_name = last_name
            current_user_avicenna.save()
            return Response({"status": "success"})
        else:
            return Response({"status": "no acess token"})


class UpdatePassword(APIView):
    def post(self, request, format=None):
        my_data = json.loads(request.body)
        print(my_data)
        if "token" in my_data:
            my_token = my_data["token"]
            current_user_avicenna = models.User.objects.get(auth_token=my_token)
            current_user_avicenna = AvicennaUser.objects.get(username=current_user_avicenna.username)
            my_oldpassword = my_data["oldpassword"]
            my_newpassword = my_data["newpassword"]
            if current_user_avicenna.check_password(my_oldpassword):
                current_user_avicenna.set_password(my_newpassword)
                current_user_avicenna.save()

                return Response({"status": "success"})
            else:
                return Response({"status": "fail"})
        else:
            return Response({"status": "fail"})


class UpdateCategories(APIView):
    def get(self, request, format=None):
        categories = AvicennaNewsCategory.objects.all()
        categories_serialized = AvicennaCategorySerializer(categories, many=True)
        return Response(categories_serialized.data)

    def post(self, request, format=None):
        my_data = json.loads(json.dumps(request.data))
        print(my_data)
        if "token" in my_data:
            my_token = my_data["token"]
            my_category_ids = my_data["categories"]
            current_user_avicenna = models.User.objects.get(auth_token=my_token)
            current_user_avicenna = AvicennaUser.objects.get(username=current_user_avicenna.username)
            current_user_avicenna.categories = []
            for id in my_category_ids:
                try:
                    current_user_avicenna.categories.add(AvicennaNewsCategory.objects.get(pk=id))
                    current_user_avicenna.save()
                except:
                    pass
            current_user_avicenna.save()

        return Response({})


class LoadUserCategories(APIView):
    def post(self, request, format=None):
        print("icindeyim")
        my_data = json.loads(json.dumps(request.data))
        print(my_data)
        if "token" in my_data:
            my_token = my_data["token"]
            current_user_avicenna = models.User.objects.get(auth_token=my_token)
            current_user_avicenna = AvicennaUser.objects.get(username=current_user_avicenna.username)
            his_categories = current_user_avicenna.categories.all()
            his_categories_serialized = AvicennaCategorySerializer(his_categories, many=True)
            print("sondayim")
            return Response(his_categories_serialized.data)
        return Response({})


def makeRedirect(request, format=None):
    if request.method == "GET":
        try:
            news_id = request.GET['id']
            print(news_id)
            page_url = AvicennaNews.objects.get(pk=news_id).news_link
            print(page_url)
            return redirect(page_url)
        except:
            return HttpResponse("not found")


def returnCategoryImage(request, format=None):
    if request.method == "GET":
        try:
            cat_id = request.GET['id']
            news_category = AvicennaNewsCategory.objects.get(pk=cat_id)
            return HttpResponse(news_category.category_image, content_type="image/png")
        except:
            return HttpResponse("not found")


def testView(request):
    return Response({})


class RecordUUID(APIView):
    def post(self, request):
        my_data = json.loads(json.dumps(request.data))
        print(my_data)
        try:
            if "token" in my_data:
                current_token = Token()
                current_token.token_content = my_data["token"]
                current_token.save()
                return Response({"status": "ok"})
        except:
            return Response({"status": "hata"})


class loadAutoCompleteNeIyiGelir(APIView):
        def get(self, request, format=None):

                symptom_list = SymptomQuestionSet.objects.filter(question_set_visible_to_search=True).all()
                full_list = []
                my_result_dict = {"results":[]}
                for symptom in symptom_list:
                    my_current_dict = {}
                    my_current_dict["text"] = symptom.question_set_name +  " şikayetine doğal ne iyi gelir makalesi 🌱"
                    my_current_dict["link"] = "https://www.lenia.ist/neiyigelir?sym_no=" + str(symptom.question_set_id)
                    my_result_dict["results"].append(my_current_dict)


                    my_current_dict_bitkisel = {}
                    my_current_dict_bitkisel["text"] = symptom.question_set_name +  " bitkisel tedavi 🌱"
                    my_current_dict_bitkisel["link"] = "https://www.lenia.ist/neiyigelir?sym_no=" + str(symptom.question_set_id)
                    my_result_dict["results"].append(my_current_dict_bitkisel)




                return Response(my_result_dict)


