# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-11-06 10:33
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('avicennarest', '0059_auto_20171102_1550'),
    ]

    operations = [
        migrations.AlterField(
            model_name='avicennamedicationuse',
            name='medication_use_time',
            field=models.DateTimeField(default=datetime.datetime(2017, 11, 6, 10, 33, 46, 946677)),
        ),
        migrations.AlterField(
            model_name='avicennausercomplaintsubdetails',
            name='complaint_situation_progress',
            field=models.CharField(blank=True, default='', max_length=200),
        ),
    ]
