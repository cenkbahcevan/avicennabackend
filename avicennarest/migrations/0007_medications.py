# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-08-07 18:02
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('avicennarest', '0006_auto_20170806_1940'),
    ]

    operations = [
        migrations.CreateModel(
            name='Medications',
            fields=[
                ('medication_id', models.AutoField(primary_key=True, serialize=False)),
                ('medication_name', models.CharField(max_length=200)),
                ('medication_content', models.TextField(default='', null=True)),
            ],
        ),
    ]
