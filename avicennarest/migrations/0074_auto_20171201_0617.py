# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-12-01 06:17
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('avicennarest', '0073_auto_20171129_1138'),
    ]

    operations = [
        migrations.AddField(
            model_name='symptomanswerkit',
            name='veriyfy_symptom',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='avicennamedicationuse',
            name='medication_use_time',
            field=models.DateTimeField(default=datetime.datetime(2017, 12, 1, 6, 17, 38, 783858)),
        ),
    ]
