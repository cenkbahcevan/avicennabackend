# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-11-28 13:39
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('avicennarest', '0070_auto_20171128_1338'),
    ]

    operations = [
        migrations.AlterField(
            model_name='avicennamedicationuse',
            name='medication_use_time',
            field=models.DateTimeField(default=datetime.datetime(2017, 11, 28, 13, 39, 20, 524374)),
        ),
    ]
