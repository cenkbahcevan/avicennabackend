# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-09-08 08:32
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('avicennarest', '0036_auto_20170908_0757'),
    ]

    operations = [
        migrations.AddField(
            model_name='avicennamedicationuse',
            name='medication_use_time',
            field=models.DateTimeField(default=datetime.datetime(2017, 9, 8, 8, 32, 57, 730668)),
        ),
    ]
