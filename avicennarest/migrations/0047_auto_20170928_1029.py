# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-09-28 10:29
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('avicennarest', '0046_auto_20170928_1024'),
    ]

    operations = [
        migrations.AlterField(
            model_name='avicennamedicationuse',
            name='medication_use_time',
            field=models.DateTimeField(default=datetime.datetime(2017, 9, 28, 10, 29, 23, 299875)),
        ),
        migrations.AlterField(
            model_name='avicennausercomplaints',
            name='complaints_end_time',
            field=models.DateTimeField(null=True),
        ),
    ]
