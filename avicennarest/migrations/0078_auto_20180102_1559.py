# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2018-01-02 12:59
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('avicennarest', '0077_auto_20171208_1233'),
    ]

    operations = [
        migrations.AddField(
            model_name='healthwikipage',
            name='page_content2',
            field=tinymce.models.HTMLField(default=''),
        ),
        migrations.AlterField(
            model_name='avicennamedicationuse',
            name='medication_use_time',
            field=models.DateTimeField(default=datetime.datetime(2018, 1, 2, 15, 59, 5, 819417)),
        ),
    ]
