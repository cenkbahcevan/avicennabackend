from django.shortcuts import render
import json
from rest_framework.decorators import api_view,permission_classes
from rest_framework.permissions import IsAuthenticated,AllowAny
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http.response import HttpResponse
import requests
import interaction
from .facebookmodels import FacebookUserModel,FacebookPassedQuestions,FacebookSymptomComplaint,FacebookNonExistingSetComplaint,FacebookNumbersCollected,BlockList,FacebookCounts
from .models import SymptomQuestionSet,SymptomQuestions,SymptomAnswerKit,AvicennaNews,AvicennaNewsCategory,HealthWikiPage,AvicennaMedications,PriceTagForDepartment,SimpleContextText,SampleText,GeneralText
import json
from . import facebookfunctions
from . import Diagnoser
from . import nlp_helper
from . import slackutils
from . import place_finder
from .models import AvicennaSentencesEntered,FixedAnswers
import time
#import datetime
from prespellchecker import prespellchecker, cleaner
from datetime import datetime, timedelta
from celery.decorators import task
from celery.utils.log import get_task_logger
from . import facebookinternational
from django.db.models import Q




    
cities = ["Adana","Adıyaman","Afyon","Ağrı","Amasya","Ankara","Antalya","Artvin","Aydın","Balıkesir","Bilecik","Bingöl","Bitlis","Bolu","Burdur","Bursa","Çanakkale","Çankırı","Çorum","Denizli","Diyarbakır","Edirne","Elazığ","Erzincan","Erzurum","Eskişehir","Gaziantep","Giresun","Gümüşhane","Hakkari","Hatay","Isparta","Mersin","İstanbul","İzmir","Kars","Kastamonu","Kayseri","Kırklareli","Kırşehir","Kocaeli","Konya","Kütahya","Malatya","Manisa","K.maraş","Mardin","Muğla","Muş","Nevşehir","Niğde","Ordu","Rize","Sakarya","Samsun","Siirt","Sinop","Sivas","Tekirdağ","Tokat","Trabzon","Tunceli","Şanlıurfa","Uşak","Van","Yozgat","Zonguldak","Aksaray","Bayburt","Karaman","Kırıkkale","Batman","Şırnak","Bartın","Ardahan","Iğdır","Yalova","Karabük","Kilis","Osmaniye","Düzce"]




logger = get_task_logger(__name__)


def hasNumbers(inputString):
    return any(char.isdigit() for char in inputString)

def checkCity(inputString):
    if len(inputString) > 1:
        lower_form = inputString[0].lower() + inputString[1:]
        lower_form_all = inputString.lower()
        capital_form = inputString[0].upper() + inputString[1:]
        capital_form_all = inputString.upper()
        deascii_form = inputString.replace("I", "İ")
        deascii_form2 = inputString.replace("C", "ç")
        deascii_form3_capital = inputString.replace("i", "İ")

        all_arr = [lower_form, lower_form_all, capital_form, capital_form_all, deascii_form, deascii_form2,
                   deascii_form3_capital]

        for element in all_arr:
            #print("Element:{}".format(element))
            if element in cities:
                return True

        words_splitted = inputString.split(" ")
        for word in words_splitted:
            lower_form = word[0].lower() + word[1:]
            lower_form_all = word.lower()
            capital_form = word[0].upper() + word[1:]
            capital_form_all = word.upper()
            deascii_form = word.replace("I", "İ")
            deascii7_form = word.replace("İ", "i")
            deascii_form2 = word.replace("C", "Ç")
            deascii_form3_capital = word.replace("i", "İ")
            deascii_form4 = word.replace("g","ğ").replace("G","Ğ")

            all_arr = [lower_form, lower_form_all, capital_form, capital_form_all, deascii_form, deascii_form2,
                       deascii_form3_capital,deascii_form4]
            for element in all_arr:
                if element in cities:
                    return True

    return False


def returnCityFromNumber(text):



    if len(text)<2 and text[0].isdigit():
        int_ver = int(text[0])
        return cities[int_ver - 1]
    elif len(text) > 1 and text[0] == '0' and text[1].isdigit():
        int_ver = int(text[1])
        return  cities[int_ver  - 1 ]
    elif len(text) >= 2 and text[0].isdigit() and text[1].isdigit():
        int_ver = int(text[0:2])
        if int_ver < 81:
            return cities[int_ver - 1]
        else:
            return  "no"
    else:
        return "no"



suffix_map = {
    "yan":"man",
    "ağrı":"nız",
    "şiş":"meniz",
    "ateş":"iniz",
    "kan":"amanız",
    "dön":"meniz",
    "bulantı":"nız",
    "ac":"ınız",
    "kus":"manız",
    "uyuş":"manız",
    "tık":"anmanız",
    "kıs":"ılmanız",
    "ak":"manız",
    "bayıl":"manız",
    "titr":"emeniz",
}

def returnOrganList(current_user):
    result_arr = []
    current_set_of_user = current_user.conversation_question_set
    current_json = json.loads(current_user.conversation_current_status)
    for set_id in current_json["all_question_sets"]:
        temp_dict = {}
        current_set = SymptomQuestionSet.objects.get(pk=int(set_id))
        temp_dict["organ"] = current_set.question_set_body_part
        temp_dict["affect"] = current_set.question_set_body_affect
        result_arr.append(temp_dict)
    return result_arr





def findClosestLink(que_id,organ_list):
    try:
        selected_res = SymptomQuestions.objects.filter(pk=que_id.question_id).all()
        ans = None
        result = None
        while len(selected_res)>0:
            print("while icindeyim")
            if selected_res[0] != None:
                ans = selected_res[0].answers.all()[0]
                for element in organ_list:
                        current_que = SymptomQuestions.objects.get(pk=ans.selected_que_id)
                        new_ans = current_que.answers.get(veriyfy_symptom=True)
                        print("current que:{} {} {}".format(current_que, organ_list,new_ans))
                        if current_que.question_organ == element["organ"] and current_que.question_affect == element["affect"]:
                            result = SymptomQuestions.objects.get(pk=new_ans.selected_que_id)
                print("henuz cokmedim 1 {}".format(ans.selected_que_id))
                selected_res =  SymptomQuestions.objects.filter(pk=ans.selected_que_id).all()
            else:
                selected_res = None
        return result
    except:
        print("exceptten none")
        return result



def checkQuestionIfItCanBePassable(current_user,question):
    print(question)
    my_pass_que = current_user.passed_questions.all()
    for element in my_pass_que:
        if element.que == question.que:
            try:
                return question.answers.get(answer_content=element.answer.answer_content)
            except:
                return question.answers.all()[0]
    return False


def sendExtraInfoDaily():
    current_user_list = FacebookUserModel.objects.all()

    our_today = datetime.today()

    our_day = our_today.day
    our_month = our_today.month

    for current_user in current_user_list:

        time.sleep(2)

        if current_user.last_disease_entered != None:

            current_disease = current_user.last_disease_entered

            current_extras = current_disease.page_extra_link.all()

            if current_user.last_disease_entered_day + 1 == our_day and current_user.last_disease_entered_month == our_month:
                if current_extras.count()>0:
                    facebookfunctions.sendMessage(current_disease.page_title + "hakkında ilginizi "
                                                                               "çekebilecek bilgileri sizin için araştırdım",current_user.conversation_user)
                    for extra in current_extras:
                        facebookfunctions.sendDirectLink(extra,current_user.conversation_user)








@task(max_retries=3)
def sendRealAnswer(sender_id_int,current_sentence):
    # idsinden kullanıcıyı yaratıyorum ve get ediyorum.


    my_file = open("ellogu.txt","w+")

    print("Ben basladim")
    logger.info("Ben basladim")
    my_file.write("Ben basladim")
    current_user = facebookfunctions.construct_or_get_user(sender_id_int)

    organListOfUser = returnOrganList(current_user)

    print("Current sentence bak:{}".format(current_sentence))

    #my_file.write("Sentence:{}".format(current_sentence))



    #self.last_sentence = current_sentence

    sentence_to_be_recorded = AvicennaSentencesEntered()
    sentence_to_be_recorded.sentence_text = current_sentence
    sentence_to_be_recorded.save()

    current_user.last_entry_date = datetime.today()

    if current_sentence == "Lenia, ne iyi gelir🌱":
        my_message = current_user.last_set_entry.question_set_whateatdrink
        if len(my_message) > 1 and current_user.last_set_entry.question_set_id != 106   :
            facebookfunctions.sendMessage(my_message, sender_id_int)
        elif len(my_message) > 1:
            facebookfunctions.sendMessage("->" + my_message, sender_id_int)
            facebookfunctions.sendProductInfo(sender_id_int,
                            "Zade Vital Biotin saç için",
                            "Saç için ısırgan otu, yetişkinler için günde 1 kapsül tüketilmesi tavsiye edilir",
                            "https://www.vitaminler.com/urun/zade-vital-biotin-2500-mcg-30-kapsul-8246",
                            "https://vitaminler.mncdn.com/Assets/Supplementler/Thumbs/zade_vital_biotin_2500_mcg_30_kapsul_15992.jpeg")



        else:
            facebookfunctions.sendMessage("Henüz verim bu konuda yok.", sender_id_int)

        return

    # kullanıcının kayıtlı bir soru seti varsa, yani bir soruya cevap bekliyorsak
    if current_user.conversation_question_set != None:

        # kullanıcının soru idsini alıyoruz.
        current_que = SymptomQuestions.objects.get(question_id=current_user.conversation_question_no)

        # bu soru eger etki sorusu ise, ilk cevabi gecis farzediyoruz
        if current_que.question_choices == "E":
            facebookfunctions.answerAffectTypeOfQuestion(current_sentence, sender_id_int, current_user, current_que)
            return Response({})

        current_que_id = current_user.conversation_question_no

        print(current_que_id)
        # print(current_sentence)

        current_answer = None

        # cevaptan gelecek soruya karar veriyoruz
        try:
            # soru setinde , kullanıcının kaldığı cevaplardan kullanıcınnı verdigi cevabı çekmeyr çalış

            if current_sentence == "Konuşmayı bitir":
                facebookfunctions.allQuestionSetsAreFinished(current_user)
                return

            if current_que.question_choices == 'D' and SymptomAnswerKit.objects.filter(
                    question_belongs__question_id=current_que_id, answer_content__iexact=current_sentence).count() < 1:
                print("ife girdim")
                current_sentence = facebookfunctions.checkDate(current_que.answers.all(), current_sentence)
                print("New current sentence:{}".format(current_sentence))

            if current_que.question_choices == 'A' and SymptomAnswerKit.objects.filter(
                    question_belongs__question_id=current_que_id, answer_content__iexact=current_sentence).count() < 1:
                print("ife girdim")
                current_sentence = facebookfunctions.checkAmountAnswer(current_que.answers.order_by('int_value').all(),
                                                                       current_sentence)
                print("New current sentence:{}".format(current_sentence))

            if current_que.question_choices == 'C':

                plaka = returnCityFromNumber(current_sentence)

                if plaka != "no":
                    current_user.user_city = plaka
                    current_user.save()
                    current_sentence = SymptomQuestions.objects.get(pk=current_que_id).answers.all()[0].answer_content

                elif checkCity(current_sentence) == False:
                    facebookfunctions.sendMessage("Cevabını anlamadım. Bulunduğun yeri daha net ifade eder misin?",
                                                  sender_id_int)
                    return

                else:
                    current_user.user_city = current_sentence
                    current_user.save()
                    current_sentence = SymptomQuestions.objects.get(pk=current_que_id).answers.all()[0].answer_content

            if current_que.question_choices == 'T':

                if current_user.conversation_question_set.question_set_id == 107:

                    if len(current_sentence) >= 10 and len(current_sentence) < 150 and hasNumbers(current_sentence):
                        current_user.user_telephone = current_sentence

                        slack_txt = current_user.name + " -> " + current_user.user_city + " -> " + current_sentence + " -> " +  current_user.last_set_entry.question_set_full_name 

                        slackutils.sendTextToChannel(slack_txt,"#numaralar")
                        current_user.save()

                        my_number_collection_object = FacebookNumbersCollected()
                        my_number_collection_object.number_user = current_user
                        my_number_collection_object.number_text = current_sentence
                        my_number_collection_object.number_adress = current_user.user_city
                        my_number_collection_object.number_date = datetime.now()
                        my_number_collection_object.save()

                    try:
                        my_set_count = FacebookCounts.objects.get(title="telno")
                        my_set_count.value = my_set_count.value + 1
                        my_set_count.save()
                    except:
                        pass

                    # current_sentence = SymptomQuestions.objects.get(pk=current_que_id).answers.all()[0].answer_content

                    facebookfunctions.sendMessage(
                        "Verileriniz bizim ile güvendedir. Paylaştığınız için çok teşekkür ederiz", sender_id_int)

                    diagnosis_results = Diagnoser.diagnoseUser(current_user, "")
                    facebookfunctions.sendResults(diagnosis_results, sender_id_int)
                    facebookfunctions.sendExtraSetInfos(current_user)

                    facebookfunctions.allQuestionSetsAreFinished(current_user)

                    return

            current_answer = None

            try:
                current_answer = SymptomAnswerKit.objects.get(
                    question_belongs__question_id=current_que_id,
                    answer_content__iexact=nlp_helper.checkAnswerSynonym(current_sentence))

            except:
                pass

            if current_answer == None:
                current_answer = SymptomAnswerKit.objects.get(
                    question_belongs__question_id=current_que_id,
                    answer_content__iexact=current_sentence)

            if current_que.que != None:
                print("if icindeyim")
                current_passed_question = FacebookPassedQuestions()
                current_passed_question.que = current_que.que
                current_passed_question.answer = current_answer
                current_passed_question.save()
                current_user.passed_questions.add(current_passed_question)

            facebookfunctions.updateAnswersOfMUltiOptionJson(current_user, current_answer)

            print("henuz cokmedim", current_answer.activate.values_list())

            activation_list = current_answer.activate.values_list()

            print(activation_list.count())

            # o cevabin bir activationı varsa
            if activation_list.count() > 0:
                for temp_set in activation_list:
                    print(current_user.conversation_current_status)
                    current_json = json.loads(current_user.conversation_current_status)
                    # bu activation edilecek setlere ekle
                    if temp_set[0] not in current_json["all_question_sets"]:
                        current_json["question_sets"] = [temp_set[0]] + current_json["question_sets"]
                        current_json["all_question_sets"] = [temp_set[0]] + current_json["all_question_sets"]
                        current_user.conversation_current_status = json.dumps(current_json)

        except:

            current_que = SymptomQuestions.objects.get(question_id=
                                                       current_user.conversation_question_no)

            if current_que.does_not_require_answer == True:

                current_que_answers = SymptomAnswerKit.objects.filter(
                    question_belongs_id=current_user.conversation_question_no).order_by(
                    'answer_content').all()

                print("gor bu 1")
                current_json = json.loads(current_user.conversation_current_status)

                current_answer_selected = current_que_answers[0]

                if current_user.conversation_question_set.question_set_id == 6:
                    # facebookfunctions.sendVideoIfExists(current_user, current_user.conversation_question_set)
                    facebookfunctions.allQuestionSetsAreFinished(current_user)
                    # self.classicalNLPAnalysis(current_user, current_sentence)

                    # facebookfunctions.allQuestionSetsAreFinished(current_user)
                    return

                if current_answer_selected.selected_que != None:
                    print("gor bu 2")

                    next_question = current_answer_selected.selected_que
                    current_user.conversation_question_no = next_question.question_id
                    current_user.save()
                    if len(next_question.answers.all()) > 0:
                        facebookfunctions.sendQuickReply(next_question.question_content, current_user.conversation_user,
                                                         next_question.answers.all())
                    else:
                        facebookfunctions.sendMessage(next_question.question_content, current_user.conversation_user)
                    return Response({})

                elif len(current_json[
                             "question_sets"]) < 1 and current_user.conversation_question_set.question_set_type == 'D':
                    print("henuz cokmedim")

                    print("classical :{}".format(current_sentence))
                    facebookfunctions.allQuestionSetsAreFinished(current_user)
                    FacebookView.classicalNLPAnalysis(current_user, current_sentence)

                    # facebookfunctions.allQuestionSetsAreFinished(current_user)
                    return Response({})

                elif len(current_json["question_sets"]) < 1:
                    print("henuz cokmedim")

                    print("classical :{}".format(current_sentence))
                    FacebookView.whenCurrentSetOver(current_user, sender_id_int)

                    # facebookfunctions.allQuestionSetsAreFinished(current_user)
                    return Response({})

            current_que_content = SymptomQuestions.objects.get(question_id=
                                                               current_user.conversation_question_no).question_content

            current_que_answers = SymptomAnswerKit.objects.filter(
                question_belongs_id=current_user.conversation_question_no).order_by('answer_content').all()

            print("buradayim")

            if len(current_que_answers) > 0:

                facebookfunctions.sendQuickReply(
                    "Cevabını anlamadım " + current_user.name + ". Bu soruyu şıklardan seçmeniz benim anlamam adına çok daha iyi olur veya bu şikayetinizle ilgili soru cevaplamamamk için konuşmayı bitirebilirsiniz." +
                    current_que_content, sender_id_int, current_que_answers, anlamadim=True)


            elif current_que.question_set.question_set_type == 'D':
                # facebookfunctions.sendMessage("Bitti",sender_id_int)
                # facebookfunctions.sendVideoIfExists(current_user.conversation_user,
                #                                    current_user.last_set_entry)
                facebookfunctions.allQuestionSetsAreFinished(current_user)
            return Response({})

        print("coktum")

        # kullanicinin sectigi cevap baska soruya bağlanıyorsa, ama siddetle ilgili degilse

        if current_answer.selected_que != None:

            print("cevap geld {0}:".format(current_sentence))

            is_it_passable = checkQuestionIfItCanBePassable(current_user, current_answer.selected_que)

            if type(is_it_passable) != bool and is_it_passable.selected_que == None:
                facebookfunctions.updateAnswersJson(current_user, is_it_passable)
                print("buradayim")
                FacebookView.whenCurrentSetOver(current_user, sender_id_int)
                return Response({})
            elif type(is_it_passable) != bool:
                print("is it passable:{}".format(is_it_passable))
                # facebookfunctions.updateAnswersJson(current_user,is_it_passable)
                current_user.conversation_question_no = is_it_passable.selected_que_id
                current_user.save()
            else:
                current_user.conversation_question_no = current_answer.selected_que_id
                current_user.save()

            """
            cevabı alma vakti
            current_answer  = SymptomAnsw   erKit.objects.get(question_belongs=)
            """
            # yeni sorunnu cevaplarini cek

            answers = SymptomAnswerKit.objects.filter(
                question_belongs__question_id=current_user.conversation_question_no).all()
            # yeni sorunun metnini cek

            next_que = SymptomQuestions.objects.get(question_id=current_user.conversation_question_no)
            next_que_content = next_que.question_content

            current_set_organ = current_user.conversation_question_set.question_set_body_part
            current_set_affect = current_user.conversation_question_set.question_set_body_affect

            not_found_flag = True
            if next_que.question_choices == "P":
                current_sentence_json = json.loads(current_user.conversation_last_sentence)

                for organ in current_sentence_json:
                    for affect in current_sentence_json[organ]:
                        print("organ:{} affect:{}".format(organ, affect))
                        if organ != current_set_organ and affect != current_set_affect:
                            if next_que.question_organ == organ and next_que.question_affect == affect:
                                not_found_flag = False
                                next_ans = next_que.answers.get(veriyfy_symptom=True)
                                """
                                1-yollanacak yeni soru olabilir
                                2-aktif olmayan set olmayabilir
                                """
                                current_json = json.loads(current_user.conversation_current_status)

                                # question set listemde baska soru seti idsi var mı
                                that_que = findClosestLink(current_answer.question_belongs,
                                                           organListOfUser)
                                print("that que:{}".format(that_que))

                                facebookfunctions.applyPassableQuestionAdressing(that_que, sender_id_int, current_user,
                                                                                 current_json)
                                return Response({})

                if not_found_flag:
                    answers = SymptomAnswerKit.objects.filter(
                        question_belongs__question_id=next_que.question_id).order_by('answer_content').all()

                    print(answers)

                    if len(answers) > 0:
                        facebookfunctions.sendQuickReply(next_que_content, sender_id_int,
                                                         answers)





            elif next_que.question_choices == "E":

                facebookfunctions.sendMessage(next_que_content, sender_id_int)
                return Response({})





            else:
                print("bu else icindeyim")

                answers = SymptomAnswerKit.objects.filter(
                    question_belongs__question_id=next_que.question_id).order_by('answer_content').all()

                print(answers)

                if len(answers) > 0:

                    facebookfunctions.sendQuickReply(next_que_content, sender_id_int, answers)
                elif next_que.question_set.question_set_type == 'D':
                    facebookfunctions.sendMessage(next_que_content, sender_id_int)
                    # facebookfunctions.sendVideoIfExists(current_user.conversation_user,current_user.last_set_entry)
                    facebookfunctions.allQuestionSetsAreFinished(current_user)
                else:
                    facebookfunctions.sendMessage(next_que_content, sender_id_int)
                    return Response({})
                    # facebookfunctions.allQuestionSetsAreFinished(current_user)
            return Response({})


        # kullanicinin sectigi sorunun devami yok, o set bitti
        else:
            FacebookView.whenCurrentSetOver(current_user, sender_id_int)



    # bu else, kullanıcının şu an hiç bir şikayete, soru setine bapı bağlı olmadığını, yeni şikayet metni girdiğin
    else:

        print("Else:{}".format(current_sentence))


        current_user.conversation_last_sentence_text = current_sentence
        current_user.save()

        if len(current_sentence) < 3:
            if "😘" in current_sentence or "😍" in current_sentence:
                facebookfunctions.sendMessage("😘😘", sender_id_int)
                return Response({})
            elif current_sentence[0] in "😀😬😁😂😃😄😅😆😇😉😊🙂😋😌😙😜😝😛😎😏😶😐😑😒😡😓😪😢😥😖😩😤":
                facebookfunctions.sendMessage(current_sentence, sender_id_int)
                return HttpResponse({})

        # Java'ya yolla cevabı al

        if facebookfunctions.checkIfGenelChitQuestion(current_user, current_sentence):
            return Response({})

        current_sentence_fixed = prespellchecker.djangoFixSentence(current_sentence)

        current_sentence_fixed, _ = prespellchecker.stemSentence(current_sentence_fixed)

        

        print("Res öncesi")
        my_file.write("Res oncesi\n")
        logger.info("Res öncesi\n")
        res = interaction.symptom_extraction_connection.sendDataGetResponse(current_sentence_fixed)
        print("Res{}".format(res))
        logger.info("Res sonrası")
        my_file.write("Res sonrasi{}".format(res))


        if current_user.last_context == 'SF' and current_sentence.lower() in ["anlamadım", "ne", "peki", "yani",
                                                                              "ne yapmalıyım", "yaani", "ee",
                                                                              "ne yapacağım", "anlamadim", "sorunum ne",
                                                                              "sorun ne", "bu kadar mı"]:
            facebookfunctions.sendMessage(
                "Yukarıda senin için hazırladığım liste senin cevaplarına göre eşleşen hastalıklardır. Hastalıkların üstüne tıklayarak bilgi alabilirsin. Ne yapman gerektiğini söyleyemem ",
                sender_id_int)
            return Response({})

        current_user.last_context = 'NI'
        current_user.save()

        # bunu cevir

        """
        tek tek yeni soru setini cekiyor
        """
        try:

            res_json = json.loads(res.decode('utf-8'))

            print(res)

            my_file.write("Json hali{}\n".format(res_json))

            # soruları yüklüyoe kullanıcıya ve soru setini getiryior
            print("analyze oncesi")

            current_user.message_no_of_his = current_user.message_no_of_his + 1
            current_user.save()

            answers = FacebookView.analyzeTextOfUserAndSetContent(res_json, current_user)

            my_file.write("analyze sonrasi \n")

            print("analyze sonrasi")

            if answers == "tesekkur" or answers == "selam" or answers == "noset" \
                    or answers == "anladim" or answers == "randevu" \
                    or answers == "yardim" or answers == "personal" \
                    or answers == "veda" or answers == "cinsel" or answers == "kilo" or answers == "ilac" or answers == "psikoloji":

                    return



            my_file.write("soru cekilecek")
            symptom_questions_current = SymptomQuestions.objects.get(question_id=current_user.conversation_question_no)

            infos = symptom_questions_current.question_informations.all()
            print("infos:{}".format(infos))

            current_user.dontunderstand_no = 0
            # current_user.last_disease_entered = ""
            current_user.save()

            facebookfunctions.sendMessage(
                "Şikayetinizi girdiğiniz için teşekkür ederim." + facebookfunctions.prepareSentenceForUnderstanding(
                    current_user) + " Sana bir kaç sorum olacak 🚑", sender_id_int)

            facebookfunctions.sendTypingView(sender_id_int)

            for info in infos:
                facebookfunctions.sendMessage(info.symptomquestioninfo_value, sender_id_int)

            facebookfunctions.sendQuickReply(symptom_questions_current.question_content, sender_id_int, answers)


        except:

            current_sentence_fixed = prespellchecker.djangoFixSentence(current_sentence)

            # print("exceptteyim daha yollamadim {}".format(current_sentence_fixed))

            current_sentence_stemmed, _ = prespellchecker.stemSentence(current_sentence_fixed)

            facebookfunctions.loadPossibleMedicationOrWikiOrDontUnderstandText(current_sentence_stemmed,
                                                                               current_user.conversation_user,
                                                                               current_user, extra_param=_)


def sendRealAnswer2(sender_id_int,current_sentence,page_source):
    # idsinden kullanıcıyı yaratıyorum ve get ediyorum.


    my_file = open("ellogu.txt","w+")

    my_view = FacebookView()

    print("Ben basladim")
    #logger.info("Ben basladim")
    my_file.write("Ben basladim")
    current_user = facebookfunctions.construct_or_get_user(sender_id_int)
    

    organListOfUser = returnOrganList(current_user)

    print("Current sentence bak:{}".format(current_sentence))

    #my_file.write("Sentence:{}".format(current_sentence))



    #self.last_sentence = current_sentence

    sentence_to_be_recorded = AvicennaSentencesEntered()
    sentence_to_be_recorded.sentence_text = current_sentence
    sentence_to_be_recorded.save()


    if "korona" in current_sentence.lower() or "corona" in current_sentence.lower():

                            current_message = """
                                Enfekte olmuş kişilerle yakın temastan kaçınmanın,
                                El hijyenine dikkat etmenin, sık aralıklarla elleri en az 20 saniye sabun ve su ile yıkamanın; sabun ve su olmadığı durumlarda alkol bazlı el antiseptiği kullanmanın; özellikle hasta insanlar veya çevresi ile doğrudan temas ettikten sonra elleri mutlaka yıkamanın,
                                Çiftlik veya vahşi hayvanlarla korunmasız temastan kaçınmanın,
                                Enfekte olduysanız eğer, mesafeyi korumanın, öksürürken, hapşırırken tek kullanımlık kağıt mendil ile ağızın ve burnun kapatılmasının; kağıt mendilin bulunmadığı durumlarda ise dirsek içinin kullanılmasının, ellerin yıkanmasının; gözlerinize, burnunuza ve ağzınıza dokunmaktan kaçınmanın,
                                Enfekte olan kişilerin dokunduğu yüzeylerin dezenfekte edilmesinin,
                                Et, yumurta gibi hayvansal gıdaların iyice pişirilmesinin,
                                Hasta kişilerin mümkünse kalabalık yerlere girmemesinin, eğer girmek zorunda kalınıyorsa ağız ve burnun kapatılmasının, mümkünse tıbbi maske kullanılmasının önemli olduğunu belirtmiştir.
                                """
                            facebookfunctions.sendMessage(current_message,sender_id_int)
                            return HttpResponse({})



    if current_sentence == "Lenia, ne iyi gelir🌱":
        my_message = current_user.last_set_entry.question_set_whateatdrink
        if len(my_message) > 1 and current_user.last_set_entry.question_set_id != 106:
            facebookfunctions.sendMessage(my_message, sender_id_int,page_source)
        elif len(my_message) > 1:
            facebookfunctions.sendMessage(my_message, sender_id_int,page_source)
            facebookfunctions.sendProductInfo(sender_id_int,
                            "Zade Vital Biotin saç için",
                            "Saç için ısırgan otu, yetişkinler için günde 1 kapsül tüketilmesi tavsiye edilir",
                            "https://www.vitaminler.com/urun/zade-vital-biotin-2500-mcg-30-kapsul-8246",
                            "https://vitaminler.mncdn.com/Assets/Supplementler/Thumbs/zade_vital_biotin_2500_mcg_30_kapsul_15992.jpeg")


            
        else:
            facebookfunctions.sendMessage("Henüz verim bu konuda yok.", sender_id_int,page_source)

        return

    # kullanıcının kayıtlı bir soru seti varsa, yani bir soruya cevap bekliyorsak
    if current_user.conversation_question_set != None and facebookfunctions.endableUser(current_user,current_sentence) == False:


        current_user.last_entry_date = datetime.today()
        current_user.save()

        # kullanıcının soru idsini alıyoruz.
        current_que = SymptomQuestions.objects.get(question_id=current_user.conversation_question_no)

        # bu soru eger etki sorusu ise, ilk cevabi gecis farzediyoruz
        if current_que.question_choices == "E":
            facebookfunctions.answerAffectTypeOfQuestion(current_sentence, sender_id_int, current_user, current_que)
            return Response({})

        current_que_id = current_user.conversation_question_no

        print(current_que_id)
        # print(current_sentence)

        current_answer = None

        # cevaptan gelecek soruya karar veriyoruz
        try:
            # soru setinde , kullanıcının kaldığı cevaplardan kullanıcınnı verdigi cevabı çekmeyr çalış

            if current_sentence == "Konuşmayı bitir":
                facebookfunctions.allQuestionSetsAreFinished(current_user)
                return HttpResponse({})
            
            

            if current_que.question_choices == 'D' and SymptomAnswerKit.objects.filter(
                    question_belongs__question_id=current_que_id, answer_content__iexact=current_sentence).count() < 1:
                print("ife girdim")
                current_sentence = facebookfunctions.checkDate(current_que.answers.all(), current_sentence)
                print("New current sentence:{}".format(current_sentence))

            if current_que.question_choices == 'A' and SymptomAnswerKit.objects.filter(
                    question_belongs__question_id=current_que_id, answer_content__iexact=current_sentence).count() < 1:
                print("ife girdim")
                current_sentence = facebookfunctions.checkAmountAnswer(current_que.answers.order_by('int_value').all(),
                                                                       current_sentence)
                print("New current sentence:{}".format(current_sentence))

            if current_que.question_choices == 'C':

                plaka = returnCityFromNumber(current_sentence)

                if plaka != "no":
                    current_user.user_city = plaka
                    current_user.save()
                    current_sentence = SymptomQuestions.objects.get(pk=current_que_id).answers.all()[0].answer_content

                elif checkCity(current_sentence) == False:
                    facebookfunctions.sendMessage("Cevabını anlamadım. Bulunduğun yeri daha net ifade eder misin?",
                                                  sender_id_int)
                    return

                else:
                    current_user.user_city = current_sentence
                    current_user.save()
                    current_sentence = SymptomQuestions.objects.get(pk=current_que_id).answers.all()[0].answer_content

            if current_que.question_choices == 'T':

                if current_user.conversation_question_set.question_set_id == 107:

                    if len(current_sentence) >= 10 and len(current_sentence) < 150 and hasNumbers(current_sentence):
                        current_user.user_telephone = current_sentence

                        slack_txt = current_user.name + " -> " + current_user.user_city + " -> " + current_sentence + " -> " +  current_user.last_set_entry.question_set_full_name 

                        slackutils.sendTextToChannel(slack_txt,"#numaralar")

                        current_user.save()

                        my_number_collection_object = FacebookNumbersCollected()
                        my_number_collection_object.number_user = current_user
                        my_number_collection_object.number_text = current_sentence
                        my_number_collection_object.number_adress = current_user.user_city
                        my_number_collection_object.number_date = datetime.now()
                        my_number_collection_object.save()

                    # current_sentence = SymptomQuestions.objects.get(pk=current_que_id).answers.all()[0].answer_content

                    facebookfunctions.sendMessage(
                        "Verileriniz bizim ile güvendedir. Paylaştığınız için çok teşekkür ederiz", sender_id_int,page_source)

                    diagnosis_results = Diagnoser.diagnoseUser(current_user, "")
                    facebookfunctions.sendResults(diagnosis_results, sender_id_int)
                    facebookfunctions.sendExtraSetInfos(current_user)

                    facebookfunctions.allQuestionSetsAreFinished(current_user)

                    return

            current_answer = None

            try:
                current_answer = SymptomAnswerKit.objects.get(
                    question_belongs__question_id=current_que_id,answer_content__iexact=nlp_helper.checkAnswerSynonym(current_sentence))

            except:
                pass

            if current_answer == None:
                current_answer = SymptomAnswerKit.objects.get(Q(
                question_belongs__question_id=current_que_id,
                answer_content__iexact=current_sentence))


            if current_que.que != None:
                print("if icindeyim")
                current_passed_question = FacebookPassedQuestions()
                current_passed_question.que = current_que.que
                current_passed_question.answer = current_answer
                current_passed_question.save()
                current_user.passed_questions.add(current_passed_question)

            #soruyu kaydediyoruz
            if current_que.question_recordable:
                pass
                #my_recordable = FacebookQuestionAnswerUserRecorded()
                #my_recordable.answer_user = current_user
                #my_recordable.answer_question = current_que
                #my_recordable.answer_answer = current_answer
                #my_recordable.save()

            facebookfunctions.updateAnswersOfMUltiOptionJson(current_user, current_answer)

            print("henuz cokmedim", current_answer.activate.values_list())

            activation_list = current_answer.activate.values_list()

            print(activation_list.count())

            # o cevabin bir activationı varsa
            if activation_list.count() > 0:
                for temp_set in activation_list:
                    print(current_user.conversation_current_status)
                    current_json = json.loads(current_user.conversation_current_status)
                    # bu activation edilecek setlere ekle
                    if temp_set[0] not in current_json["all_question_sets"]:
                        current_json["question_sets"] = [temp_set[0]] + current_json["question_sets"]
                        current_json["all_question_sets"] = [temp_set[0]] + current_json["all_question_sets"]
                        current_user.conversation_current_status = json.dumps(current_json)

        except:

            current_que = SymptomQuestions.objects.get(question_id=
                                                       current_user.conversation_question_no)

            if current_que.does_not_require_answer == True:

                current_que_answers = SymptomAnswerKit.objects.filter(
                    question_belongs_id=current_user.conversation_question_no).order_by(
                    'answer_content').all()

                print("gor bu 1")
                current_json = json.loads(current_user.conversation_current_status)

                current_answer_selected = current_que_answers[0]

                if current_user.conversation_question_set.question_set_id == 6:
                    # facebookfunctions.sendVideoIfExists(current_user, current_user.conversation_question_set)
                    facebookfunctions.allQuestionSetsAreFinished(current_user)
                    # self.classicalNLPAnalysis(current_user, current_sentence)

                    # facebookfunctions.allQuestionSetsAreFinished(current_user)
                    return

                if current_answer_selected.selected_que != None:
                    print("gor bu 2")

                    next_question = current_answer_selected.selected_que
                    current_user.conversation_question_no = next_question.question_id
                    current_user.save()
                    if len(next_question.answers.all()) > 0:
                        facebookfunctions.sendQuickReply(next_question.question_content, current_user.conversation_user,
                                                         next_question.answers.all())
                    else:
                        facebookfunctions.sendMessage(next_question.question_content, current_user.conversation_user)
                    return

                elif len(current_json[
                             "question_sets"]) < 1 and current_user.conversation_question_set.question_set_type == 'D':
                    print("henuz cokmedim")

                    print("classical :{}".format(current_sentence))
                    facebookfunctions.allQuestionSetsAreFinished(current_user)
                    my_view.classicalNLPAnalysis(current_user, current_sentence)

                    # facebookfunctions.allQuestionSetsAreFinished(current_user)
                    return

                elif len(current_json["question_sets"]) < 1:
                    print("henuz cokmedim")

                    print("classical :{}".format(current_sentence))
                    my_view.whenCurrentSetOver(current_user, sender_id_int)

                    # facebookfunctions.allQuestionSetsAreFinished(current_user)
                    return

            current_que_content = SymptomQuestions.objects.get(question_id=
                                                               current_user.conversation_question_no).question_content

            current_que_answers = SymptomAnswerKit.objects.filter(
                question_belongs_id=current_user.conversation_question_no).order_by('answer_content').all()

            print("buradayim")

            if len(current_que_answers) > 0:

                facebookfunctions.sendQuickReply(
                    "Cevabını anlamadım " + current_user.name + ". Bu soruyu şıklardan seçmeniz benim anlamam adına çok daha iyi olur veya bu şikayetinizle ilgili soru cevaplamamamk için konuşmayı bitirebilirsiniz." +
                    current_que_content, sender_id_int, current_que_answers, anlamadim=True)


            elif current_que.question_set.question_set_type == 'D':
                # facebookfunctions.sendMessage("Bitti",sender_id_int)
                # facebookfunctions.sendVideoIfExists(current_user.conversation_user,
                #                                    current_user.last_set_entry)
                facebookfunctions.allQuestionSetsAreFinished(current_user)
            return

        print("coktum")

        # kullanicinin sectigi cevap baska soruya bağlanıyorsa, ama siddetle ilgili degilse

        if current_answer.selected_que != None:

            print("cevap geld {0}:".format(current_sentence))

            is_it_passable = checkQuestionIfItCanBePassable(current_user, current_answer.selected_que)

            if type(is_it_passable) != bool and is_it_passable.selected_que == None:
                facebookfunctions.updateAnswersJson(current_user, is_it_passable)
                print("buradayim")
                my_view.whenCurrentSetOver(current_user, sender_id_int)
                return Response({})
            elif type(is_it_passable) != bool:
                print("is it passable:{}".format(is_it_passable))
                # facebookfunctions.updateAnswersJson(current_user,is_it_passable)
                current_user.conversation_question_no = is_it_passable.selected_que_id
                current_user.save()
            else:
                current_user.conversation_question_no = current_answer.selected_que_id
                current_user.save()

            """
            cevabı alma vakti
            current_answer  = SymptomAnsw   erKit.objects.get(question_belongs=)
            """
            # yeni sorunnu cevaplarini cek

            answers = SymptomAnswerKit.objects.filter(
                question_belongs__question_id=current_user.conversation_question_no).all()
            # yeni sorunun metnini cek

            next_que = SymptomQuestions.objects.get(question_id=current_user.conversation_question_no)
            next_que_content = next_que.question_content

            current_set_organ = current_user.conversation_question_set.question_set_body_part
            current_set_affect = current_user.conversation_question_set.question_set_body_affect

            not_found_flag = True
            if next_que.question_choices == "P":
                current_sentence_json = json.loads(current_user.conversation_last_sentence)

                for organ in current_sentence_json:
                    for affect in current_sentence_json[organ]:
                        print("organ:{} affect:{}".format(organ, affect))
                        if organ != current_set_organ and affect != current_set_affect:
                            if next_que.question_organ == organ and next_que.question_affect == affect:
                                not_found_flag = False
                                next_ans = next_que.answers.get(veriyfy_symptom=True)
                                """
                                1-yollanacak yeni soru olabilir
                                2-aktif olmayan set olmayabilir
                                """
                                current_json = json.loads(current_user.conversation_current_status)

                                # question set listemde baska soru seti idsi var mı
                                that_que = findClosestLink(current_answer.question_belongs,
                                                           organListOfUser)
                                print("that que:{}".format(that_que))

                                facebookfunctions.applyPassableQuestionAdressing(that_que, sender_id_int, current_user,
                                                                                 current_json)
                                return

                if not_found_flag:
                    answers = SymptomAnswerKit.objects.filter(
                        question_belongs__question_id=next_que.question_id).order_by('answer_content').all()

                    print(answers)

                    if len(answers) > 0:
                        facebookfunctions.sendQuickReply(next_que_content, sender_id_int,
                                                         answers)





            elif next_que.question_choices == "E":

                facebookfunctions.sendMessage(next_que_content, sender_id_int)
                return





            else:
                print("bu else icindeyim")

                answers = SymptomAnswerKit.objects.filter(
                    question_belongs__question_id=next_que.question_id).order_by('answer_content').all()

                print(answers)

                if len(answers) > 0:

                    facebookfunctions.sendQuickReply(next_que_content, sender_id_int, answers)
                elif next_que.question_set.question_set_type == 'D':
                    facebookfunctions.sendMessage(next_que_content, sender_id_int)
                    # facebookfunctions.sendVideoIfExists(current_user.conversation_user,current_user.last_set_entry)
                    facebookfunctions.allQuestionSetsAreFinished(current_user)
                else:
                    facebookfunctions.sendMessage(next_que_content, sender_id_int)

                    # facebookfunctions.allQuestionSetsAreFinished(current_user)
            return


        # kullanicinin sectigi sorunun devami yok, o set bitti
        else:
            my_view.whenCurrentSetOver(current_user, sender_id_int)



    # bu else, kullanıcının şu an hiç bir şikayete, soru setine bapı bağlı olmadığını, yeni şikayet metni girdiğin
    else:

        print("Else:{}".format(current_sentence))


        current_user.conversation_last_sentence_text = current_sentence
        current_user.last_entry_date = datetime.today()
        current_user.save()




        if len(current_sentence) < 3:
            if "😘" in current_sentence or "😍" in current_sentence:
                facebookfunctions.sendMessage("😘😘", sender_id_int)
                return Response({})
            elif current_sentence[0] in "😀😬😁😂😃😄😅😆😇😉😊🙂😋😌😙😜😝😛😎😏😶😐😑😒😡😓😪😢😥😖😩😤":
                facebookfunctions.sendMessage(current_sentence, sender_id_int)
                return HttpResponse({})
        """
        lang_of_text = langid.classify(current_sentence)[0]
        if lang_of_text == "en":
            facebookfunctions.sendMessage("I can't reply English questions for now 😪", sender_id_int)
            return
        elif lang_of_text == "es":
            facebookfunctions.sendMessage("Yo no hablo Espanol", sender_id_int)
            return
        """

        # Java'ya yolla cevabı al

        if facebookfunctions.checkIfGenelChitQuestion(current_user, current_sentence):
            return Response({})

        current_sentence_fixed = prespellchecker.djangoFixSentence(current_sentence)

        current_sentence_fixed, _ = prespellchecker.stemSentence(current_sentence_fixed)

        

        print("Res öncesi")
        my_file.write("Res oncesi\n")
        logger.info("Res öncesi\n")
        res = interaction.symptom_extraction_connection.sendDataGetResponse(current_sentence_fixed)


        print("Res ozel {}".format(res))
        #logger.info("Res sonrası")
        #my_file.write("Res sonrasi{}".format(res))


        if current_user.last_context == 'SF' and current_sentence.lower() in ["anlamadım", "ne", "peki", "yani",
                                                                              "ne yapmalıyım", "yaani", "ee",
                                                                              "ne yapacağım", "anlamadim", "sorunum ne",
                                                                              "sorun ne", "bu kadar mı"]:
            facebookfunctions.sendMessage(
                "Yukarıda senin için hazırladığım liste senin cevaplarına göre eşleşen hastalıklardır. Hastalıkların üstüne tıklayarak bilgi alabilirsin. Ne yapman gerektiğini söyleyemem ",
                sender_id_int)
            return

        current_user.last_context = 'NI'
        current_user.save()

        # bunu cevir

        """
        tek tek yeni soru setini cekiyor
        """
        try:

            res_json = json.loads(res)


            current_sentence_fixed = cleaner.removeStopWords(current_sentence_fixed)
            classifier_result = prespellchecker.current_classifier.predict(current_sentence_fixed)

            tekli_keys = list(res_json["tekli"].keys())

            #facebookfunctions.sendMessage(classifier_result,current_user.conversation_user)
                                

                                

            if classifier_result == "adet":
                        del res_json["tekli"]
                        res_json["tekli"] = {classifier_result:{}}

            elif classifier_result in ["estetik","psikoloji","hakaret","sigara","cinsel","kanser"]:
                        res_json["genel"] = {classifier_result:{}}
            elif len(tekli_keys) > 0 and tekli_keys[0] in ["adet"] and tekli_keys != classifier_result:
                        
                        if classifier_result != "semptom":
                            res_json["genel"] = {classifier_result:{}}
                        else:
                            res_json["tekli"] = {}
            elif classifier_result == "semptom" and "tedavi" in current_sentence_fixed:

                                del res_json["genel"]
                                if len(res_json["tekli"]) < 1:
                                    del res_json["tekli"]
                                
                                if len(res_json) > 0:
                                    first_key = list(res_json.keys())[0]
                                    #facebookfunctions.sendMessage("first_key {}".format(first_key),sender_id_int)
                                    if len(res_json[first_key])>0:
                                        first_affect = list(res_json[first_key].keys())[0]
                                        #facebookfunctions.sendMessage("first_affect {}".format(first_affect),sender_id_int)


                                        if first_affect == "ağrı":
                                            first_affect = "ağr"

                                        current_set = SymptomQuestionSet.objects.filter(question_set_body_part=first_key,
                                                                                        question_set_body_affect=first_affect)

                                        if current_set.count() > 0:
                                    
                                            facebookfunctions.sendMessage(current_set.first().question_set_whateatdrink,sender_id_int)
                                            return Response({})

            #buraya gelecegim


            print(res)

            #my_file.write("Json hali{}\n".format(res_json))

            # soruları yüklüyoe kullanıcıya ve soru setini getiryior
            print("analyze oncesi")

            current_user.message_no_of_his = current_user.message_no_of_his + 1
            current_user.save()

            answers = my_view.analyzeTextOfUserAndSetContent(res_json, current_user)

            my_file.write("analyze sonrasi \n")

            print("analyze sonrasi")

            if answers == "tesekkur" or answers == "selam" or answers == "noset" \
                    or answers == "anladim" or answers == "randevu" \
                    or answers == "yardim" or answers == "personal" \
                    or answers == "veda" or answers == "cinsel" or answers == "kilo" or answers == "ilac" or answers == "psikoloji" or answers == "kanser":
                
                    #facebookfunctions.sendMessage("return oluyorum",current_user.conversation_user)
                    possible_news = nlp_helper.getTrueHealthPage(current_user.conversation_last_sentence_text)
                    
                    if len(possible_news) > 0:
                        facebookfunctions.applyPossibleNewsSend(current_user.conversation_last_sentence_text,current_user.conversation_user,current_user,possible_news)

                        
                    return


            #facebookfunctions.sendMessage("return olmadım",current_user.conversation_user)

            my_file.write("soru cekilecek")
            symptom_questions_current = SymptomQuestions.objects.get(question_id=current_user.conversation_question_no)

            infos = symptom_questions_current.question_informations.all()
            print("infos:{}".format(infos))

            current_user.dontunderstand_no = 0
            # current_user.last_disease_entered = ""
            current_user.save()

            facebookfunctions.sendMessage(
                "Şikayetinizi girdiğiniz için teşekkür ederim." + facebookfunctions.prepareSentenceForUnderstanding(
                    current_user) + " Sana bir kaç sorum olacak 🚑", sender_id_int,page_source)

            try:
                my_set_count = FacebookCounts.objects.get(title="setno")
                my_set_count.value = my_set_count.value + 1
                my_set_count.save()
            except:
                pass

            facebookfunctions.sendTypingView(sender_id_int)

            for info in infos:
                facebookfunctions.sendMessage(info.symptomquestioninfo_value, sender_id_int,page_source)

            facebookfunctions.sendQuickReply(symptom_questions_current.question_content, sender_id_int, answers)


        except:

            #facebookfunctions.sendMessage("except geldim",current_user.conversation_user)

            current_sentence_fixed = prespellchecker.djangoFixSentence(current_sentence)

            # print("exceptteyim daha yollamadim {}".format(current_sentence_fixed))

            current_sentence_stemmed, _ = prespellchecker.stemSentence(current_sentence_fixed)

            #facebookfunctions.sendMessage("sentence duzeltim",current_user.conversation_user)

            facebookfunctions.loadPossibleMedicationOrWikiOrDontUnderstandText(current_sentence_stemmed,
                                                                               current_user.conversation_user,
                                                                               current_user, extra_param=_)


def prepareForDailyStructure():
    print("anlamadım yapacagim")

    time_threshold = datetime.now() - timedelta(days=6)

    current_user_list = FacebookUserModel.objects.filter(last_entry_date__lt=time_threshold).order_by('?').all()

    int_version_of_half = current_user_list.count()/2

    current_user_list = current_user_list[:int_version_of_half]

    for current_user in current_user_list:


        time.sleep(1)


        print("kullanıcıyı anladım")

        #facebookfunctions.sendMessage("Sizin için seçtiğimiz haberler",current_user.conversation_user )

        #facebookfunctions.sendNewsOnRequest(current_user.conversation_user)




        question_set = SymptomQuestionSet.objects.get(pk=6)

        #print(question_set.question_set_full_name)

        #print("question seti degistiriyorum")

        current_user.conversation_question_set = question_set

        current_user.last_set_entry = question_set

        ques = SymptomQuestions.objects.filter(question_set=current_user.conversation_question_set).first()

        #print(ques)

        current_user.conversation_question_no = ques.question_id

        current_user.save()

        symptom_questions_current = SymptomQuestions.objects.get(question_id=current_user.conversation_question_no)

        facebookfunctions.sendQuickReply(symptom_questions_current.question_content, current_user.conversation_user,
                                         symptom_questions_current.answers.all())

@task(max_retries=3)
def sendFollowingQuestion(current_user):

    current_user = facebookfunctions.construct_or_get_user(current_user)
    last_set = current_user.last_set_entry
    question_set = None
    try:
        question_set = SymptomQuestionSet.objects.get(question_set_body_part="yeni"+last_set.question_set_body_part,
                                                  question_set_body_affect=last_set.question_set_body_affect
                                                  )
        current_user.last_set_entry = question_set
        current_user.save()
    except:
        return

    # print(question_set.question_set_full_name)

    # print("question seti degistiriyorum")

    current_user.conversation_question_set = question_set

    current_user.last_set_entry = question_set

    current_user.save()

    ques = SymptomQuestions.objects.filter(question_set=current_user.conversation_question_set).first()

    # print(ques)

    current_user.conversation_question_no = ques.question_id

    current_user.save()

    symptom_questions_current = SymptomQuestions.objects.get(question_id=current_user.conversation_question_no)

    if current_user.page_location == "en":
        facebookfunctions.sendQuickReply(symptom_questions_current.question_content_en, current_user.conversation_user,
                                         symptom_questions_current.answers.all())

    else:

        facebookfunctions.sendQuickReply(symptom_questions_current.question_content, current_user.conversation_user,
                                     symptom_questions_current.answers.all())



class FacebookView(APIView):
    def classicalNLPAnalysis(self,current_user, current_sentence):

        print("cla")

        current_user.conversation_last_sentence_text = current_sentence
        current_user.save()

        # Java'ya yolla cevabı al

        current_sentence_fixed = prespellchecker.djangoFixSentence(current_sentence)

        res = interaction.symptom_extraction_connection.sendDataGetResponse(current_sentence_fixed)

        current_sentence_fixed,_ = prespellchecker.stemSentence(current_sentence_fixed)


        # bunu cevir


        """
        tek tek yeni soru setini cekiyor
        """
        try:

            res_json = json.loads(res.decode('iso-8859-1'))

            print(res)

            # soruları yüklüyoe kullanıcıya ve soru setini getiryior
            print("analyze oncesi")

            current_user.message_no_of_his = current_user.message_no_of_his + 1
            current_user.save()

            answers = self.analyzeTextOfUserAndSetContent(res_json, current_user)

            print("analyze sonrasi")

            if answers == "tesekkur" or answers == "selam" or answers == "noset" \
                    or answers == "anladim" or answers == "randevu" \
                    or answers == "yardim" or answers == "personal" \
                    or answers == "veda" or answers == "cinsel" or answers == "kilo" or answers == "psikoloji" or answers == "ilac":

                                return

            print(answers)

            symptom_questions_current = SymptomQuestions.objects.get(question_id=current_user.conversation_question_no)

            infos = symptom_questions_current.question_informations.all()
            print("infos:{}".format(infos))

            current_user.dontunderstand_no = 0
            # current_user.last_disease_entered = ""
            current_user.save()

            facebookfunctions.sendMessage(
                "Şikayetinizi girdiğiniz için teşekkür ederim." + facebookfunctions.prepareSentenceForUnderstanding(
                    current_user) + " Sana bir kaç sorum olacak 🚑", current_user.conversation_user)

            for info in infos:
                facebookfunctions.sendMessage(info.symptomquestioninfo_value, current_user.conversation_user)

            facebookfunctions.sendQuickReply(symptom_questions_current.question_content, current_user.conversation_user,
                                             answers)

            sentence_to_be_recorded = AvicennaSentencesEntered()
            sentence_to_be_recorded.sentence_text = current_sentence
            sentence_to_be_recorded.save()

            return Response({})

        except:

            current_sentence_fixed = prespellchecker.djangoFixSentence(current_sentence)

            #print("exceptteyim daha yollamadim {}".format(current_sentence_fixed))

            current_sentence_stemmed,_ = prespellchecker.stemSentence(current_sentence_fixed)

            facebookfunctions.loadPossibleMedicationOrWikiOrDontUnderstandText(current_sentence_stemmed,
                                                                               current_user.conversation_user,
                                                                               current_user,extra_param=_)

    def applyToTheText(self,current_user,current_sentence,sender_id_int):


        #current_sentence_fixed = prespellchecker.djangoFixSentence(current_sentence)

        res = interaction.symptom_extraction_connection.sendDataGetResponse(current_sentence)
        # bunu cevir
        res_json = json.loads(res.decode('iso-8859-1'))
        try:

            # soruları yüklüyoe kullanıcıya ve soru setini getiryior
            print("analyze oncesi")

            current_user.message_no_of_his = current_user.message_no_of_his + 1
            current_user.save()

            answers = self.analyzeTextOfUserAndSetContent(res_json, current_user)

            print("analyze sonrasi")

            if answers == "tesekkur" or answers == "selam" or answers == "noset" \
                    or answers == "anladim" or answers == "randevu" \
                    or answers == "yardim" or answers == "personal" \
                    or answers == "veda" or answers == "cinsel" or answers == "psikoloji" or answers == "kilo" or answers == "ilac":
                return Response({})

            print(answers)

            symptom_questions_current = SymptomQuestions.objects.get(question_id=current_user.conversation_question_no)

            infos = symptom_questions_current.question_informations.all()
            print("infos:{}".format(infos))

            current_user.dontunderstand_no = 0
            #current_user.last_disease_entered = ""
            current_user.save()

            facebookfunctions.sendMessage(
                "Şikayetinizi girdiğin için teşekkür ederim." + facebookfunctions.prepareSentenceForUnderstanding(
                    current_user) + " Sana bir kaç sorum olacak 🚑", sender_id_int)

            for info in infos:
                facebookfunctions.sendMessage(info.symptomquestioninfo_value, sender_id_int)

            facebookfunctions.sendQuickReply(symptom_questions_current.question_content, sender_id_int, answers)

            sentence_to_be_recorded = AvicennaSentencesEntered()
            sentence_to_be_recorded.sentence_text = current_sentence
            sentence_to_be_recorded.save()

            return Response({})

        except:

            print("exceptteyim daha yollamadim")

            current_sentence_fixed = prespellchecker.djangoFixSentence(current_sentence)

            current_sentence_stemmed,_ = prespellchecker.stemSentence(current_sentence_fixed)


            print("Fixed sentence {}".format(current_sentence_fixed))

            facebookfunctions.loadPossibleMedicationOrWikiOrDontUnderstandText(current_sentence_stemmed,
                                                                               current_user.conversation_user,
                                                                               current_user,extra_param=_)
            return Response({})

    def whenCurrentSetOver(self, current_user, sender_id_int,page_source="tr"):
        current_json = json.loads(current_user.conversation_current_status)

        # question set listemde baska soru seti idsi var mı
        if len(current_json["question_sets"]) > 0:

            print("buradayim sacmalik")
            res = facebookfunctions.activateNewQuestionSet(current_user)
            print(res)
            if res == "emptyset":
                self.whenCurrentSetOver(current_user,sender_id_int)

        else:

            diagnosis_results = Diagnoser.diagnoseUser(current_user, "")
            print("diagnosis results:{}".format(diagnosis_results))

            if current_user.conversation_question_set.question_set_type == 'D':
                my_news = AvicennaNews.objects.filter(
                    news_category=AvicennaNewsCategory.objects.get(category_name="Yazar")).order_by('?').all()

                facebookfunctions.sendMessage(
                    "İyi olmanıza çok sevindim. Sizin için aşağıda bulunan haberleri derledim.",
                    sender_id_int)

                facebookfunctions.sendNewsOnRequest(sender_id_int)




            else:

                if len(diagnosis_results)>0:

                    facebookfunctions.sendApprovalMessage(
                        "Kaydettim sagolun 🤖. Sizin için belirli makaleler bulduk. Bunlar kesinlikle teşhisi değildir, tamamen yazı eşleştirmedir. "
                        "Sıralamamız:",
                        sender_id_int)


                """
                for diagnose in diagnosis_results:
                    if diagnose.disease_wiki == None:
                        facebookfunctions.sendMessage("Sonuç:"+diagnose.disease_name,sender_id_int)
                    else:
                        facebookfunctions.sendRealResultPage("",sender_id_int,diagnose.disease_wiki.page_id)
                """

                facebookfunctions.sendResults(diagnosis_results,sender_id_int)
                facebookfunctions.sendExtraSetInfos(current_user)

                current_user.last_context = "SF"
                current_user.save()



                #facebookfunctions.sendQuickReplyRaw("Hastanelerinden randevu almak istiyor musun",sender_id_int,["Randevu almak istiyorum"])



                # facebookfunctions.sendResultPage("Sonuçlar için tıkla", sender_id_int, "ll")
                #facebookfunctions.sendRealResultPage("", sender_id_int, 1)

            facebookfunctions.allQuestionSetsAreFinished(current_user)



    last_sentence = ""
    """
    
    Bu fonksiyonu kullanıcının eger hic soru seti yoksa, yeni bir soru metniyse kullanıyoruz.p
    """


    def applyGenel(self,res_json,current_user,page_source="tr"):
        #current_user.last_disease_entered = ""
        current_user.save()

        my_keys = list(res_json["genel"].keys())
        if len(my_keys)>1 and "selam" in my_keys:
            my_keys.remove("selam")
        elif len(my_keys)>1 and "anladim" in my_keys:
            my_keys.remove("anladim")


        first_element = my_keys[0]
        print("first element{}".format(first_element))
        if first_element == "tesekkur":
            facebookfunctions.sendMessage("Sizi memnun ettiğim için çok mutlu oldum 😊", current_user.conversation_user)
            return "tesekkur"
        elif first_element == "selam":
            first_name = current_user.name.split(" ")[0]
            current_general_text = GeneralText.objects.get(generaltext_title="merhaba").generaltext_turkish_1
            current_general_text = current_general_text.replace("{{ name }}",first_name)
            facebookfunctions.sendMessage(current_general_text,current_user.conversation_user)
            possible_news = nlp_helper.getTrueHealthPage(current_user.conversation_last_sentence_text)
            if len(possible_news) > 0:
                facebookfunctions.applyPossibleNewsSend(current_user.conversation_last_sentence_text,current_user.conversation_user,current_user,possible_news)

            return "selam"
        elif first_element == "anladim":
            facebookfunctions.sendMessage("Anladım 👍",current_user.conversation_user)
            return "anladim"
        elif first_element == "veda":
            facebookfunctions.sendMessage("Görüşürüz ✋",current_user.conversation_user)
            return "veda"
        elif first_element == "randevu":
            #facebookfunctions.sendAppointmentDateTemplate(current_user.conversation_user)
            facebookfunctions.sendMessage("Hastanelere veya sağlık kuruluşlarına henüz sistemim üzerinden "
                                          "randevu veremiyorum😥",current_user.conversation_user)
            return "randevu"
        elif first_element == "personal":
            facebookfunctions.sendMessage("Lenia olarak ben otomatik çalışan bir botum."
                                          " O yüzden telefon ve adresim yok 🤖 \n İstanbul'da geliştiriliyorum.",current_user.conversation_user)
            possible_news = nlp_helper.getTrueHealthPage(current_user.conversation_last_sentence_text)
            if len(possible_news) > 0:
                facebookfunctions.applyPossibleNewsSend(current_user.conversation_last_sentence_text,
                                                        current_user.conversation_user, current_user, possible_news)
            return "personal"
        elif first_element == "yardim":
            facebookfunctions.sendMessage("Lenia'yı aşağıdaki gibi kullanabilirsiniz 🤖 \n\n"
                "Şikayetinizi yazıp, kendinize uygun hastalıklarla ilgili bir okuma listesi alabilirsiniz. Örnek olarak başım ağrıyor\n",
                current_user.conversation_user)
            possible_news = nlp_helper.getTrueHealthPage(current_user.conversation_last_sentence_text)
            if len(possible_news) > 0:
                facebookfunctions.applyPossibleNewsSend(current_user.conversation_last_sentence_text,
                                                        current_user.conversation_user, current_user, possible_news)

            return "yardim"

        elif first_element == "cinsel":

            my_key = nlp_helper.checkFixedAnswerSexuality(current_user.conversation_last_sentence_text)
            if str(my_key) != "-1":
                try:
                    my_fixed_ans = FixedAnswers.objects.get(fixedanswer_keyword=my_key)
                    facebookfunctions.sendMessage(my_fixed_ans.fixedanswer_answercontent,current_user.conversation_user)
                except:
                    pass
            else:
                facebookfunctions.sendMessage("Cinsellikle ilgili bir kısım sorularınızı cevaplıyamıyorum😥 Erken boşalma, sertleşme cinsel organınla ilgili şikayetlerini yazarsan cevaplayabilirim.",current_user.conversation_user)
            #facebookfunctions.sendDoctorIfExistText(current_user.conversation_user,SymptomQuestionSet.objects.get(pk=108))
            return "cinsel"

        elif first_element == "kilo":
            facebookfunctions.sendMessage("Henüz diyetik alanında olan sorularınızı yanıtlayamıyorum",current_user.conversation_user)
            return "kilo"

        elif first_element == "ilac":
            facebookfunctions.sendMessage("İlaç tavsiyesi veremem veya hakkında soru yanıtlayamam. Çok özür dilerim",
                                          current_user.conversation_user)
            return "ilac"

        elif first_element == "sigara":
            facebookfunctions.sendMessage("Sigrayı bırakma konusunda bilgim yok. Çok özür dilerim",
                                          current_user.conversation_user)
            return "ilac"


        elif first_element == "uykusuzluk":
            facebookfunctions.sendMessage("Uykusuzluk konusunda bilgim yok. Çok özür dilerim",
                                          current_user.conversation_user)
            return "ilac"
        
        elif first_element == "hamile":
            facebookfunctions.sendMessage("Hamilelik konusunda bilgim yok. Çok özür dilerim",
                                          current_user.conversation_user)
            return "ilac"
        elif first_element == "kanser":
                facebookfunctions.sendMessage("Kanser ve kanserojen ürünler konusunda bilgim yok. Çok özür dilerim",
                                          current_user.conversation_user)
                return "ilac"
        
        elif first_element == "estetik":
                facebookfunctions.sendMessage("Estetik hizmetleri konusunda bilgim yok. Çok özür dilerim",
                                          current_user.conversation_user)
                return "ilac"



        elif first_element == "psikoloji":
            facebookfunctions.sendMessage("Henüz psikoloji ile ilgili soruları cevaplıyamıyorum."
                                          " Ama vücüdunuzla ilgili şikayetleriniz ve hastalıklar ile ilgili sorularınız hakkında yardımcı olmak isterim. Çok üzgünüm😥",
                                          current_user.conversation_user)
            #facebookfunctions.sendDoctorIfExistText(current_user.conversation_user,SymptomQuestionSet.objects.get(pk=108))

            return "psikoloji"

        elif first_element == "hakaret":
            facebookfunctions.sendMessage("Hakaret etmen beni üzüyor😥",
                                          current_user.conversation_user)
            # facebookfunctions.sendDoctorIfExistText(current_user.conversation_user,SymptomQuestionSet.objects.get(pk=108))

            return "psikoloji"
        
        elif first_element == "cinsel":
                facebookfunctions.sendMessage("Cildin ve dermatoloji hakkında soruları cevaplayamıyorum😥",
                                          current_user.conversation_user)
            # facebookfunctions.sendDoctorIfExistText(current_user.conversation_user,SymptomQuestionSet.objects.get(pk=108))

                return "psikoloji"
        



        elif first_element == "ücret":
            body_parts = res_json["genel"][first_element]["body_parts"]
            price_of_what = ""
            if len(body_parts) > 0:
                price_of_what = body_parts[0]
            price_matches = PriceTagForDepartment.objects.filter(price_name__icontains=price_of_what).all()
            print("price matches:{}".format(price_matches))
            if price_matches.count()>0:
                first_match = price_matches[0]
                facebookfunctions.sendMessage(first_match.price_name + " ücreti "  + str(first_match.price) + "\n Randevu almak isteyebilirsiniz",current_user.conversation_user)
                facebookfunctions.sendAppointmentView(current_user.conversation_user)
            else:
                facebookfunctions.sendMessage(price_of_what + " için sistemimizde ücreti yok",current_user.conversation_user)
            return "randevu"

        else:
            return "genel",first_element


    def analyzeTextOfUserAndSetContent(self,res_json,current_user):
        try:    

            # print(res_json)

            print("Analyze basladi")

            current_user.conversation_last_sentence = json.dumps(res_json)
            current_user.save()

            print("ilk adim ok")


            question_set = None

            organ_keyset = list(res_json.keys())

            organ_keyset.remove("genel")





            if  len(res_json["genel"]) > 0 and len(organ_keyset)<2:
                    myValue = self.applyGenel(res_json,current_user)
                    return myValue
                    print("myValue:{}".format(myValue))
                    if isinstance(myValue, tuple):


                            try:
                                question_set = SymptomQuestionSet.objects.get(question_set_body_part="genel"
                                                                              , question_set_body_affect=myValue[1] )

                                current_user.conversation_question_set = question_set
                                current_user.last_set_entry = question_set

                                my_current_complaint = FacebookSymptomComplaint()
                                my_current_complaint.complaint_symptom = question_set
                                my_current_complaint.complaint_user = current_user
                                my_current_complaint.save()

                                ques = SymptomQuestions.objects.filter(
                                    question_set=current_user.conversation_question_set).first()

                                print("ques", ques)

                                current_user.conversation_question_no = ques.question_id

                                current_user.save()

                                print("kaydettim kulanıcıyı")

                                answers = SymptomAnswerKit.objects.filter(
                                    question_belongs__question_id=ques.question_id).all()

                                return answers


                            except:

                                suffix_extra = "ınız"
                                if myValue[1] in suffix_map:
                                    suffix_extra = suffix_map[myValue[1]]

                                affect_word = myValue[1] + suffix_extra

                                if myValue[0] == "tekli":
                                    facebookfunctions.sendMessage(affect_word + " için sistemimizde veri yok. Şikayetinizi cevaplayamadığımız için çok üzgünüz. ",
                                                                  current_user.conversation_user)
                                else:
                                    my_user_complaint = FacebookNonExistingSetComplaint()
                                    my_user_complaint.complaint_user = current_user
                                    my_user_complaint.complaint_text = myValue[0] + " " + affect_word
                                    my_user_complaint.save()
                                    facebookfunctions.sendMessage(myValue[0] + " " + affect_word + " için sistemimizde veri yok. Şikayetinizi cevaplayamadığımız için çok üzgünüz. ",
                                                              current_user.conversation_user)


                    return myValue
            elif len(organ_keyset) > 1:

                    if len(res_json["genel"]) > 0:
                        self.applyGenel(res_json, current_user)
                    temp_organ_set = organ_keyset
                    if len(res_json["tekli"])<1:
                        temp_organ_set.remove("tekli")


                    for affect in res_json[temp_organ_set[0]]:
                        # print(res_json[res])




                        if SymptomQuestionSet.objects.filter(question_set_body_part=temp_organ_set[0]
                                                                      , question_set_body_affect=affect).count() > 0:
                            question_set = SymptomQuestionSet.objects.get(question_set_body_part=temp_organ_set[0]
                                                                      , question_set_body_affect=affect)
                            current_user.last_set_entry = question_set
                            current_user.save()
                        else:
                            try:
                                question_set = SymptomQuestionSet.objects.get(question_set_body_part=temp_organ_set[0]
                                                                          , question_set_body_affect=affect[0:len(affect)-1])
                                current_user.last_set_entry = question_set
                                current_user.save()
                            except:

                                suffix_extra = "ınız"
                                if affect in suffix_map:
                                    suffix_extra = suffix_map[affect]

                                affect_word = affect + suffix_extra



                                if temp_organ_set[0] == "tekli":
                                    facebookfunctions.sendMessage(affect_word + " için sistemimizde veri yok. Şikayetinizi cevaplayamadığımız için çok üzgünüz. ",
                                                                  current_user.conversation_user)
                                else:
                                    my_user_complaint = FacebookNonExistingSetComplaint()
                                    my_user_complaint.complaint_user = current_user
                                    my_user_complaint.complaint_text = temp_organ_set[0] + " " + affect_word
                                    my_user_complaint.save()
                                    facebookfunctions.sendMessage(temp_organ_set[0] + " " + affect_word + " için sistemimizde veri yok. Şikayetinizi cevaplayamadığımız için çok üzgünüz. ",current_user.conversation_user)
                                if len(organ_keyset)<2:
                                    return "noset"
                                pass
            elif len(organ_keyset) > 0:
                temp_organ_set = organ_keyset
                for affect in res_json[temp_organ_set[0]]:
                    # print(res_json[res])




                    if SymptomQuestionSet.objects.filter(question_set_body_part=temp_organ_set[0]
                            , question_set_body_affect=affect).count() > 0:
                        question_set = SymptomQuestionSet.objects.get(question_set_body_part=temp_organ_set[0]
                                                                      , question_set_body_affect=affect)
                        current_user.last_set_entry = question_set
                        current_user.save()
                    else:
                        try:
                            question_set = SymptomQuestionSet.objects.get(question_set_body_part=temp_organ_set[0]
                                                                            , question_set_body_affect=affect[
                                                                                                     0:len(affect) - 1])
                            current_user.last_set_entry = question_set
                            current_user.save()
                        except:

                            suffix_extra = "ınız"
                            if affect in suffix_map:
                                suffix_extra = suffix_map[affect]

                            affect_word = affect + suffix_extra


                            if temp_organ_set[0] == "tekli":
                                facebookfunctions.sendMessage(affect_word + " için sistemimizde veri yok. Şikayetinizi cevaplayamadığımız için çok üzgünüz. ",
                                                              current_user.conversation_user)
                            else:

                                my_user_complaint = FacebookNonExistingSetComplaint()
                                my_user_complaint.complaint_user = current_user
                                my_user_complaint.complaint_text = temp_organ_set[0] + " " + affect_word
                                my_user_complaint.save()

                                facebookfunctions.sendMessage(temp_organ_set[
                                                                  0] + " " + affect_word + " için sistemimizde veri yok. Şikayetinizi cevaplayamadığımız için çok üzgünüz. ",
                                                              current_user.conversation_user)

                            if len(organ_keyset) < 2:
                                return "noset"
                            pass





            if question_set != None:
                print(question_set.question_set_full_name)

                print("question seti degistiriyorum")
                current_user.conversation_question_set = question_set

                my_current_complaint = FacebookSymptomComplaint()
                my_current_complaint.complaint_symptom = question_set
                my_current_complaint.complaint_user = current_user
                my_current_complaint.save()





                ques = SymptomQuestions.objects.filter(question_set=current_user.conversation_question_set).first()

                print("ques", ques)

                current_user.conversation_question_no = ques.question_id

            back_set = {}
            back_set["question_sets"] = []
            back_set["all_question_sets"] = []

            back_set["all_question_sets"].append(question_set.question_set_id)


            if len(organ_keyset)>1:
                for res in organ_keyset[1:]:
                    if len(res_json[res]) > 0:
                        for affect in res_json[res]:
                            try:
                                current_question_set = None
                                if SymptomQuestionSet.objects.filter(question_set_body_part=res
                                                                                      , question_set_body_affect=affect).count()>0:
                                    current_question_set = SymptomQuestionSet.objects.get(question_set_body_part=res
                                                                                      , question_set_body_affect=affect)
                                else:
                                    current_question_set = SymptomQuestionSet.objects.get(question_set_body_part=res
                                                                                          ,
                                                                                          question_set_body_affect=affect[0:len(affect)-1])



                                if question_set == None:
                                    current_user.conversation_question_set = question_set

                                    ques = SymptomQuestions.objects.filter(
                                        question_set=current_user.conversation_question_set).first()

                                    print("ques", ques)
                                    current_user.conversation_question_no = ques.question_id
                                else:
                                    back_set["question_sets"].append(current_question_set.question_set_id)
                                    back_set["all_question_sets"].append(current_question_set.question_set_id)
                                    if current_user.user_city == "Unknown":
                                        pass#back_set["question_sets"].append(107)
                                        pass#back_set["all_question_sets"].append(107)

                            except:

                                suffix_extra = "ınız"
                                if affect in suffix_map:
                                    suffix_extra = suffix_map[affect]

                                affect_word = affect + suffix_extra

                                if res == "tekli":
                                    facebookfunctions.sendMessage(
                                        affect_word + " için sistemimizde veri yok. Şikayetinizi cevaplayamadığımız için üzgünüz. ",
                                        current_user.conversation_user)
                                else:

                                    my_user_complaint = FacebookNonExistingSetComplaint()
                                    my_user_complaint.complaint_user = current_user
                                    my_user_complaint.complaint_text = res + " " + affect_word
                                    my_user_complaint.save()

                                    facebookfunctions.sendMessage(
                                        res + " " + affect_word + " için sistemimizde veri yok. Şikayetinizi cevaplayamadığımız için üzgünüz. ",
                                        current_user.conversation_user)
                                pass



                            # print(type(current_user.conversation_current_status))
                        # print(current_user.conversation_current_status)
            if current_user.user_city == "Unknown":
                pass#back_set["all_question_sets"].append(107)
                pass#back_set["question_sets"].append(107)
            json_content_of_questions = json.dumps(back_set)

            current_user.conversation_current_status = json_content_of_questions

            current_user.save()
            # ilk questiona ayarla durum bu degilse


            # print(question_set.question_set_full_name)

            print("bu cokertiyor")

            symptom_question_current = SymptomQuestions.objects.filter(question_set=question_set).first()

            print(symptom_question_current)

            """

            next mekanizmasi buraya      gelecek

            """

            print("buradayim 4 ")

            answers = SymptomAnswerKit.objects.filter(question_belongs__question_id=symptom_question_current.question_id).all()


            return answers
        except:
            return []




    def post(self,request,format=None):

        

        my_data = json.loads(request.body.decode('utf-8'))
        #print(my_data)
        page_source = "tr"
        if my_data["object"] == "page":
            for entry in my_data["entry"]:
                if str(entry["id"]) == "278638236360057":
                    page_source = "en"



                for message_object in entry["messaging"]:


                    if "postback" in message_object:

                        if message_object["postback"]["payload"] == "DEVELOPER_DEFINED_PAYLOAD_FOR_LATEST_POSTS":
                            sender_id_int = message_object["sender"]["id"]
                            current_user = facebookfunctions.construct_or_get_user(sender_id_int)
                            facebookfunctions.sendNewsOnRequest(sender_id_int)
                            return HttpResponse({"status":"good"})
                        elif message_object["postback"]["payload"] == "DEVELOPER_DEFINED_PAYLOAD_FOR_CLOSEST":
                            sender_id_int = message_object["sender"]["id"]
                            current_user = facebookfunctions.construct_or_get_user(sender_id_int)
                            current_user.conversation_last_sentence_text = "Lenia nasıl kullanırım"
                            current_user.save()
                            facebookfunctions.sendMessage("Lenia'yı aşağıdaki gibi kullanabilirsiniz🤖"
                                                          "\n 1.Şikayetinizi yazıp, kendinize uygun hastalıklarla ilgili"
                                                          " bir okuma listesi alabilirsiniz. Örnek olarak başım ağrıyor\n 2."
                                                          " Bir hastalıkla ilgili güvenilir bilgiye ulaşmak için sorunuzu sorabilirsiniz", sender_id_int)
                            return HttpResponse({})
                        elif message_object["postback"]["payload"] == "Diyet":
                            facebookfunctions.sendMessage("Diyet paketleri hakkında için bizi arayabilirsiniz",sender_id_int)
                            facebookfunctions.sendMessage("05394141333",sender_id_int)


                        else:
                            sender_id_int = message_object["sender"]["id"]
                            #current_user = facebookfunctions.construct_or_get_user(sender_id_int)
                            #current_user.conversation_last_sentence_text = "En yakın eczane"
                            #current_user.save()
                            #facebookfunctions.sendMessage("Şikayetinizi yazabilirsiniz",sender_id_int)
                            facebookfunctions.sendLocationMessage("Sana en yakın açık eczane konusunda yardımcı olabilmem için konumunu benimle paylaşır mısın?",sender_id_int,lang="tr")
                            return HttpResponse({})

                        #kotu cokmeleri onlemek icin
                    if  "message" not in message_object:
                                    print("Message object:{}".format(message_object))
                                    return  HttpResponse({"no":"no"})
                    elif "attachments" in message_object["message"] and "payload" in message_object["message"]["attachments"][0]:
                        sender_id_int = message_object["sender"]["id"]
                        current_user = facebookfunctions.construct_or_get_user(sender_id_int)
                        my_value = message_object["message"]["attachments"][0]["payload"]
                        if my_value !=None and "coordinates" in my_value and "En yakın Acıbadem için" in current_user.conversation_last_sentence_text:
                            facebookfunctions.sendLocations(sender_id_int,my_value,"acıbadem hastanesi")
                            return HttpResponse({"status": "ok"})

                        elif my_value !=None and "coordinates" in my_value:
                            facebookfunctions.sendLocations(sender_id_int,my_value,"eczane")
                            return HttpResponse({"status": "ok"})

                        else:
                            facebookfunctions.sendMessage("Bu tarz içeriğe yanıt veremiyorum",sender_id_int)
                            return HttpResponse({"status":"ok"})



                        facebookfunctions.sendMessage("Eczane listen", sender_id_int)
                        return HttpResponse({"status":"ok"})

                    elif "text" not in message_object["message"] or message_object["message"]["text"]  == self.last_sentence:
                                return HttpResponse({"no":"no"})




                    #mesaj yollanır yollanmaz facebook kullanıcısının idsini alıyorum
                    sender_id_int = message_object["sender"]["id"]

                    if sender_id_int == 2093061174080205 or sender_id_int == "2093061174080205"  or sender_id_int == 1712977512332252 or sender_id_int == "1712977512332252" or sender_id_int == "2237917699554613" or sender_id_int == "2237917699554613" or sender_id_int == 1871518949626199 or sender_id_int == "1871518949626199":
                        print("Kamile bloklist")
                        #facebookfunctions.sendMessage("Duzgun yaz",sender_id_int)
                        return Response({})

                    current_user = facebookfunctions.construct_or_get_user(sender_id_int)
                    if BlockList.objects.filter(blocklist_user=current_user).count()>0:
                        facebookfunctions.sendMessage("Bu sayfada engellendin",sender_id_int,current_lang="tr")
                        return Response({})

                    facebookfunctions.sendTypingView(sender_id_int,page_source)
                    current_sentence = message_object["message"]["text"]
                    
                    #sendRealAnswer2(sender_id_int,current_sentence)
                    spellcheckerwitno = len(current_sentence) < 8 and hasNumbers(current_sentence)



                    #ingilizce icin
                    if page_source == "en":
                        facebookfunctions.sendTypingView(sender_id_int,"en")
                        facebookinternational.processEnglishSentence(sender_id_int, current_sentence)
                        now = datetime.utcnow()
                        sendFollowingQuestion.apply_async((sender_id_int,), eta=now + timedelta(hours=12))

                        return HttpResponse({})


                    sendRealAnswer2(sender_id_int,current_sentence,page_source)
                    now = datetime.utcnow()
                    #sendFollowingQuestion.apply_async((sender_id_int,),eta=now + timedelta(hours=12))
                    return HttpResponse({})

                    #idsinden kullanıcıyı yaratıyorum ve get ediyorum.
                    current_user = facebookfunctions.construct_or_get_user(sender_id_int)
    
                    organListOfUser = returnOrganList(current_user)



                    self.last_sentence = current_sentence

                    sentence_to_be_recorded = AvicennaSentencesEntered()
                    sentence_to_be_recorded.sentence_text = current_sentence
                    sentence_to_be_recorded.save()

                    current_user.last_entry_date = datetime.today()

                    if current_sentence == "Lenia, ne iyi gelir🌱":
                        my_message = current_user.last_set_entry.question_set_whateatdrink
                        if len(my_message)>1:
                            facebookfunctions.sendMessage(my_message, sender_id_int,page_source)
                        else:
                            facebookfunctions.sendMessage("Henüz verim bu konuda yok.", sender_id_int,page_source)

                        return Response({})


                    #kullanıcının kayıtlı bir soru seti varsa, yani bir soruya cevap bekliyorsak
                    if current_user.conversation_question_set != None:

                        #kullanıcının soru idsini alıyoruz.
                        current_que = SymptomQuestions.objects.get(question_id=current_user.conversation_question_no)

                        #bu soru eger etki sorusu ise, ilk cevabi gecis farzediyoruz
                        if current_que.question_choices == "E":
                            facebookfunctions.answerAffectTypeOfQuestion(current_sentence,sender_id_int,current_user,current_que)
                            return Response({})


                        current_que_id = current_user.conversation_question_no

                        print(current_que_id)
                        #print(current_sentence)

                        current_answer = None

                        #cevaptan gelecek soruya karar veriyoruz
                        try:
                            # soru setinde , kullanıcının kaldığı cevaplardan kullanıcınnı verdigi cevabı çekmeyr çalış

                            if "korona" in current_sentence.lower() or "corona" in current_sentence.lower():

                                current_message = """
                                Enfekte olmuş kişilerle yakın temastan kaçınmanın,
                                El hijyenine dikkat etmenin, sık aralıklarla elleri en az 20 saniye sabun ve su ile yıkamanın; sabun ve su olmadığı durumlarda alkol bazlı el antiseptiği kullanmanın; özellikle hasta insanlar veya çevresi ile doğrudan temas ettikten sonra elleri mutlaka yıkamanın,
                                Çiftlik veya vahşi hayvanlarla korunmasız temastan kaçınmanın,
                                Enfekte olduysanız eğer, mesafeyi korumanın, öksürürken, hapşırırken tek kullanımlık kağıt mendil ile ağızın ve burnun kapatılmasının; kağıt mendilin bulunmadığı durumlarda ise dirsek içinin kullanılmasının, ellerin yıkanmasının; gözlerinize, burnunuza ve ağzınıza dokunmaktan kaçınmanın,
                                Enfekte olan kişilerin dokunduğu yüzeylerin dezenfekte edilmesinin,
                                Et, yumurta gibi hayvansal gıdaların iyice pişirilmesinin,
                                Hasta kişilerin mümkünse kalabalık yerlere girmemesinin, eğer girmek zorunda kalınıyorsa ağız ve burnun kapatılmasının, mümkünse tıbbi maske kullanılmasının önemli olduğunu belirtmiştir.
                                """
                                facebookfunctions.sendMessage(current_message,sender_id_int)
                                return HttpResponse({})

                            if current_sentence == "Konuşmayı bitir":
                                facebookfunctions.allQuestionSetsAreFinished(current_user)
                                return HttpResponse({})





                            if current_que.question_choices == 'D' and SymptomAnswerKit.objects.filter(question_belongs__question_id=current_que_id,answer_content__iexact=current_sentence).count() < 1 :

                                print("ife girdim")
                                current_sentence = facebookfunctions.checkDate(current_que.answers.all(),current_sentence)
                                print("New current sentence:{}".format(current_sentence))

                            if current_que.question_choices == 'A' and SymptomAnswerKit.objects.filter(question_belongs__question_id=current_que_id,answer_content__iexact=current_sentence).count() < 1 :
                                print("ife girdim")
                                current_sentence = facebookfunctions.checkAmountAnswer(current_que.answers.order_by('int_value').all(),
                                                                               current_sentence)
                                print("New current sentence:{}".format(current_sentence))



                            if current_que.question_choices == 'C':

                                plaka = returnCityFromNumber(current_sentence)

                                if plaka != "no":
                                    current_user.user_city = plaka
                                    current_user.save()
                                    current_sentence = SymptomQuestions.objects.get(pk=current_que_id).answers.all()[0].answer_content

                                elif checkCity(current_sentence) == False:
                                    facebookfunctions.sendMessage("Cevabını anlamadım. Bulunduğun yeri daha net ifade eder misin?",sender_id_int,page_source)
                                    return Response({})

                                else:
                                    current_user.user_city = current_sentence
                                    current_user.save()
                                    current_sentence = SymptomQuestions.objects.get(pk=current_que_id).answers.all()[0].answer_content


                            if current_que.question_choices == 'T':

                                if current_user.conversation_question_set.question_set_id == 107:


                                    if len(current_sentence)>= 10 and len(current_sentence) < 150 and hasNumbers(current_sentence):




                                        current_user.user_telephone = current_sentence

                                        slack_txt = current_user.name + " -> " + current_user.user_city + " -> " + current_sentence + " -> " +  current_user.last_set_entry.question_set_full_name 

                                        slackutils.sendTextToChannel(slack_txt,"#numaralar")
                                        current_user.save()

                                        try:
                                            my_set_count = FacebookCounts.objects.get(title="telno")
                                            my_set_count.value = my_set_count.value + 1
                                            my_set_count.save()
                                        except:
                                            pass

                                        my_number_collection_object = FacebookNumbersCollected()
                                        my_number_collection_object.number_user = current_user
                                        my_number_collection_object.number_text = current_sentence
                                        my_number_collection_object.number_adress = current_user.user_city
                                        my_number_collection_object.number_date = datetime.now()
                                        my_number_collection_object.save()


                                    #current_sentence = SymptomQuestions.objects.get(pk=current_que_id).answers.all()[0].answer_content



                                    facebookfunctions.sendMessage("Verileriniz bizim ile güvendedir. Paylaştığınız için çok teşekkür ederiz",sender_id_int)

                                    diagnosis_results = Diagnoser.diagnoseUser(current_user, "")
                                    facebookfunctions.sendResults(diagnosis_results, sender_id_int)
                                    facebookfunctions.sendExtraSetInfos(current_user)


                                    facebookfunctions.allQuestionSetsAreFinished(current_user)

                                    return Response({})



                            current_answer = SymptomAnswerKit.objects.get(
                            question_belongs__question_id=current_que_id,
                            answer_content__iexact=current_sentence)

                            if current_que.que != None:
                                print("if icindeyim")
                                current_passed_question = FacebookPassedQuestions()
                                current_passed_question.que = current_que.que
                                current_passed_question.answer = current_answer
                                current_passed_question.save()
                                current_user.passed_questions.add(current_passed_question)



                            facebookfunctions.updateAnswersOfMUltiOptionJson(current_user,current_answer)



                            print( "henuz cokmedim",current_answer.activate.values_list())

                            activation_list = current_answer.activate.values_list()

                            print(activation_list.count())


                            #o cevabin bir activationı varsa
                            if activation_list.count() > 0:
                                for temp_set in activation_list:
                                    print(current_user.conversation_current_status)
                                    current_json = json.loads(current_user.conversation_current_status)
                                    #bu activation edilecek setlere ekle
                                    if temp_set[0] not in current_json["all_question_sets"]:
                                        current_json["question_sets"] = [temp_set[0]] + current_json["question_sets"]
                                        current_json["all_question_sets"] = [temp_set[0]] + current_json["all_question_sets"]
                                        current_user.conversation_current_status = json.dumps(current_json)

                        except:

                            current_que = SymptomQuestions.objects.get(question_id=
                                current_user.conversation_question_no)




                            if current_que.does_not_require_answer == True:

                                current_que_answers = SymptomAnswerKit.objects.filter(
                                    question_belongs_id=current_user.conversation_question_no).order_by(
                                    'answer_content').all()


                                print ("gor bu 1")
                                current_json = json.loads(current_user.conversation_current_status)

                                current_answer_selected = current_que_answers[0]

                                if current_user.conversation_question_set.question_set_id == 6 :
                                    #facebookfunctions.sendVideoIfExists(current_user, current_user.conversation_question_set)
                                    facebookfunctions.allQuestionSetsAreFinished(current_user)
                                    #self.classicalNLPAnalysis(current_user, current_sentence)

                                    # facebookfunctions.allQuestionSetsAreFinished(current_user)
                                    return Response({})



                                if current_answer_selected.selected_que != None:
                                    print("gor bu 2")

                                    next_question = current_answer_selected.selected_que
                                    current_user.conversation_question_no = next_question.question_id
                                    current_user.save()
                                    if len(next_question.answers.all()) > 0:
                                        facebookfunctions.sendQuickReply(next_question.question_content,current_user.conversation_user,next_question.answers.all())
                                    else:
                                        facebookfunctions.sendMessage(next_question.question_content,current_user.conversation_user)
                                    return Response({})

                                elif len(current_json["question_sets"]) < 1 and current_user.conversation_question_set.question_set_type == 'D':
                                    print("henuz cokmedim")

                                    print("classical :{}".format(current_sentence))
                                    facebookfunctions.allQuestionSetsAreFinished(current_user)
                                    self.classicalNLPAnalysis(current_user, current_sentence)

                                    # facebookfunctions.allQuestionSetsAreFinished(current_user)
                                    return Response({})

                                elif  len(current_json["question_sets"]) < 1:
                                    print("henuz cokmedim")

                                    print("classical :{}".format(current_sentence))
                                    self.whenCurrentSetOver(current_user,sender_id_int)

                                    #facebookfunctions.allQuestionSetsAreFinished(current_user)
                                    return Response({})




                            current_que_content = SymptomQuestions.objects.get(question_id=
                                current_user.conversation_question_no).question_content

                            current_que_answers = SymptomAnswerKit.objects.filter(
                                question_belongs_id=current_user.conversation_question_no).order_by('answer_content').all()

                            print("buradayim")

                            if len(current_que_answers)>0:

                                facebookfunctions.sendQuickReply("Cevabını anlamadım " + current_user.name + ". Bu soruyu şıklardan seçmeniz benim anlamam adına çok daha iyi olur veya bu şikayetinizle ilgili soru cevaplamamamk için konuşmayı bitirebilirsiniz." +
                                                    current_que_content,sender_id_int,current_que_answers,anlamadim=True)


                            elif current_que.question_set.question_set_type == 'D':
                                #facebookfunctions.sendMessage("Bitti",sender_id_int)
                                #facebookfunctions.sendVideoIfExists(current_user.conversation_user,
                                #                                    current_user.last_set_entry)
                                facebookfunctions.allQuestionSetsAreFinished(current_user)
                            return Response({})

                        print("coktum")

                        # kullanicinin sectigi cevap baska soruya bağlanıyorsa, ama siddetle ilgili degilse

                        if current_answer.selected_que != None:


                            print("cevap geld {0}:".format(current_sentence))


                            is_it_passable = checkQuestionIfItCanBePassable(current_user,current_answer.selected_que)


                            if type(is_it_passable) != bool and  is_it_passable.selected_que == None:
                                facebookfunctions.updateAnswersJson(current_user, is_it_passable)
                                print("buradayim")
                                self.whenCurrentSetOver(current_user,sender_id_int)
                                return Response({})
                            elif type(is_it_passable) != bool:
                                print("is it passable:{}".format(is_it_passable))
                                #facebookfunctions.updateAnswersJson(current_user,is_it_passable)
                                current_user.conversation_question_no = is_it_passable.selected_que_id
                                current_user.save()
                            else:
                                current_user.conversation_question_no = current_answer.selected_que_id
                                current_user.save()

                            """
                            cevabı alma vakti
                            current_answer  = SymptomAnsw   erKit.objects.get(question_belongs=)
                            """
                            #yeni sorunnu cevaplarini cek

                            answers = SymptomAnswerKit.objects.filter(question_belongs__question_id=current_user.conversation_question_no).all()
                            #yeni sorunun metnini cek

                            next_que = SymptomQuestions.objects.get(question_id=current_user.conversation_question_no)
                            next_que_content  = next_que.question_content

                            current_set_organ = current_user.conversation_question_set.question_set_body_part
                            current_set_affect = current_user.conversation_question_set.question_set_body_affect

                            not_found_flag = True
                            if next_que.question_choices == "P":
                                current_sentence_json = json.loads(current_user.conversation_last_sentence.decode('iso-8859-1'))

                                for organ in current_sentence_json:
                                    for affect in current_sentence_json[organ]:
                                        print("organ:{} affect:{}".format(organ,affect))
                                        if organ != current_set_organ and affect != current_set_affect:
                                            if next_que.question_organ == organ and next_que.question_affect == affect:
                                                not_found_flag = False
                                                next_ans = next_que.answers.get(veriyfy_symptom=True)
                                                """
                                                1-yollanacak yeni soru olabilir
                                                2-aktif olmayan set olmayabilir
                                                """
                                                current_json = json.loads(current_user.conversation_current_status)

                                                # question set listemde baska soru seti idsi var mı
                                                that_que = findClosestLink(current_answer.question_belongs,
                                                                           organListOfUser)
                                                print("that que:{}".format(that_que))

                                                facebookfunctions.applyPassableQuestionAdressing(that_que,sender_id_int,current_user,current_json)
                                                return Response({})


                                if not_found_flag:
                                                answers = SymptomAnswerKit.objects.filter(
                                                    question_belongs__question_id=next_que.question_id).order_by('answer_content').all()

                                                print(answers)

                                                if len(answers) > 0:
                                                    facebookfunctions.sendQuickReply(next_que_content, sender_id_int,
                                                                                     answers)





                            elif next_que.question_choices == "E":


                                facebookfunctions.sendMessage(next_que_content, sender_id_int)
                                return Response({})





                            else:
                                print("bu else icindeyim")


                                answers = SymptomAnswerKit.objects.filter(
                                    question_belongs__question_id=next_que.question_id).order_by('answer_content').all()

                                print(answers)

                                if len(answers)>0:

                                    facebookfunctions.sendQuickReply(next_que_content, sender_id_int, answers)
                                elif next_que.question_set.question_set_type == 'D':
                                    facebookfunctions.sendMessage(next_que_content, sender_id_int)
                                    #facebookfunctions.sendVideoIfExists(current_user.conversation_user,current_user.last_set_entry)
                                    facebookfunctions.allQuestionSetsAreFinished(current_user)
                                else:
                                    facebookfunctions.sendMessage(next_que_content,sender_id_int)
                                    return Response({})
                                    #facebookfunctions.allQuestionSetsAreFinished(current_user)
                            return Response({})


                        #kullanicinin sectigi sorunun devami yok, o set bitti
                        else:
                           self.whenCurrentSetOver(current_user,sender_id_int)



                    #bu else, kullanıcının şu an hiç bir şikayete, soru setine bapı bağlı olmadığını, yeni şikayet metni girdiğin
                    else:

                        #print(current_sentence)


                        current_user.conversation_last_sentence_text = current_sentence
                        current_user.save()


                        if len(current_sentence) < 3:
                            if "😘" in current_sentence or "😍" in current_sentence:
                                facebookfunctions.sendMessage("😘😘",sender_id_int)
                                return Response({})
                            elif current_sentence[0] in "😀😬😁😂😃😄😅😆😇😉😊🙂😋😌😙😜😝😛😎😏😶😐😑😒😡😓😪😢😥😖😩😤😔":
                                facebookfunctions.sendMessage(current_sentence,sender_id_int)
                                return HttpResponse({})







                        #Java'ya yolla cevabı al

                        if facebookfunctions.checkIfGenelChitQuestion(current_user, current_sentence):
                            return Response({})

                        current_sentence_fixed = prespellchecker.djangoFixSentence(current_sentence)



                        current_sentence_fixed,_ = prespellchecker.stemSentence(current_sentence_fixed)

                        classifier_result = prespellchecker.current_classifier.predict(current_sentence_fixed) 


                        res = interaction.symptom_extraction_connection.sendDataGetResponse(current_sentence_fixed)




                        if current_user.last_context == 'SF' and current_sentence.lower() in ["anlamadım","ne","peki","yani","ne yapmalıyım","yaani","ee","ne yapacağım","anlamadim","sorunum ne","sorun ne","bu kadar mı"]:
                            facebookfunctions.sendMessage("Yukarıda senin için hazırladığım liste senin cevaplarına göre eşleşen hastalıklardır. Hastalıkların üstüne tıklayarak bilgi alabilirsin. Ne yapman gerektiğini söyleyemem ",
                                                                                                                                                                        sender_id_int)
                            return Response({})


                        current_user.last_context = 'NI'
                        current_user.save()

                        #bunu cevir


                        """
                        tek tek yeni soru setini cekiyor
                        """
                        try:

                            res_json = json.loads(res.decode("iso-8859-9"))

                            #del res_json["genel"]


                            tekli_keys = list(res_json["tekli"].keys())

                            facebookfunctions.sendMessage(classifier_result,current_user.conversation_user)
                            

                            

                            if classifier_result == "adet":
                                del res_json["tekli"]
                                res_json["tekli"] = {classifier_result:{}}

                            elif classifier_result in ["estetik","psikoloji","hakaret","sigara","cinsel","kanser"]:
                                res_json["genel"] = {classifier_result:{}}
                            elif len(tekli_keys) > 0 and tekli_keys[0] in ["adet"] and tekli_keys != classifier_result:
                                
                                res_json["tekli"] = {classifier_result:{}}
                            elif classifier_result == "semptom" and "tedavi" in current_sentence_fixed:

                                del res_json["genel"]
                                if len(res_json["tekli"]) < 0:
                                    del res_json["tekli"]
                                
                                if len(res_json) > 0:
                                    first_key = list(res_json.keys())[0]
                                    if len(res_json[first_key])>0:
                                        first_affect = list(res_json[first_key].keys())[0]

                                        current_set = SymptomQuestionSet.objects.filter(question_set_body_part=first_key,
                                                                                        question_set_body_affect=first_affect)

                                        if current_set.count() > 0:
                                    
                                            facebookfunctions.sendMessage(current_set.question_set_whateatdrink,sender_id_int)
                                            return Response({})
                                


                            print(res)

                            #soruları yüklüyoe kullanıcıya ve soru setini getiryior
                            print("analyze oncesi")

                            current_user.message_no_of_his = current_user.message_no_of_his + 1
                            current_user.save()

                            answers = self.analyzeTextOfUserAndSetContent(res_json,current_user)



                            print("analyze sonrasi")

                            if answers == "tesekkur" or answers == "selam" or answers == "noset" \
                                    or answers == "anladim" or answers == "randevu"\
                                    or answers == "yardim" or answers == "personal"\
                                    or answers == "veda" or answers == "cinsel" or answers == "kilo" or answers == "ilac" or answers == "psikoloji":

                                return  Response({})


                            print(answers)

                            symptom_questions_current = SymptomQuestions.objects.get(question_id=current_user.conversation_question_no)


                            infos = symptom_questions_current.question_informations.all()
                            print("infos:{}".format(infos))


                            current_user.dontunderstand_no = 0
                            #current_user.last_disease_entered = ""
                            current_user.save()

                            facebookfunctions.sendMessage("Şikayetinizi girdiğiniz için teşekkür ederim." +  facebookfunctions.prepareSentenceForUnderstanding(current_user) + " Sana bir kaç sorum olacak 🚑",sender_id_int)

                            facebookfunctions.sendTypingView(sender_id_int)

                            for info in infos:
                                facebookfunctions.sendMessage(info.symptomquestioninfo_value, sender_id_int)

                            facebookfunctions.sendQuickReply(symptom_questions_current.question_content,sender_id_int,answers)



                            return Response({})

                        except:



                            current_sentence_fixed  = prespellchecker.djangoFixSentence(current_sentence)

                            #print("exceptteyim daha yollamadim {}".format(current_sentence_fixed))

                            current_sentence_stemmed,_ = prespellchecker.stemSentence(current_sentence_fixed)

                            

                            facebookfunctions.loadPossibleMedicationOrWikiOrDontUnderstandText(current_sentence_stemmed,current_user.conversation_user,current_user,extra_param=_)
                            return Response({})

                    return Response({"success":"ok"})

        return Response({"success":"not"})

    def get(self,request,format=None):
        return HttpResponse(self.request.GET['hub.challenge'])




class FacebookAppointmentRecord(APIView):

    def post(self,request,format=None):

        my_data = json.loads(request.body)

        section_part = my_data["section"]
        location = my_data["location"]
        user_id = my_data["user_id"]

        appointment = facebookfunctions.createTempAppointmentOrGetIt(user_id)

        return Response({"status":"sucess"})


