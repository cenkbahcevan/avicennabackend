# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class HeatlhInfo(models.Model):
    info_id = models.AutoField(primary_key=True)
    info_title = models.TextField(null=False)
    info_content = models.TextField(null=True)


class HealthQuestion(models.Model):
    question_id = models.AutoField(primary_key=True)
    question_content = models.TextField(null=False)


class HealthAnswers(models.Model):
    answer_id = models.AutoField(primary_key=True)
    answer_content = models.TextField(null=False)
    questions = models.ForeignKey(HealthQuestion,null=True,blank=False)
