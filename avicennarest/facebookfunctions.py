from .facebookmodels import FacebookUserModel,FacebookAppointment,FacebookSymptomComplaint,FacebookNonExistingSetComplaint
import json
import requests
from . import place_finder
from avicennarest.models import HealthWikiPage,SymptomQuestionSet,SymptomQuestions,SymptomAnswerKit,AvicennaNews,AvicennaNewsCategory,AvicennaSentencesEntered,SetExtraInfo,SetVideos,SimpleContextText,SampleText,Doctor,SymptomDoctor
from . import nlp_helper
import time
import datetime
import operator
import re
import pytz
from django.utils import timezone


days_dict = {
    0:"Pazartesi",
    1:"Salı",
    2:"Çarşamba",
    3:"Perşembe",
    4:"Cuma",
    5:"Cumartesi",
    6:"Pazar"
}

first_prefix_for_user_info = "https://graph.facebook.com/v3.0/"
acess_token =  "EAATU4NXzrDgBAHKNUsLJNUnABjUrGtZBJsQ3drwSZAoY8fqliNlLBLTROy22ojdvkANPLZCEMUoQDCdDEEGzMl0FVs466CzP9bL8GFoZCbr6vNGyjf9a4MAV3At6O9VFnpD4EO6h4g7kpin1LiyZAgQD5FtMFA33ktdE9wgMGKkt6ZBbZCZBOIPs"


acess_token_en = "EAATU4NXzrDgBAMz1i5cvqduhuX0N3pZCAixNs2bXGShT7MsoJ54HLPWhZAzZCWPPIuFnjsHwKyIaiEbHFAVEUN3NJhaFE1wxJLZBw5VyR24s7HJV0kf5s7n1XDWcYl7Nq8FiI4va953LN6ZCEafcrnWOfKAJuSqeGZC0RPZCxdwxP5Ey28GvoTN"




def construct_quick_reply(answers,anlamadim=False,page_source="tr"):
    X = []
    dict = {}
    if anlamadim and page_source == "tr":
        dict = {
            "content_type": "text",
            "title": "Konuşmayı bitir",
            "payload": "DEVELOPER_DEFINED_PAYLOAD_FOR_PICKING_RED"
        }
    elif anlamadim:
        dict = {
            "content_type": "text",
            "title": "End conversation",
            "payload": "DEVELOPER_DEFINED_PAYLOAD_FOR_PICKING_RED"
        }
    if len(dict)>0:
        X.append(dict)
    for answer in answers:
        # print(answer)
        if page_source == "tr":
            dict = \
                {
                    "content_type": "text",
                    "title": answer.answer_content,
                    "payload": "DEVELOPER_DEFINED_PAYLOAD_FOR_PICKING_RED"}
            X.append(dict)
        else:
            dict = \
                {
                    "content_type": "text",
                    "title": answer.answer_content_en,
                    "payload": "DEVELOPER_DEFINED_PAYLOAD_FOR_PICKING_RED"}
            X.append(dict)

    return X



def sendNeGelir(set_id,user_id):
    params = {
        "access_token": "EAATU4NXzrDgBAFCUZCk4OGjQ3MecGLhE0hA0MnZAhYyzkyu9NIygJhGKZBunS8RGnhg3cI4cgDRIBW0AJFFZB5QDqmgRD8w9P26Y3dJ4zibS5GiiA41YTH3ZCZB6azECzAQiLlwXbYLiXBlBOJFnMZBBtwFeGPjEVcRZC5jij33AiwZDZD"
    }
    headers = {
        "Content-Type": "application/json"
    }
    data = json.dumps({
        "recipient": {
            "id": user_id
        },
        "message": {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "button",
                    "text":"Lenia, ne iyi gelir makelesi🌱",
                    "buttons": [                  {
                            "type": "web_url",
                            "url": "https://www.lenia.ist/neiyigelir?sym_no=" + str(set_id),
                            "title": "Okumak için tıkla🌱 ",
                            "webview_height_ratio": "full"
                    }]
                }
            }
        }})

    r = requests.post("https://graph.facebook.com/v2.6/me/messages", params=params, headers=headers, data=data)
    if r.status_code != 200:
        print(r.text)


def construct_new_item(news):
    X = []
    for new in news:
        current_new = {}
        current_new["title"] = new.news_title
        current_new["image_url"] = new.news_image_url
        current_new["subtitle"] = new.news_summary
        current_new["default_action"]  = {
            "type": "web_url",
            "url": "https://www.lenia.ist/makeRedirect?id=" + str(new.news_id),
            "messenger_extensions": True,
            "webview_height_ratio": "tall",
            "fallback_url": "https://www.lenia.ist/makeRedirect?id=" + str(new.news_id)
        }
        X.append(current_new)

    return X


def construct_result_item(results,page_location="tr"):
    print("RESULTS:{}".format(results))
    end_results = []
    for result in results:
        current_wiki = result.disease_wiki
        if current_wiki != None:
            current_new = {}
            if page_location == "en":
                current_new["title"] = current_wiki.page_title_en
                current_new["subtitle"] = "Click to view"
                current_link = current_wiki.webmd_link
                #print("Current link:{}".format(current_link))
                if len(current_link) < 5:
                    current_link = "https://www.lenia.ist"
                current_new["default_action"] = {
                    "type": "web_url",
                    "url": current_link,
                    "messenger_extensions": True,
                    "webview_height_ratio": "tall",
                    "fallback_url": current_wiki.webmd_link
                }
            else:
                current_new["title"] = current_wiki.page_title
                current_new["subtitle"] = current_wiki.page_summary
                current_new["default_action"] = {
                    "type": "web_url",
                    "url": "https://lenia.ist/loadWikiPage?page_no=" + str(current_wiki.page_id),
                    "messenger_extensions": True,
                    "webview_height_ratio": "tall",
                    "fallback_url": "https://lenia.ist/loadWikiPage?page_no=" + str(current_wiki.page_id)
                }





            end_results.append(current_new)
        else:
            current_new = {}
            current_new["title"] = result.disease_name
            current_new["subtitle"] =   result.disease_name + " hastalığı için sistemimizde veri yok"
            current_new["default_action"] = {
                "type": "web_url",
                "url": "https://lenia.ist/",
                "messenger_extensions": True,
                "webview_height_ratio": "tall",
                "fallback_url": "https://lenia.ist"
            }
            end_results.append(current_new)



    return end_results

def sendResults(diagnosis_results,sender_id_int):
    current_user = FacebookUserModel.objects.get(conversation_user=sender_id_int)

    current_token = acess_token
    page_source = "tr"
    if current_user.page_location == "en":
        current_token = acess_token_en
        page_source = "en"


    diagnosis_results = dict(sorted(diagnosis_results.items(), key=operator.itemgetter(1)))

    if len(diagnosis_results)<1:

        if page_source == "en":
            sendMessage("I could not find suitable disease for you",sender_id_int,"en")
        else:
            sendMessage("Cevaplarınızı inceleyerek sana uygun bir hastalık bulamadım. Şikayetlerinin ciddi olduğunu düşünüyorsan doktora danışmalısın.",sender_id_int)
        return
    elif len(diagnosis_results)<2:
        curr_disease = list(diagnosis_results.keys())[0]
        print("İçerideyim")
        if curr_disease.disease_wiki != None:
            sendRealResultPage("",sender_id_int,curr_disease.disease_wiki.page_id)
            return
        else:
            if page_source == "en":
                sendMessage(curr_disease.disease_name,sender_id_int,"en")
            else:
                sendMessage(curr_disease.disease_name,sender_id_int)
            return




    sending_data = []
    if len(diagnosis_results)>=3 and len(diagnosis_results)<9:
        divided_no = round((len(diagnosis_results))/2)
        part_a = list(diagnosis_results.keys())[0:divided_no]
        part_b = list(diagnosis_results.keys())[divided_no:]
        if len(part_b) < 2:
            all_part = part_a + part_b
            sending_data.append(all_part)
        else:
            sending_data.append(part_a)
            sending_data.append(part_b)
    else:
        part_a = list(diagnosis_results.keys())[0:4]
        part_b = list(diagnosis_results.keys())[4:9]
        sending_data.append(part_a)
        sending_data.append(part_b)

    for diagnosis_list in sending_data:
        my_news_curr = construct_result_item(diagnosis_list)
        if page_source == "en":
            my_news_curr = construct_result_item(diagnosis_list,page_location="en")

        params = {
            "access_token": current_token
        }
        headers = {
            "Content-Type": "application/json"
        }
        data = json.dumps({
            "recipient": {
                "id": sender_id_int
            },
            "message": {
                "attachment": {
                    "type": "template",
                    "payload": {
                        "template_type": "list",
                        "top_element_style": "compact",
                        "elements":my_news_curr
                    }
                }
            }})

        r = requests.post("https://graph.facebook.com/v2.6/me/messages", params=params, headers=headers, data=data)
        if r.status_code != 200:
            print(r.text)


def sendMyNews(new1,new2,new3,sender_id_int):
    my_news_curr = construct_new_item([new1,new2,new3])

    params = {
        "access_token": "EAATU4NXzrDgBAFCUZCk4OGjQ3MecGLhE0hA0MnZAhYyzkyu9NIygJhGKZBunS8RGnhg3cI4cgDRIBW0AJFFZB5QDqmgRD8w9P26Y3dJ4zibS5GiiA41YTH3ZCZB6azECzAQiLlwXbYLiXBlBOJFnMZBBtwFeGPjEVcRZC5jij33AiwZDZD"
    }
    headers = {
        "Content-Type": "application/json"
    }
    data = json.dumps({
        "recipient": {
            "id": sender_id_int
        },
        "message": {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "list",
                    "top_element_style": "compact",
                    "elements":my_news_curr
                }
            }
        }})

    r = requests.post("https://graph.facebook.com/v2.6/me/messages", params=params, headers=headers, data=data)
    if r.status_code != 200:
        print(r.text)


def sendTypingView(sender_id_int,current_lang="tr"):
    current_token = acess_token
    if current_lang == "en":
        current_token = acess_token_en
    params = {
        "access_token": current_token
    }
    headers = {
        "Content-Type": "application/json"
    }
    data = json.dumps({
        "recipient": {
            "id": sender_id_int
        },
        "sender_action": "typing_on"
        })

    r = requests.post("https://graph.facebook.com/v2.6/me/messages", params=params, headers=headers, data=data)
    if r.status_code != 200:
        print(r.text)







def construct_quick_reply_raw(answers):
    X = []
    for answer in answers:
        # print(answer)
        dict = \
            {
                "content_type": "text",
                "title": answer,
                "payload": "DEVELOPER_DEFINED_PAYLOAD_FOR_PICKING_RED"}
        X.append(dict)
    return X

def construct_quick_reply_raw(answers):
    X = []
    for answer in answers:
        # print(answer)
        dict = \
            {
                "content_type": "text",
                "title": answer,
                "payload": "DEVELOPER_DEFINED_PAYLOAD_FOR_PICKING_RED"}
        X.append(dict)
    return X


# Anlamadim gibi, şıkkı olmayan basit mesajları yolluyor
def sendApprovalMessage(message, to):
    params = {
        "access_token": "EAATU4NXzrDgBAFCUZCk4OGjQ3MecGLhE0hA0MnZAhYyzkyu9NIygJhGKZBunS8RGnhg3cI4cgDRIBW0AJFFZB5QDqmgRD8w9P26Y3dJ4zibS5GiiA41YTH3ZCZB6azECzAQiLlwXbYLiXBlBOJFnMZBBtwFeGPjEVcRZC5jij33AiwZDZD"
    }
    headers = {
        "Content-Type": "application/json"
    }
    data = json.dumps({
        "recipient": {
            "id": to
        },
        "message": {
            "text": message,

        }
    })
    r = requests.post("https://graph.facebook.com/v2.6/me/messages", params=params, headers=headers, data=data)
    if r.status_code != 200:
        print(r.text)


def sendDontUnderStandMessage(sender_id_int,current_user):
    if current_user.dontunderstand_no == 0:
        first_name = current_user.name.split(" ")[0]
        my_json = json.loads(current_user.conversation_last_sentence)
        if len(my_json) > 2:
            my_json.pop("genel", None)
            my_json.pop("tekli", None)

            my_organ = list(my_json.keys())[0]
            sendMessage(
                list(my_json.keys())[0] + " şikayetini tam olarak belirtir misin? Farklı kelimelerle anlatmalısın. Bana başım ağrıyor gibi "
                        "şikayetlerinizi söyleyip, bu şikayetlerin veya hastalıklar hakkında bilgi alabilirsiniz ⌛😟😟",
                sender_id_int)
            
            my_symptom_list = []

            my_possible_set_list = SymptomQuestionSet.objects.filter(question_set_body_part=my_organ)

            if my_possible_set_list.count()>0 and my_organ not in ["genel","tekli"]:
                possible_text = "Girdiğiniz vücut kısmı ile altta bulunan şikayetleri cevaplayabiliriz.\n"
                for current_set in my_possible_set_list:
                    possible_text = "-" + possible_text + current_set.question_set_name + "\n"
                    my_symptom_list.append(current_set.question_set_name)


                sendQuickReplyRaw(possible_text, sender_id_int,my_symptom_list)




            current_user.dontunderstand_no = 1
            current_user.save()
        else:
            sendMessage("Anlamadım. Tekrarlar mısın " + first_name  +  "? Bana başım ağrıyor gibi "
                        "şikayetlerinizi söyleyip, bu şikayetlerin veya hastalıklar hakkında bilgi alabilirsiniz ⌛😟", sender_id_int)
            current_user.dontunderstand_no = 1
            current_user.save()

    else:
        my_json = json.loads(current_user.conversation_last_sentence)
        if len(my_json)>2:
            my_json.pop("genel",None)
            my_json.pop("tekli",None)
            my_organ = list(my_json.keys())[0]
            sendMessage(list(my_json.keys())[0] + " şikayetini anlamadım. Farklı kelimelerle anlatmalısın⌛😟😟", sender_id_int)

            my_possible_set_list = SymptomQuestionSet.objects.filter(question_set_body_part=my_organ)

            my_symptom_list = []

            if my_possible_set_list.count() > 0 and my_organ not in ["genel", "tekli"]:
                possible_text = "Girdiğiniz vücut kısmı ile altta bulunan şikayetleri cevaplayabiliriz.\n"
                for current_set in my_possible_set_list:
                    possible_text = possible_text + current_set.question_set_name + "\n"
                    my_symptom_list.append(current_set.question_set_name)

                sendQuickReplyRaw(possible_text, sender_id_int,my_symptom_list)
        else:

            first_name = current_user.name.split(" ")[0]
            sendMessage("Yine anlamadım " + first_name  + ". Tekrarlar mısın? ⌛😟😟", sender_id_int)

def sendLocations(sender_id_int,my_value,place_text):
    lat = my_value["coordinates"]["lat"]
    lon = my_value["coordinates"]["long"]
    print(str(lat) + " " + str(lon))
    sendMessage("Sizin için bulduğumuz konumlar aşağıda",sender_id_int)

    cordinates_in_form = str(lat) + "," +  str(lon)

    places_dict = place_finder.getCloserPlaces(place_text,cordinates_in_form)

    end_point_acik = 3

    if end_point_acik>len(places_dict["acik"]):

        end_point_acik = len(places_dict["acik"])

        for element in places_dict["acik"][:end_point_acik]:
            sendLocation(sender_id_int,element["lat"],element["lon"],element["title"])
    else:
        for element in places_dict["acik"][:end_point_acik]:
            sendLocation(sender_id_int,element["lat"],element["lon"],element["title"])

def saveSentence(sentence):
    sentence_to_be_recorded = AvicennaSentencesEntered()
    sentence_to_be_recorded.sentence_text = sentence
    sentence_to_be_recorded.save()


def applyPossibleNewsSend(current_sentence,sender_id_int,current_user,possible_news):
    if len(possible_news[0])>0 and isinstance(possible_news[0][0], HealthWikiPage):
        #sendMessage("Anladım. " + possible_news[0][0].page_title + " hakkında sizin için bulduğum sonuçlar şunlar:👇", sender_id_int)
        pass
    for res in possible_news[0]:
        for current_part in possible_news[1]:
            if isinstance(res, HealthWikiPage):
                #print(res.page_id)
                his_text = returnCurrentText(res.page_id, current_part)
                sendMessage(his_text, sender_id_int)


                current_day = datetime.datetime.today().day
                current_month = datetime.datetime.today().month

                current_user.last_disease_entered = res
                current_user.last_disease_entered_day = current_day
                current_user.last_disease_entered_month = current_month
                current_user.save()
                my_answer_list = ["Anlamı", "Tedavi yolu", "Belirtileri"]

                sendQuickReplyRaw(current_user.last_disease_entered.page_title + " hakkında ne öğrenmek istersiniz.", sender_id_int,
                                  my_answer_list)

                saveSentence(current_sentence)
                return

        if isinstance(res, HealthWikiPage):
            sendRealResultPage("Aradığınızın", sender_id_int, res.page_id)

            current_day = datetime.datetime.today().day
            current_month = datetime.datetime.today().month

            current_user.last_disease_entered_day = current_day
            current_user.last_disease_entered_month = current_month
            current_user.last_disease_entered = res
            current_user.save()

            my_answer_list = ["Anlamı", "Tedavi yolu", "Belirtileri"]

            sendQuickReplyRaw(res.page_title + " hakkında aşağıda bulunan şıkları kullanarak hızlıca bilgi alabilirsiniz.",
                              sender_id_int, my_answer_list)

            saveSentence(current_sentence)
            return
        else:
            sendMessage(
                res.avm_name + " hakkında bilgiler\n____________________________\n"
                               " Ne zaman kullanılır\n____________________________\n" + res.avm_when + "\n", sender_id_int)
            sendMessage("Yan etkileri\n____________________________\n" + res.avm_side + "\n", sender_id_int)
            saveSentence(current_sentence)
            #current_user.last_disease_entered = ""
            #current_user.save()
            return


def in_sentence(current_sentence):
    current_sentence_splitted = current_sentence.split(" ")
    arr = ["Anlamı", "Belirtileri", "Tedavi","Ne"]
    for element in arr:
        if element in current_sentence_splitted:
            return True
    return False



def loadPossibleMedicationOrWikiOrDontUnderstandText(current_sentence,sender_id_int,current_user,extra_param=None):
    #sendMessage("fonksiyon basladi",sender_id_int)
    current_sentence_untouched = current_user.conversation_last_sentence_text
    #sendMessage(current_sentence_untouched,sender_id_int)
    possible_news = nlp_helper.getTrueHealthPage(current_sentence_untouched)

    #sendMessage("metinleri alim",sender_id_int)
    if isinstance(possible_news,str):
        sendMessage(possible_news,sender_id_int)
        return
    print("possible news:{}".format(possible_news))

    if len(possible_news[0]) > 0:
        #sendMessage("haberleri alim",sender_id_int)
        applyPossibleNewsSend(current_sentence,sender_id_int,current_user,possible_news)

    elif current_user.last_disease_entered != None and in_sentence(current_sentence_untouched):
        new_current_sentence = current_user.last_disease_entered.page_title + " " + current_sentence
        print("new current sentence:{}".format(new_current_sentence))
        possible_news = nlp_helper.getTrueHealthPage(new_current_sentence)
        applyPossibleNewsSend(new_current_sentence,sender_id_int,current_user,possible_news)

    elif extra_param != None and extra_param != "":

        current_user.message_failure_of_his = current_user.message_failure_of_his + 1
        current_user.save()

        sendMessage(extra_param + " şikayetin var, anladım. Vücudunun hangi kısmının bu durumu yaşadığını daha iyi ifade eder misin?",sender_id_int)
    else:

        #current_user.last_disease_entered = None
        #current_user.save()
        current_user.message_failure_of_his = current_user.message_failure_of_his + 1
        current_user.save()

        sendDontUnderStandMessage(sender_id_int,current_user)




def sendRealResultPage(message,to,code,extra_param=None):


    current_user = FacebookUserModel.objects.get(conversation_user=to)

    current_token = acess_token
    page_source = "tr"
    if current_user.page_location == "en":
        current_token = acess_token_en
        page_source = "en"

    current_res = HealthWikiPage.objects.get(pk=code)

    current_page_title = current_res.page_title

    current_link = "https://lenia.ist/loadWikiPage?page_no=" + str(code)

    current_summary = current_res.page_summary
    if current_user.page_location == "en":
        current_page_title = current_res.page_title_en
        current_summary = current_res.page_summary_en
        if len(current_res.webmd_link) > 3 :
            current_link = current_res.webmd_link


    current_title = "Detaylı oku"
    if page_source == "en":
        current_summary = current_res.page_summary_en
        current_title = "Read Details"






    params = {
        "access_token": current_token
    }
    headers = {
        "Content-Type": "application/json"
    }
    data = json.dumps({
        "recipient": {
            "id": to
        },
        "message": {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": [
                        {
                            "title": current_page_title,
                            "image_url": "https://lenia.ist/static/1.png",
                            "subtitle": current_summary,
                            "default_action": {
                                "type": "web_url",
                                "url": current_link ,
                                "messenger_extensions": "True",
                                "webview_height_ratio": "tall",
                                "fallback_url": current_link
                            },
                            "buttons": [
                                {
                                    "type": "web_url",
                                    "url": current_link,
                                    "title": current_title
                                },
                            ]
                        }
                    ]
                }
            }
        }
    })
    r = requests.post("https://graph.facebook.com/v2.6/me/messages", params=params, headers=headers, data=data)
    if r.status_code != 200:
        print(r.text)

def sendDirectLink(extra,to,extra_param=None):
    params = {
        "access_token": "EAATU4NXzrDgBAFCUZCk4OGjQ3MecGLhE0hA0MnZAhYyzkyu9NIygJhGKZBunS8RGnhg3cI4cgDRIBW0AJFFZB5QDqmgRD8w9P26Y3dJ4zibS5GiiA41YTH3ZCZB6azECzAQiLlwXbYLiXBlBOJFnMZBBtwFeGPjEVcRZC5jij33AiwZDZD"
    }
    headers = {
        "Content-Type": "application/json"
    }
    data = json.dumps({
        "recipient": {
            "id": to
        },
        "message": {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": [
                        {
                            "title": extra.page_title,
                            "image_url": "https://lenia.ist/static/avicenna.png",
                            "subtitle": extra.page_title,
                            "default_action": {
                                "type": "web_url",
                                "url": extra.page_link ,
                                "messenger_extensions": "True",
                                "webview_height_ratio": "tall",
                                "fallback_url": extra.page_link
                            },
                            "buttons": [
                                {
                                    "type": "web_url",
                                    "url": extra.page_link ,
                                    "title": "Detaylı oku"
                                },
                            ]
                        }
                    ]
                }
            }
        }
    })
    r = requests.post("https://graph.facebook.com/v2.6/me/messages", params=params, headers=headers, data=data)
    if r.status_code != 200:
        print(r.text)

def sendAppointmentDateTemplate(to):
    now = time.strftime("%w")
    turkish_form = ""
    if now == '6':
        turkish_form = days_dict[0]
    else:
        turkish_form = days_dict[int(now) + 1]

    params = {
        "access_token": "EAATU4NXzrDgBACzZAi6XjF8op02kuvSgk0eTibUo1INz454tRW1dWts54ami9IOTZBe0wMSCfRvhIxLxa4J1dlDg6Wb7MvfaQo3CY5kNZBq0AKUjS03qn5abhy8v65YmZCDOoC8mHHEcIZA6MWh7I3AH90ekevvoddz7FODI804qJyExu4vZC9"
    }
    headers = {
        "Content-Type": "application/json"
    }
    data = json.dumps({
        "recipient": {
            "id": to
        },
        "message": {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": [
                        {
                            "title": "Tarih seç",
                            "image_url": "https://www.lenia.ist/static/acibadem.jpg",
                            "subtitle": "Randevu almak istediğiniz tarihi seçin" ,
                            "default_action": {
                                "type": "web_url",
                                "url": "https://lenia.ist",
                                "messenger_extensions": "true",
                                "webview_height_ratio": "tall",
                                "fallback_url": "https://lenia.ist"
                            },
                            "buttons": [
                                {
                                  "type": "postback",
                                    "title":"Yarın",
                                    "payload":"TomorrowApp"
                                },
                                {
                                    "type":"postback",
                                    "title":turkish_form,
                                    'payload':turkish_form + "App"
                                },
                                {
                                    "type": "web_url",
                                    "url": "https://www.lenia.ist/randevu" ,
                                    "title": "Farklı tarih",
                                    "messenger_extensions": "true",
                                },

                            ]
                        }
                    ]
                }
            }
        }
    })
    r = requests.post("https://graph.facebook.com/v2.6/me/messages", params=params, headers=headers, data=data)
    if r.status_code != 200:
        print(r.text)



# şıkkı olan mesajları yolluyor
def sendQuickReply(message, to, answers,anlamadim=False):

    current_user = FacebookUserModel.objects.get(conversation_user=to)

    current_token = acess_token
    page_source = "tr"
    if current_user.page_location == "en":
        current_token = acess_token_en
        page_source = "en"

    params = {
        "access_token": current_token
    }
    headers = {
        "Content-Type": "application/json"
    }
    data = json.dumps({
        "recipient": {
            "id": to
        },
        "message": {
            "text": message,
            "quick_replies": construct_quick_reply(answers,anlamadim,page_source)
        }
    })
    r = requests.post("https://graph.facebook.com/v2.6/me/messages", params=params, headers=headers, data=data)
    if r.status_code != 200:
        print(r.text)

def sendQuickReplyRaw(message,to,answers):
    current_user = FacebookUserModel.objects.get(conversation_user=to)

    current_token = acess_token
    page_source = "tr"
    if current_user.page_location == "en":
        current_token = acess_token_en
        page_source = "en"
    params = {
        "access_token": current_token
    }
    headers = {
        "Content-Type": "application/json"
    }
    data = json.dumps({
        "recipient": {
            "id": to
        },
        "message": {
            "text": message,
            "quick_replies": construct_quick_reply_raw(answers)
        }
    })
    r = requests.post("https://graph.facebook.com/v2.6/me/messages", params=params, headers=headers, data=data)
    if r.status_code != 200:
        print(r.text)


def sendMessage(message, to,current_lang="tr"):
    current_token = acess_token
    if current_lang == "en":
        current_token = acess_token_en
    params = {
        "access_token": current_token
    }
    headers = {
        "Content-Type": "application/json"
    }
    data = json.dumps({
        "recipient": {
            "id": to
        },
        "message": {
            "text": message,

        }

    })
    print(data)
    r = requests.post("https://graph.facebook.com/v2.6/me/messages", params=params, headers=headers, data=data)
    if r.status_code != 200:
        print(r.text)


"""
Eğer kayıtlı değilse, Facebook kulanıcsını kaydediyor.
Her türlü  kullanıcıyı return ediyor.
"""

def sendLocationMessage(message,to,lang="en"):
    current_token = acess_token
    if lang == "en":
        current_token = acess_token_en
    params = {
        "access_token": current_token
    }
    headers = {
        "Content-Type": "application/json"
    }
    data = json.dumps({
        "recipient": {
            "id": to
        },
        "message": {
            "text": message,
            "quick_replies": [{"content_type":"location"}]
        }
    })
    r = requests.post("https://graph.facebook.com/v2.6/me/messages", params=params, headers=headers, data=data)
    if r.status_code != 200:
        print(r.text)

def sendMessageWithLinkPreview(to,title,summary):
    params = {
        "access_token": "EAATU4NXzrDgBAFCUZCk4OGjQ3MecGLhE0hA0MnZAhYyzkyu9NIygJhGKZBunS8RGnhg3cI4cgDRIBW0AJFFZB5QDqmgRD8w9P26Y3dJ4zibS5GiiA41YTH3ZCZB6azECzAQiLlwXbYLiXBlBOJFnMZBBtwFeGPjEVcRZC5jij33AiwZDZD"
    }
    headers = {
        "Content-Type": "application/json"
    }
    data = json.dumps({
        "recipient": {
            "id": to
        },
        "message": {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": [
                        {
                            "title": title,
                            "image_url": "http://avicenna.ngrok.io/static/cenk.png",
                            "subtitle": summary,
                            "default_action": {
                                "type": "web_url",
                                "url": "https://lenia.ist/",
                                "messenger_extensions": "True",
                                "webview_height_ratio": "tall",
                                "fallback_url": "https://lenia.ist/"
                            },
                            "buttons": [
                                {
                                    "type": "web_url",
                                    "url": "https://petersfancybrownhats.com",
                                    "title": "Detaylı oku"
                                },
                            ]
                        }
                    ]
                }
            }
        }
    })
    r = requests.post("https://graph.facebook.com/v2.6/me/messages", params=params, headers=headers, data=data)
    if r.status_code != 200:
        print(r.text)


def sendMedia(to):
    params = {
        "access_token": "EAATU4NXzrDgBAFCUZCk4OGjQ3MecGLhE0hA0MnZAhYyzkyu9NIygJhGKZBunS8RGnhg3cI4cgDRIBW0AJFFZB5QDqmgRD8w9P26Y3dJ4zibS5GiiA41YTH3ZCZB6azECzAQiLlwXbYLiXBlBOJFnMZBBtwFeGPjEVcRZC5jij33AiwZDZD"
    }
    headers = {
        "Content-Type": "application/json"
    }
    data = json.dumps({
        "recipient": {
            "id": to
        },
    "message": {
        "attachment": {
            "type": "template",
            "payload": {
                "template_type": "media",
                "elements": [
                    {
                        "media_type": "<image|video>",
                        "attachment_id": "<ATTACHMENT_ID>"
                    }
                ]
            }
        }
    }
    })
    r = requests.post("https://graph.facebook.com/v2.6/me/messages", params=params, headers=headers, data=data)
    if r.status_code != 200:
        print(r.text)


def sendLocation(to,lat,lon,title):
    params = {
        "access_token": "EAATU4NXzrDgBAFCUZCk4OGjQ3MecGLhE0hA0MnZAhYyzkyu9NIygJhGKZBunS8RGnhg3cI4cgDRIBW0AJFFZB5QDqmgRD8w9P26Y3dJ4zibS5GiiA41YTH3ZCZB6azECzAQiLlwXbYLiXBlBOJFnMZBBtwFeGPjEVcRZC5jij33AiwZDZD"
    }
    headers = {
        "Content-Type": "application/json"
    }
    data = json.dumps({
        "recipient": {
            "id": to
        },
        "message": {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": {
                        "element": {
                            "title": title,
                            "image_url": "https://maps.googleapis.com/maps/api/staticmap?size=764x400&center=" + str(lat) + "," + str(lon) + "&zoom=25&markers=" + str(lat) + "," + str(lon),
                            "item_url": "http://maps.apple.com/maps?q=" + str(lat) + "," + str(lon) + "&z=16"
                        }
                    }
                }
            }
        }

    })
    r = requests.post("https://graph.facebook.com/v2.6/me/messages", params=params, headers=headers, data=data)
    if r.status_code != 200:
        print(r.text)




def updateNames(sender_id):
    current_user = FacebookUserModel.objects.get(conversation_user=sender_id)
    if  "Unknown" in current_user.name:
        user_json = getInformationOfUser2(str(sender_id), page_location="en")
        current_user.name = user_json["first_name"] + " " + user_json["last_name"]
        current_user.profile_pic = user_json["profile_pic"]
        current_user.save()
    return




def construct_or_get_user(sender_id,source="tr"):
    sender_exist = FacebookUserModel.objects.filter(conversation_user=sender_id).exists()


    current_user = None

    if not sender_exist:
        current_user = FacebookUserModel()
        if source == "en":
            sendMessage("en user bak",sender_id,"en")
            current_user.page_location = "en"
            current_user.conversation_user = sender_id
            current_user.conversation_question_no = 0
            current_user.save()

        user_json = getInformationOfUser(str(sender_id),source)
        current_user.conversation_user = sender_id

        current_user.creation_date = datetime.datetime.today()

        try:
            #sendMessage(str(sender_id),sender_id,"en")
            #sendMessage(str(user_json),sender_id,"en")
            current_user.name = user_json["first_name"] + " " + user_json["last_name"]
            current_user.profile_pic = user_json["profile_pic"]
        except Exception as e:

            #sendMessage("bu kısım " + str(e),sender_id,"en")
            current_user.name = "Unknown Unknown"
            current_user.profile_pic = "Unknown"
            #sendMessage("bak source->" + source,sender_id,"en")
            if source == "en":
                current_user.page_location = "en"
                current_user.save()


        print(sender_id)
        current_user.conversation_question_no = -1
        #current_user.conversation_count_no = 1
        #print("henüz çökmedim")
        current_user.save()
    else:
        current_user = FacebookUserModel.objects.get(conversation_user=sender_id)


    return current_user


def sendNews(to,news_summary,news_link,news_title,news_image):
    params = {
        "access_token": "EAATU4NXzrDgBAFCUZCk4OGjQ3MecGLhE0hA0MnZAhYyzkyu9NIygJhGKZBunS8RGnhg3cI4cgDRIBW0AJFFZB5QDqmgRD8w9P26Y3dJ4zibS5GiiA41YTH3ZCZB6azECzAQiLlwXbYLiXBlBOJFnMZBBtwFeGPjEVcRZC5jij33AiwZDZD"
    }
    headers = {
        "Content-Type": "application/json"
    }
    data = json.dumps({
        "recipient": {
            "id": to
        },
        "message": {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": [
                        {
                            "title": news_title,
                            "image_url": news_image,
                            "subtitle": news_summary,
                            "default_action": {
                                "type": "web_url",
                                "url": "https://www.lenia.ist/makeRedirect?id=" + str(news_link),
                                "messenger_extensions": "True",
                                "webview_height_ratio": "tall",
                                "fallback_url": "https://www.lenia.ist/"
                            },
                            "buttons": [
                                {
                                    "type": "web_url",
                                    "url": "https://www.lenia.ist/makeRedirect?id=" + str(news_link),
                                    "title": "Detaylı oku"
                                },
                            ]
                        }
                    ]
                }
            }
        }
    })
    r = requests.post("https://graph.facebook.com/v2.6/me/messages", params=params, headers=headers, data=data)
    if r.status_code != 200:
        print(r.text)






def sendAppointmentView(to):
    params = {
        "access_token": "EAATU4NXzrDgBAFCUZCk4OGjQ3MecGLhE0hA0MnZAhYyzkyu9NIygJhGKZBunS8RGnhg3cI4cgDRIBW0AJFFZB5QDqmgRD8w9P26Y3dJ4zibS5GiiA41YTH3ZCZB6azECzAQiLlwXbYLiXBlBOJFnMZBBtwFeGPjEVcRZC5jij33AiwZDZD"
    }
    headers = {
        "Content-Type": "application/json"
    }
    data = json.dumps({
        "recipient": {
            "id": to
        },
        "message": {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": [
                        {
                            "title": "Randevu al",
                            "image_url": "https://www.lenia.ist/static/acibadem.jpg",
                            "subtitle": "Bu linke tıklayarak randevu alabilirsiniz",
                            "default_action": {
                                "type": "web_url",
                                "url": "https://www.lenia.ist/randevu",
                                "messenger_extensions": "True",
                                "webview_height_ratio": "tall",
                                "fallback_url": "https://www.lenia.ist/"
                            },
                            "buttons": [
                                {
                                    "type": "web_url",
                                    "url": "https://www.lenia.ist/randevu",
                                    "title": "Detaylı oku"
                                },
                            ]
                        }
                    ]
                }
            }
        }
    })
    r = requests.post("https://graph.facebook.com/v2.6/me/messages", params=params, headers=headers, data=data)
    if r.status_code != 200:
        print(r.text)



def returnCurrentText(wiki_id, current_section):
    if current_section == 0:
        return HealthWikiPage.objects.get(pk=wiki_id).page_symtpoms
    elif current_section == 1:
        return HealthWikiPage.objects.get(pk=wiki_id).page_summary
    elif current_section == 2:
        return HealthWikiPage.objects.get(pk=wiki_id).page_prevention
    elif current_section == 3:
        return HealthWikiPage.objects.get(pk=wiki_id).page_prevention




def updateAnswersJson(current_user,current_answer):

        current_json = json.loads(current_user.answers_to_questions)

        current_json[current_user.conversation_question_no] = current_answer

        current_user.answers_to_questions = json.dumps(current_json)

        current_user.save()

def updateAnswersJson2(current_user, current_answer,current_que):
            current_json = json.loads(current_user.answers_to_questions)

            current_json[current_que] = current_answer

            current_user.answers_to_questions = json.dumps(current_json)

            current_user.save()

def updateAnswersOfMUltiOptionJson(current_user,current_answer):

            print("current answer:{}".format(current_answer))

            current_json = json.loads(current_user.answers_to_questions)

            current_json[current_user.conversation_question_no] = current_answer.answer_id

            current_user.answers_to_questions = json.dumps(current_json)

            current_user.save()





def sendVideoIfExists(user,set):
    videos = SetVideos.objects.filter(question_set=set).all()

    if len(videos) > 0:
        params = {
            "access_token": "EAATU4NXzrDgBAFCUZCk4OGjQ3MecGLhE0hA0MnZAhYyzkyu9NIygJhGKZBunS8RGnhg3cI4cgDRIBW0AJFFZB5QDqmgRD8w9P26Y3dJ4zibS5GiiA41YTH3ZCZB6azECzAQiLlwXbYLiXBlBOJFnMZBBtwFeGPjEVcRZC5jij33AiwZDZD"
        }
        headers = {
            "Content-Type": "application/json"
        }
        data = json.dumps({
            "recipient": {
                "id": user
            },
            "message": {
                "attachment": {
                    "type": "template",
                    "payload": {
                        "template_type": "open_graph",
                        "elements": [
                            {
                                "url": videos[0].video_link,
                                "buttons": [
                                    {
                                        "type": "web_url",
                                        "url":  videos[0].video_link,
                                        "title": "Bilgi videosu"
                                    }
                                ]
                            }
                        ]
                    }
                }
            }
        })
        r = requests.post("https://graph.facebook.com/v2.6/me/messages", params=params, headers=headers, data=data)
        if r.status_code != 200:
            print(r.text)



def sendDoctorIfExistText(user,set):
    doctors = SymptomDoctor.objects.filter(symptom=set).all()
    if len(doctors) > 0:
        sendMessage(doctors[0].thatdoc.doctor_subtext,user)
        sendMessage(doctors[0].thatdoc.doctor_tel,user)

def sendDoctorsIfExist(user,set):

    doctors = SymptomDoctor.objects.filter(symptom=set).all()
    if len(doctors)>0:


        params = {
            "access_token": "EAATU4NXzrDgBAFCUZCk4OGjQ3MecGLhE0hA0MnZAhYyzkyu9NIygJhGKZBunS8RGnhg3cI4cgDRIBW0AJFFZB5QDqmgRD8w9P26Y3dJ4zibS5GiiA41YTH3ZCZB6azECzAQiLlwXbYLiXBlBOJFnMZBBtwFeGPjEVcRZC5jij33AiwZDZD"
        }
        headers = {
            "Content-Type": "application/json"
        }
        data = json.dumps({
            "recipient": {
                "id": user
            },
        "message": {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": [
                        {
                            "title": "Bilgi al",
                            "image_url": doctors[0].thatdoc.doctor_image_path,
                            "subtitle": doctors[0].thatdoc.doctor_subtext,
                            "buttons": [
                                {
                                    "type": "web_url",
                                    "url": doctors[0].thatdoc.doctor_website,
                                    "title": "Bilgi al"
                                }, {
                                    "type": "phone_number",
                                    "title": "Hemen ara, desteği al",
                                    "payload": doctors[0].thatdoc.doctor_tel
                                }
                            ]
                        }
                    ]
                }
            }
        }
        })
        r = requests.post("https://graph.facebook.com/v2.6/me/messages", params=params, headers=headers, data=data)
        if r.status_code != 200:
            print(r.text)

def allQuestionSetsAreFinished(current_user):

        last_set = current_user.conversation_question_set

        sendVideoIfExists(current_user.conversation_user,last_set)
        #sendDoctorIfExistText(current_user.conversation_user,last_set)

        current_user.conversation_question_set = None
        current_user.conversation_question_no = 0
        current_user.answers_to_questions = "{}"

        back_set = {}
        back_set["question_sets"] = []
        back_set["all_question_sets"] = []

        json_content_of_questions = json.dumps(back_set)

        current_user.conversation_current_status = json_content_of_questions

        current_user.passed_questions.clear()

        current_user.save()

        current_last_set = current_user.last_set_entry

        if current_last_set != None and current_last_set.question_set_whatdont != ".":
            sendQuickReplyRaw("Ne yapıldığı makalesi hakkında bilgi alabilirsiniz",current_user.conversation_user,["Lenia, ne iyi gelir🌱"])
            if current_last_set.question_set_body_part == "diş":
                pass#sendAppointmentDateTemplate(current_user.conversation_user)
                
                sendNeGelir(current_last_set.question_set_id,current_user.conversation_user)



def prepareSentenceForUnderstanding(current_user):
    #current_conversation_status = json.loads(current_user.conversation_question_set)
    current_set =  current_user.conversation_question_set
    return "" + current_set.question_set_name +  " şikayetiniz için üzüldüm"



def checkQuestionIfItCanBePassable(current_user,question):
    print(question)
    my_pass_que = current_user.passed_questions.all()
    for element in my_pass_que:
        if element.que == question.que:
            try:
                return question.answers.get(answer_content=element.answer.answer_content)
            except:
                return question.answers.all()[0]
    return False

def activateNewQuestionSet(current_user):
    # question set listemde baska soru seti idsi var mı

        current_json = json.loads(current_user.conversation_current_status)

        current_user.conversation_question_set = SymptomQuestionSet.objects.get(
            pk=current_json["question_sets"][0])

        del current_json["question_sets"][0]

        new_arr = current_json["question_sets"]
        new_arr2 = current_json["all_question_sets"]
        back_set = {}
        back_set["question_sets"] = new_arr
        back_set["all_question_sets"] = new_arr2

        current_user.conversation_current_status = json.dumps(back_set)

        if  SymptomQuestions.objects.filter(
            question_set=current_user.conversation_question_set).count() > 0:

            current_symptom_question = SymptomQuestions.objects.filter(
                question_set=current_user.conversation_question_set).first()

            passable_or_not_que = checkQuestionIfItCanBePassable(current_user,current_symptom_question)

            if  passable_or_not_que != False and passable_or_not_que.selected_que == None:
                #FacebookView.allQuestionSetsAreFinished(current_user)
                #zz = FacebookView()
                #21 degil digerinin cevabini ver
                updateAnswersJson2(current_user, passable_or_not_que.answer_id,passable_or_not_que.question_belongs.question_id)
                return "emptyset"
            elif type(passable_or_not_que) != bool and passable_or_not_que.selected_que != None:

                current_symptom_question = passable_or_not_que.selected_que

                current_user.conversation_question_no = passable_or_not_que.selected_que.question_id

                current_user.save()

                answers = SymptomAnswerKit.objects.filter(
                    question_belongs__question_id=current_symptom_question.question_id).order_by('answer_content').all()

                infos = current_symptom_question.question_informations.all()

                for info in infos:
                    sendMessage(info.value,current_user.current_user.conversation_user)

                sendQuickReply(
                    current_symptom_question.question_content,
                    current_user.conversation_user, answers)

                return



            current_user.conversation_question_no = current_symptom_question.question_id

            current_user.save()

            answers = SymptomAnswerKit.objects.filter(
                question_belongs__question_id=current_symptom_question.question_id).order_by('answer_content').all()

            infos = current_symptom_question.question_informations.all()

            for info in infos:
                sendMessage(info.value, current_user.current_user.conversation_user)

            sendQuickReply(
                current_symptom_question.question_content,
                current_user.conversation_user, answers)
        else:
            return "emptyset"


def applyPassableQuestionAdressing(that_que,sender_id_int,current_user,current_json):
    if that_que != None:

        current_user.conversation_question_no = that_que.question_id

        current_user.save()

        answers = SymptomAnswerKit.objects.filter(
            question_belongs__question_id=that_que.question_id).order_by('answer_content').all()

        print(answers)

        if len(answers) > 0:
            sendQuickReply(that_que.question_content,
                                             sender_id_int,
                                             answers)



    elif len(current_json["question_sets"]) < 1:
        allQuestionSetsAreFinished(current_user)
        sendApprovalMessage(
            "Kaydettim sagol 1.",
            sender_id_int)

        # facebookfunctions.sendResultPage("Sonuçlar için tıkla", sender_id_int, "ll")
        sendRealResultPage("", sender_id_int, 1)

    elif that_que == None:
        activateNewQuestionSet(current_user)
    else:
        #print("else icindeyim istedigim")

        current_user.conversation_question_no = that_que.question_id

        current_user.save()

        answers = SymptomAnswerKit.objects.filter(
            question_belongs__question_id=that_que.question_id).order_by('answer_content').all()

        print(answers)

        if len(answers) > 0:

            infos = that_que.question_informations.all()
            print("infos:{}".format(infos))

            for info in infos:
                sendMessage(
                    info.symptomquestioninfo_value, sender_id_int)

            sendQuickReply(that_que.question_content,
                                             sender_id_int,
                                             answers)



def sendNewsOnRequest(sender_id_int):
    my_news = AvicennaNews.objects.filter(
        news_category=AvicennaNewsCategory.objects.get(category_name="Yazar")).order_by(
        '?').all()

    first_new = my_news[0]

    second_new = my_news[1]
    third_new = my_news[2]

    """
    sendNews(sender_id_int, first_new.news_summary, first_new.news_id,
                               first_new.news_title, first_new.news_image_url)

    sendNews(sender_id_int, second_new.news_summary, second_new.news_id,
                               second_new.news_title, second_new.news_image_url)
    """

    sendMyNews(first_new,second_new,third_new,sender_id_int)


def validateIfNumberBetweenOneAndHundrer(no):
    try:
        val = int(no)
        return val >= 1 and val <= 100
    except:
        return False


def sendExtraSetInfos(current_user):
    current_json = json.loads(current_user.conversation_current_status)
    for set in current_json["all_question_sets"]:
        if set != 107:
            extra_info_sets = SetExtraInfo.objects.filter(symptom_set__question_set_id=set).all()
            if len(extra_info_sets)>0:
                sendMessage(extra_info_sets[0].extra_info_content,current_user.conversation_user)



def answerAffectTypeOfQuestion(current_sentence,sender_id_int,current_user,current_que):
    print("buradayim geldim")

    if validateIfNumberBetweenOneAndHundrer(current_sentence):

        # fix bir cevap vermis fazediyoruz
        temp_ans = current_que.answers.all()[0]

        # eger kayit sorusu son soru ise
        current_json = json.loads(current_user.conversation_current_status)
        if temp_ans.selected_que == None and len(current_json["question_sets"]) < 1:

            allQuestionSetsAreFinished(current_user)
            sendMessage("Kaydettim. Sağol", sender_id_int)
            sendRealResultPage("", sender_id_int, 1)
            return

        elif temp_ans.selected_que == None:
            activateNewQuestionSet(current_user)
            return

        """
        verilen cevabı sisteme kaydediyoruz.
        """

        updateAnswersJson(current_user, current_sentence)

        current_user.conversation_question_no = temp_ans.selected_que_id

        current_user.save()

        current_new_que = SymptomQuestions.objects.get(question_id=current_user.conversation_question_no)

        current_new_que_answers = SymptomAnswerKit.objects.filter(question_belongs=current_new_que).all()

        if len(current_new_que_answers) > 0:


            infos = current_new_que.question_informations.all()
            print("infos:{}".format(infos))

            for info in infos:
                sendMessage(info.symptomquestioninfo_value, sender_id_int)

            sendQuickReply(current_new_que.question_content, sender_id_int, current_new_que_answers)


        else:
            allQuestionSetsAreFinished(current_user)

    else:
        sendMessage("Lütfen belirtilen aralıkta sayı girin",sender_id_int)

def recordFirstSetAsStartPoint(res_json,current_user,mode):
    organ_keyset = list(res_json.keys())
    organ_keyset.remove("genel")

    temp_organ_set = organ_keyset

    for affect in res_json[temp_organ_set[0]]:
        # print(res_json[res])



        if SymptomQuestionSet.objects.filter(question_set_body_part=temp_organ_set[0]
                , question_set_body_affect=affect).count() > 0:
            question_set = SymptomQuestionSet.objects.get(question_set_body_part=temp_organ_set[0]
                                                          , question_set_body_affect=affect)
        else:
            try:
                question_set = SymptomQuestionSet.objects.get(question_set_body_part=temp_organ_set[0]
                                                              , question_set_body_affect=affect[0:len(affect) - 1])
            except:


                my_user_complaint = FacebookNonExistingSetComplaint()
                my_user_complaint.complaint_user = current_user
                my_user_complaint.complaint_text = temp_organ_set[0] + " " + affect


                sendMessage(temp_organ_set[0] + " " + affect + "ınız için sistemimizde veri yok. Şikayetinizi cevaplayamadığımız için çok üzgünüz. ",
                                              current_user.conversation_user)
                if len(organ_keyset) < 2:
                    return "noset"
                pass

def createTempAppointmentOrGetIt(userid):
    appointment = FacebookAppointment()
    appointment.facebook_id = FacebookUserModel.objects.get(conversation_user=int(userid))
    appointment.save()
    return appointment

def getInformationOfUser2(userid, page_location="en"):
        current_token = acess_token

        if page_location != "tr":
            current_token = acess_token_en
        try:
            my_source = requests.get(first_prefix_for_user_info + userid + "/?access_token=" + current_token)
            return json.loads(my_source.content.decode("utf-8"))
        except Exception as e:
            sendMessage(str(e), userid, "en")
            return {"first_name": "unk", "last_name": "unk", "profile_pic": "unk"}


def getInformationOfUser(userid,page_location="tr"):
        current_token = acess_token
    
        if page_location != "tr":
            current_token = acess_token_en
        try:
            my_source = requests.get(first_prefix_for_user_info + userid + "/?access_token=" + current_token)
            return json.loads(my_source.content.decode("utf-8"))
        except Exception as e:
            sendMessage(str(e),userid,"en")
            return {"first_name":"unk","last_name":"unk","profile_pic":"unk"}



def sendHowAreYou(current_user):

        question_set = SymptomQuestionSet.objects.get(pk=6)

        current_user.conversation_question_set = question_set

        ques = SymptomQuestions.objects.filter(question_set=current_user.conversation_question_set).first()

        current_user.conversation_question_no = ques.question_id

        current_user.save()

        symptom_questions_current = SymptomQuestions.objects.get(question_id=current_user.conversation_question_no)

        sendQuickReply(symptom_questions_current.question_content, current_user.conversation_user,
                                         symptom_questions_current.answers.all())



def checkDate(answers,sentence,lang="tr"):
    sentence_date = None
    if lang == "tr":
        sentence_date = nlp_helper.extractDateFromSentence(sentence)
    else:
        sentence_date = nlp_helper.extractDateFromSentenceEn(sentence)

    if sentence_date == None:
        return  None

    match_answer_txt  = ""
    answers_ordered = answers.order_by("int_value")
    for answer in answers_ordered:
        if sentence_date > answer.int_value:
            if lang == "tr":
                match_answer_txt = answer.answer_content
            else:
                match_answer_txt = answer.answer_content_en

    return match_answer_txt




def checkAmountAnswer(answers,sentence):
    sentence_date = nlp_helper.extractNumber(sentence)
    print("sentence date:{}".format(sentence_date))

    match_answer_txt = ""
    for answer in answers:
        if sentence_date > answer.int_value:
            match_answer_txt = answer.answer_content

    return match_answer_txt



def checkIfGenelChitQuestion(current_user,sentence):
    query_output = SampleText.objects.filter(sample_text__icontains=sentence).all()
    if len(query_output)>0:
        sendMessage(query_output[0].simplecontexttype.simple_context_match_text,current_user.conversation_user)
        return True
    return False


def sendProductInfo(to,title,explanation,link,image_link):
    params = {
        "access_token": "EAATU4NXzrDgBAFCUZCk4OGjQ3MecGLhE0hA0MnZAhYyzkyu9NIygJhGKZBunS8RGnhg3cI4cgDRIBW0AJFFZB5QDqmgRD8w9P26Y3dJ4zibS5GiiA41YTH3ZCZB6azECzAQiLlwXbYLiXBlBOJFnMZBBtwFeGPjEVcRZC5jij33AiwZDZD"
    }
    headers = {
        "Content-Type": "application/json"
    }
    data = json.dumps({
        "recipient": {
            "id": to
        },
        "message": {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": [
                        {
                            "title": title,
                            "image_url": image_link,
                            "subtitle": explanation,
                            "default_action": {
                                "type": "web_url",
                                "url": link,
                                "messenger_extensions": "True",
                                "webview_height_ratio": "tall",
                                "fallback_url":link
                            },
                            "buttons": [
                                {
                                    "type": "postback",
                                    "payload": "Onemsiz",
                                    "title": "En yakın eczane"
                                },
                            ]
                        }
                    ]
                }
            }
        }
    })
    r = requests.post("https://graph.facebook.com/v2.6/me/messages", params=params, headers=headers, data=data)
    if r.status_code != 200:
        print(r.text)


def endSetsNotResponseTaken():
        time_threshold = datetime.now() - timedelta(days=2)

        current_user_list = FacebookUserModel.objects.filter(last_entry_date__lt=time_threshold).order_by('?').all()

        for current_user in current_user_list:
            allQuestionSetsAreFinished(current_user)

def endableUser(current_user,txt):
    if txt not in ["Evet","Hayır"] and "tedavi" not in current_user.conversation_question_set.question_set_name:
        time_threshold = datetime.datetime.now(pytz.timezone('Europe/Istanbul'))
        user_date = current_user.last_entry_date
        local_dt = timezone.localtime(user_date, pytz.timezone('Europe/Istanbul'))
        #sendMessage(str(local_dt.day) + " " + str(time_threshold.day),current_user.conversation_user)  
        if time_threshold.day != local_dt.day and time_threshold.hour != 0:  
            allQuestionSetsAreFinished(current_user)
            return True

    
    return False