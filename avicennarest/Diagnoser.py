from avicennarest.models import AvicennaUser,SymptomQuestionSet,SymptomQuestions,SymptomAnswerKit,Diseases,AnswerScoreForDiagnosis
from avicennarest.facebookmodels import FacebookUserModel
import json


def extractCandidates(dict):
    pass


def diagnoseUserBackend(answersDict):
    candidates_list = {}
    passed_keys_list = []
    for key in answersDict:
        try:
            that_answer = SymptomAnswerKit.objects.get(pk=answersDict[key])
            that_que_of_answer = that_answer.question_belongs
            if that_que_of_answer.question_choices == 'P':

                temp_list = Diseases.objects.filter(answerscorefordiagnosis__answer_of_it=that_answer)
                for disease in temp_list:
                    if disease not in candidates_list:
                        candidates_list[disease] = 1
                    else:
                        candidates_list[disease] = candidates_list[disease] + 1
                    passed_keys_list.append(key)
        except:
            pass
    for second_time_key in answersDict:
        print()
        if second_time_key not in passed_keys_list:
            try:
                that_answer = SymptomAnswerKit.objects.get(pk=answersDict[second_time_key])
                temp_list = Diseases.objects.filter(answerscorefordiagnosis__answer_of_it=that_answer)
                for disease in temp_list:
                    if disease in candidates_list:
                        candidates_list[disease] = candidates_list[disease] + 1

            except:
                pass



                # print("that answer:{}".format(that_answer))
                # print("candidate list:{}".format(candidates_list))
    return candidates_list

def diagnoseUser(user,answers_dict):
    candidates_list = {}
    passed_keys_list = []
    if isinstance(user,FacebookUserModel):
        print(user.conversation_last_sentence)
        print("Diagnosis is:{}".format(user.answers_to_questions))
        my_answers_dict = json.loads(user.answers_to_questions)
        for key in my_answers_dict:
            try:
                that_answer = SymptomAnswerKit.objects.get(pk=my_answers_dict[key])
                that_que_of_answer = that_answer.question_belongs
                if that_que_of_answer.question_choices == 'P':

                    temp_list = Diseases.objects.filter(answerscorefordiagnosis__answer_of_it=that_answer)
                    for disease in temp_list:
                        if disease not in candidates_list:
                            candidates_list[disease] = 1
                        else:
                            candidates_list[disease] = candidates_list[disease] + 1
                        passed_keys_list.append(key)
            except:
                pass
        for second_time_key in my_answers_dict:
            print()
            if second_time_key not in passed_keys_list:
                try:
                    that_answer = SymptomAnswerKit.objects.get(pk=my_answers_dict[second_time_key])


                    temp_list = Diseases.objects.filter(answerscorefordiagnosis__answer_of_it=that_answer)
                    for disease in temp_list:
                        if disease in candidates_list:
                            answer_score_point = AnswerScoreForDiagnosis.objects.get(disease_id_of_it=disease.disease_id,answer_of_it=that_answer)
                            candidates_list[disease] = candidates_list[disease] + answer_score_point.point

                except:
                    pass



        #print("that answer:{}".format(that_answer))
        #print("candidate list:{}".format(candidates_list))
        for key in list(candidates_list.keys()):
            if candidates_list[key] < 0:
                del candidates_list[key]
        return candidates_list

