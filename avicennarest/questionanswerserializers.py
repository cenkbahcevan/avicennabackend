from rest_framework import serializers
from .models import *
from avicennarest.facebookmodels import FacebookNumbersCollected,FacebookUserModel

class AvicennaMedicationsSerializer(serializers.ModelSerializer):

    class Meta:
        model = AvicennaMedications
        fields = ('avm_id','avm_name','avm_why','avm_when','avm_side',)



class SymptomQuestionSetSerializer(serializers.ModelSerializer):

    class Meta:

        depth = 2
        model = SymptomQuestionSet
        fields = ('question_set_id',SymptomQuestions)


class SymptomQuestionSerializer(serializers.ModelSerializer):

    #question = SymptomQuestions()


    class Meta:
        depth=1
        model = SymptomQuestions
        fields = ('question_content','question_id','answers','question_choices','question_organ','question_affect','que','question_informations')



class SymptomAnswerKitSerializer(serializers.ModelSerializer):



    class Meta:
        model = SymptomAnswerKit
        fields = ('question_belongs','answer_content','activate',)
        depth=1


class GeneralSicknessSerializer(serializers.ModelSerializer):

    class Meta:
        model = GeneralSickness
        fields = ('sickness_id','sickness_name',)


class AvicennaUserComplaintSerializer(serializers.ModelSerializer):


    class Meta:
        model = AvicennaUserComplaints
        fields = ('complaint_type','complaints_time',)
        depth = 1

class HealthWikiPageSerializer(serializers.ModelSerializer):

    class Meta:
        model = HealthWikiPage
        fields = ('page_id','page_title','page_summary',)


class AvicennaNewSerializer(serializers.ModelSerializer):

    class Meta:
        model = AvicennaNews
        depth = 2
        fields = ('news_title','news_summary','news_link','news_source','news_category','news_image_url',)


class AvicennaCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = AvicennaNewsCategory
        fields = ('category_id','category_name',)



class SymptomQuestionInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = SymptomQuestionInfo
        fields = ('symptomquestioninfo_id','symptomquestioninfo_type','symptomquestioninfo_value')


class FacebookUserModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = FacebookUserModel
        fields = ('name','conversation_user',)


class FacebookNumbersCollectedSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='number_user.name', read_only=True)
    last_set = serializers.CharField(source='number_user.last_set_entry.question_set_name')
    number_date = serializers.DateTimeField(read_only=True, format="%d.%m.%Y")

    class Meta:
        model = FacebookNumbersCollected
        fields = ('name','number_text','number_adress','number_date','last_set',)

