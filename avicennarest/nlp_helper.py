from avicennarest.models import HealthWikiPage,AvicennaMedications
import interaction
from django.db.models import Q
from prespellchecker import prespellchecker
from prespellchecker import sexualmatcher
import re
import json
import requests



section_list = ["nedir","semptom","tedavi"]

section_list = sorted(section_list)

section_list_dict = {"nedir":["nedir","ne","anlam"],
                    "semptom":["belirti","bulgu","semptom"], 
                     "tedavi":["tedavi","düzel"]
}


section_list_dict = {k: v for k, v in sorted(section_list_dict.items(), key=lambda item: item[0])}

medicine_section_list_dict = {
    "etki":{"etki"},
    "ne":{"ne"}
}

terms_dict_en = {"week": 7, "day": 1, "year": 365,"days":1,"weeks":7,"years":365,"hour":1/24,"hours":1/24}

numbers_in_key_en = {"one": 1, "two": 2, "three": 3, "four": 4, "five": 5, "six": 6, "seven": 7, "eight": 8, "nine": 9}

inc_dec_words_en = {"more": "+", "less": "-"}

days_en = ["monday","tuesday","wednesday","thursday","friday","saturday","sunday"]



def searchSectionList(word):
    i = 0
    for section in section_list:
            if word in section_list_dict[section] or word.lower() in section_list_dict[section]:
                return i
            i = i + 1
    return -1




def checkAnswerSynonym(answer_text):
    try:
        r = requests.post("http://127.0.0.1:5000/similarity",json={"answer":answer_text})
        return r.json()["result"]
    except:
        return "hata"


def checkFixedAnswerSexuality(question):
    result = sexualmatcher.cinsel_similarity(question) 
    if result == "-1":
        return "hata"
    return result


def checkFixedMatcher(question):
    try:
        r = requests.post("http://127.0.0.1:5000/mainmatcher",json={"sentence":question})
        return r.json()["result"]
    except:
        return "hata"



def checkEnglishNLPResult(sentence):
    try:
        r = requests.post("http://127.0.0.1:5000/nlp",json={"sentence":sentence})
        return r.json()["result"]
    except:
        return {"single":"bos"}

def extractDiseasesFromSentenceOnline(sentence):
    """
    try:
        r = requests.post("http://13.90.103.28/turkishner",json={"sentence":sentence})
        return r.json()["result"]
    except:
    """
    return "hata"


def getRawNLPDNER(text):
    diseases_list = extractDiseasesFromSentenceOnline(text[0].lower() + text[1:])
    return str(diseases_list)

def compareLengthAndResult(diseases_list):
    pass

def getTrueHealthPage(text):

    text = text.replace("(","")
    text = text.replace(")", "")

    if len(text)<2:
        return ([], set())

    text_stemmed  = prespellchecker.stemmer.stemSentence(text)
    if len(text_stemmed) <= 1:
        return ([],set())

    print("text stemmed:{}".format(text_stemmed))
    current_sections = set()

    print(text)
    possible_candidates = []
    words_splitted = []
    if text_stemmed[len(text_stemmed)-1] == " ":
        words_splitted = text_stemmed.split(" ")[:-1]
    else:
        words_splitted = text_stemmed.split(" ")

    print("words splitted:{}".format(words_splitted))

    for sec in range(0,len(words_splitted),2):

        word = " ".join(words_splitted[sec:sec+2])
        #print("toplu :{} {}".format(word,len(word)))

        if "var" not in  words_splitted:

            current_word_result = searchSectionList(word)
            if current_word_result != -1:
                current_sections.add(current_word_result)
            my_candidates = HealthWikiPage.objects.filter(page_title__istartswith=word, visible_to_search=True).all()
            if len(my_candidates) > 0:
                possible_candidates.append(my_candidates[0])
        
        for word in words_splitted:
            if len(word)>2 and word not in  ["ne","var"] and word != "var":
                current_word_result = searchSectionList(word)
                if current_word_result != -1:
                        current_sections.add(current_word_result)
                my_candidates = HealthWikiPage.objects.filter(Q(page_title__istartswith=word, visible_to_search=True)).all()
                if len(my_candidates) > 0 and len(possible_candidates) < 1:
                    possible_candidates.append(my_candidates[0])

                #my_drug_candidates = AvicennaMedications.objects.filter(avm_name__iexact=word).all()
                #if len(my_drug_candidates) > 0:
                #    possible_candidates.append(my_drug_candidates[0])

    print("my candidates:{} current section:{}".format(possible_candidates,current_sections))
    return (possible_candidates,current_sections)




days = ["pazartesi", "salı", "çarşamba", "perşembe", "cuma", "cumartesi", "paza"]

terms_dict = {"hafta": 7, "gün": 1, "yıl": 365}
numbers_in_key = {"bir": 1, "iki": 2, "üç": 3, "dört": 4, "beş": 5, "altı": 6, "yedi": 7, "sekiz": 8, "dokuz": 9}

inc_dec_words = {"fazla": "+", "az": "-"}



keys_collection_en = list(terms_dict_en.keys()) + list(numbers_in_key_en.keys()) + days_en

def extractDateFromSentence(sentence):
    day_count = 0

    stemmed_sentence = prespellchecker.stemmer.stemSentence(sentence)
    words_tokenized = prespellchecker.stemmer.white_space_tokenize(stemmed_sentence)

    term_unit_found = False
    term_unit_str = ""

    date_unit_found = False
    date_unit_no = 0

    cache = []

    for i in range(0, len(words_tokenized)):
        if words_tokenized[i] in terms_dict and date_unit_found == False:
            term_unit_found = True
            term_unit_str = words_tokenized[i]

        elif words_tokenized[i] in numbers_in_key and term_unit_found == False:
            date_unit_found = True
            date_unit_no = numbers_in_key[words_tokenized[i]]
        elif prespellchecker.checkInt(words_tokenized[i]) and term_unit_found == False:
            date_unit_found = True
            date_unit_no = int(words_tokenized[i])
        elif prespellchecker.checkInt(words_tokenized[i]) and term_unit_found:
            int_ver = prespellchecker.checkInt(words_tokenized[i])
            day_count += terms_dict[term_unit_str] * int_ver
        elif words_tokenized[i] in numbers_in_key and term_unit_found:
            day_count += terms_dict[term_unit_str] * numbers_in_key[words_tokenized[i]]
        elif words_tokenized[i] in terms_dict:
            term_unit_found = True
            term_unit_str = words_tokenized[i]

            day_count += terms_dict[term_unit_str] * date_unit_no
        elif term_unit_found and date_unit_found and words_tokenized[i] in inc_dec_words:
            karsilik = inc_dec_words[words_tokenized[i]]
            if karsilik == "+":
                day_count = day_count + 1
            else:
                day_count = day_count - 1

    return day_count


def checkInt(no):
    try:
        int(no)
        return True
    except:
        return False


def extractDateFromSentenceEn(sentence):

    day_count = 0

    term_exist = False

    words_tokenized = sentence.split(" ")

    for element in words_tokenized:
        if element in keys_collection_en or checkInt(element):
            term_exist = True

    if term_exist == False:
        return None

    term_unit_found = False
    term_unit_str = ""

    date_unit_found = False
    date_unit_no = 0

    cache = []

    for i in range(0, len(words_tokenized)):

        if words_tokenized[i] in terms_dict_en and date_unit_found == False:
            term_unit_found = True
            term_unit_str = words_tokenized[i]

        elif words_tokenized[i] in numbers_in_key_en and term_unit_found == False:
            date_unit_found = True
            date_unit_no = numbers_in_key_en[words_tokenized[i]]
        elif checkInt(words_tokenized[i]) and term_unit_found == False:
            date_unit_found = True
            date_unit_no = int(words_tokenized[i])
        elif checkInt(words_tokenized[i]) and term_unit_found:
            int_ver = checkInt(words_tokenized[i])
            day_count += terms_dict_en[term_unit_str] * int_ver
        elif words_tokenized[i] in numbers_in_key_en and term_unit_found:
            day_count += terms_dict_en[term_unit_str] * numbers_in_key_en[words_tokenized[i]]
        elif words_tokenized[i] in terms_dict_en:
            term_unit_found = True
            term_unit_str = words_tokenized[i]

            day_count += terms_dict_en[term_unit_str] * date_unit_no
        elif term_unit_found and date_unit_found and words_tokenized[i] in inc_dec_words_en:
            karsilik = inc_dec_words_en[words_tokenized[i]]
            if karsilik == "+":
                day_count = day_count + 1
            else:
                day_count = day_count - 1

    return day_count


def extractNumber(sentence):
    # stemmed_sentence = prespellchecker.stemmer.stemSentence(sentence)
    numbers = [int(s) for s in re.findall(r'-?\d+\.?\d*', sentence)]
    print(numbers)
    if len(numbers) > 1:
        if "litre" in sentence:
            return numbers[1] * 4
        else:
            return numbers[1]

    if len(numbers) == 1:
        if "litre" in sentence:
            return numbers[0] * 4
        else:
            return numbers[0]

    return -1
