# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import AvicennaUser,SymptomQuestions,SymptomAnswerKit,SymptomQuestionSet,GeneralSickness,AvicennaUserSickness,Medications,PreAnswers,Token
from .models import AvicennaMedications,AvicennaUserComplaints,AvicennaMedicationUse,OrganInfo,AvicennaUserComplaintSubDetails,AvicennaSentencesEntered,SetExtraInfo,SetVideos,SampleText,SimpleContextText,Doctor,SymptomDoctor,CookieUser,SymptomQuestionImage,GeneralText,FixedAnswers
from .qamodels import HealthQuestion,HealthAnswers
from .facebookmodels import FacebookUserModel,FacebookPassedQuestions,FacebookAppointment,FacebookSymptomComplaint,FacebookNonExistingSetComplaint,FacebookNumbersCollected,BlockList,FacebookCounts
from .models import *
from avicennaweb.models import LeniaAdvices, AutoCompleteAdditional


# Register your models here.
admin.site.register(AvicennaUser)

admin.site.register(AutoCompleteAdditional)

class SymptomQuestionsAdmin(admin.ModelAdmin):
    list_filter = ('question_set',)
    search_fields = ['question_set__question_set_name']


class SymptomAnswerKitAdmin(admin.ModelAdmin):
    list_filter = ('question_belongs',)


class FacebookSymptomComplaintAdmin(admin.ModelAdmin):
    list_filter = ('complaint_user',)



admin.site.register(FacebookSymptomComplaint,FacebookSymptomComplaintAdmin)



admin.site.register(SymptomQuestions,SymptomQuestionsAdmin)

admin.site.register(SymptomAnswerKit,SymptomAnswerKitAdmin)



admin.site.register(SymptomQuestionSet)

admin.site.register(GeneralSickness)

admin.site.register(AvicennaUserSickness)

admin.site.register(HealthQuestion)



class MyMedicationsAdmin(admin.ModelAdmin):
    def queryset(self, request):
        qs = super(MyMedicationsAdmin, self).queryset(request)
        return qs.filter(user=User.objects.filter(pk=1))


admin.site.register(Medications,MyMedicationsAdmin)




class FacebookUserModelAdmin(admin.ModelAdmin):
    list_filter = ('name','page_location','conversation_user',)

admin.site.register(FacebookUserModel,FacebookUserModelAdmin)

admin.site.register(AvicennaMedications)

admin.site.register(HealthAnswers)


admin.site.register(AvicennaUserComplaints)

admin.site.register(AvicennaMedicationUse)

admin.site.register(OrganInfo)

admin.site.register(AvicennaUserComplaintSubDetails)


admin.site.register(HealthWikiCategory)

class HealthWikiPageAdmin(admin.ModelAdmin):
    ordering = ["page_title"]

admin.site.register(HealthWikiPage,HealthWikiPageAdmin)

"""
class DiseasePageAdmin(admin.ModelAdmin):
    ordering = ["disease_name"]
"""

admin.site.register(Diseases)

admin.site.register(SymptomForDiseases)
admin.site.register(AvicennaNews)

admin.site.site_header = 'Avicenna Yönetim Paneli'
admin.site.site_title = "Avicenna Yönetim Paneli"

admin.site.register(AvicennaNewsCategory)
admin.site.register(AnswerScoreForDiagnosis)

admin.site.register(Question)

admin.site.register(FacebookPassedQuestions)

admin.site.register(SymptomQuestionInfo)

admin.site.register(PriceTagForDepartment)

admin.site.register(FacebookAppointment)

admin.site.register(AvicennaSentencesEntered)

admin.site.register(ExtraInfoPage)


admin.site.register(FacebookNonExistingSetComplaint)

admin.site.register(FacebookNumbersCollected)

admin.site.register(SetExtraInfo)

admin.site.register(PreAnswers)

admin.site.register(SetVideos)

admin.site.register(SampleText)

admin.site.register(SimpleContextText)

admin.site.register(Doctor)

admin.site.register(SymptomDoctor)


admin.site.register(CookieUser)

admin.site.register(SymptomQuestionImage)

admin.site.register(GeneralText)

admin.site.register(BlockList)

admin.site.register(FacebookCounts)

admin.site.register(TranslationText)

admin.site.register(FixedAnswers)

admin.site.register(LeniaAdvices)

admin.site.register(Token)

admin.site.register(LeniaProduct)

admin.site.register(NotificationLastText)