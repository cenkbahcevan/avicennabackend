from django.db import models
from .models import  SymptomQuestionSet,Question,HealthWikiPage,SymptomQuestions,SymptomAnswerKit
from jsonfield import JSONField
from django.core.exceptions import ValidationError
from datetime import datetime
import json


def validate_day(value):
    if value<1 and value > 31 :
        raise ValidationError('%s unvalid day' % value)


def validate_month(value):
    if value < 1 and value > 12:
        raise ValidationError('%s unvalid month' % value)



class FacebookUserModel(models.Model):
    name = models.TextField(null=False,default="")
    profile_pic = models.TextField(null=False,default="")
    conversation_user = models.BigIntegerField(blank=False)
    conversation_question_set = models.ForeignKey('SymptomQuestionSet',null=True,blank=True)
    conversation_question_no =  models.IntegerField()
    conversation_current_status = JSONField(default="{\"question_sets\":[],\"all_question_sets\":[]}",blank=False,null=False)
    answers_to_questions = JSONField(default="{}",blank=True,null=False)
    conversation_last_sentence = models.TextField(null=True,default="",blank=True)
    conversation_last_sentence_text = models.TextField(null=False,default="",blank=True)
    passed_questions = models.ManyToManyField('FacebookPassedQuestions',blank=True)
    dontunderstand_no = models.IntegerField(default=0)
    last_disease_entered = models.ForeignKey('HealthWikiPage',null=True,default=None)
    last_disease_entered_day = models.IntegerField(validators=[validate_day],null=True)
    last_disease_entered_month = models.IntegerField(validators=[validate_month],null=True)
    message_no_of_his = models.IntegerField(default=0)
    message_success_of_his = models.IntegerField(default=0)
    message_failure_of_his = models.IntegerField(default=0)
    creation_date = models.DateTimeField(null=True)
    last_entry_date = models.DateTimeField(null=True)
    context_type_options = (
        ('SF', 'SetFinish'),
        ('AD', 'AfterDisease'),
        ('NI','NotImportant'),
    )
    last_context = models.CharField(default="NI",max_length=2,choices=context_type_options)
    user_city = models.CharField(max_length=200,default="Unknown")
    user_telephone = models.CharField(max_length=200,default='')
    last_set_entry = models.ForeignKey(SymptomQuestionSet,default=None,related_name="lastset",null=True)
    location_options = (
        ('tr','Turkish'),
        ('en','English'),
    )
    page_location = models.CharField(default="tr",choices=location_options,max_length=5)


    def __str__(self):
        return str(self.conversation_user) + " " + self.name

    def terminateEverything(self):
        current_status_json_form = json.loads(self.conversation_current_status)

        # current_answers_form = json.loads(current_user.answers_to_questions)
        current_answers_form = {}

        current_status_json_form["question_sets"] = []
        current_status_json_form["all_question_sets"] = []

        self.conversation_current_status = json.dumps(current_status_json_form)
        self.answers_to_questions = json.dumps(current_answers_form)

        self.conversation_question_set = None
        self.conversation_question_no = 0
        self.save()


class FacebookPassedQuestions(models.Model):
    que = models.ForeignKey('Question')
    answer = models.ForeignKey('SymptomAnswerKit',default=None)

    def __str__(self):
        return self.que.que_content + ":" + self.answer.answer_content


class FacebookAppointment(models.Model):
    appointment_id = models.AutoField(primary_key=True)
    facebook_id = models.ForeignKey('FacebookUserModel')
    location_of_hospital = models.CharField(max_length=50,default="",blank=True)
    doctor_name = models.CharField(max_length=150,default="",blank=True)
    appointment_date = models.DateTimeField(null=True,blank=True)

    def __str__(self):
        return self.doctor_name

class FacebookSymptomComplaint(models.Model):
    complaint_id = models.AutoField(primary_key=True)
    complaint_user = models.ForeignKey(FacebookUserModel)
    complaint_symptom = models.ForeignKey(SymptomQuestionSet)

    def __str__(self):
        return self.complaint_user.name + " " + self.complaint_symptom.question_set_full_name


class FacebookNonExistingSetComplaint(models.Model):
    complaint_id = models.AutoField(primary_key=True)
    complaint_user = models.ForeignKey(FacebookUserModel)
    complaint_text = models.CharField(max_length=200,default="")

    def __str__(self):
        return self.complaint_user.name + " " + self.complaint_text


class FacebookNumbersCollected(models.Model):
    collected_id = models.AutoField(primary_key=True)
    number_user = models.ForeignKey(FacebookUserModel)
    number_text = models.CharField(max_length=200,default="")
    number_adress = models.TextField(default="")
    number_date = models.DateTimeField(default=datetime.now())

    def __str__(self):
        if self.number_user.last_set_entry != None:
            return self.number_user.name + "|" + self.number_text + "|" +  self.number_adress + "|" + self.number_user.last_set_entry.question_set_name + "|" + self.number_date.strftime("%Y-%m-%d %H:%M:%S")

        return self.number_user.name + "|" + self.number_text + "|" + self.number_adress


class BlockList(models.Model):
    blocklist_id = models.AutoField(primary_key=True)
    blocklist_user = models.ForeignKey(FacebookUserModel)

    def __str__(self):
        return self.blocklist_user.name

class FacebookCounts(models.Model):
    title = models.CharField(max_length=50)
    value  = models.IntegerField(default=0)

    def __str__(self):
        return self.title