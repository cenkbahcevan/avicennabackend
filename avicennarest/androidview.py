from django.http import HttpResponse, HttpResponseNotFound, JsonResponse
from .facebookmodels import FacebookUserModel,FacebookPassedQuestions,FacebookSymptomComplaint,FacebookNonExistingSetComplaint,FacebookNumbersCollected
from . import facebookfunctions
from .models import SymptomQuestionSet,SymptomQuestions,SymptomAnswerKit,Question,HealthWikiPage,GeneralText, FixedAnswers, SampleText
import json
from . import Diagnoser
from . import nlp_helper
from prespellchecker import prespellchecker
import interaction
from . import slackutils
import copy


def applyGenel(genel_json,sentence):
    possible_text_should_be_sended = ""
    
    if len(genel_json)>0:
        first_element = list(genel_json.keys())[0]
        
        
        if first_element == "tesekkur":
            return "Sizi memnun ettiğim için çok mutlu oldum"
        elif first_element == "selam":
           current_general_text = GeneralText.objects.get(generaltext_title="merhaba").generaltext_turkish_1
           return current_general_text
        elif first_element == "anladim":
            return "Anladım 👍"
        elif first_element == "veda":
            return "Görüşürüz ✋"
        elif first_element == "randevu":
            #facebookfunctions.sendAppointmentDateTemplate(current_user.conversation_user)
           return "Hastanelere veya sağlık kuruluşlarına henüz sistemim üzerinden randevu veremiyorum😥"
        elif first_element == "personal":
            return "Lenia olarak ben otomatik çalışan bir botum. O yüzden telefon ve adresim yok 🤖 \n İstanbul'da geliştiriliyorum."
        elif first_element == "yardim":
            return "Lenia'yı aşağıdaki gibi kullanabilirsiniz 🤖 \n\n Şikayetinizi yazıp, kendinize uygun hastalıklarla ilgili bir okuma listesi alabilirsiniz. Örnek olarak başım ağrıyor\n"

        elif first_element == "cinsel":

            my_key = nlp_helper.checkFixedAnswerSexuality(sentence)
            if str(my_key) != "-1":
                try:
                    my_fixed_ans = FixedAnswers.objects.get(fixedanswer_keyword=my_key)
                    return my_fixed_ans.fixedanswer_answercontent
                except:
                    pass
            else:
                return "Cinsellikle ilgili bir kısım sorularınızı cevaplıyamıyorum😥 Erken boşalma, sertleşme cinsel organınla ilgili şikayetlerini yazarsan cevaplayabilirim."
            #facebookfunctions.sendDoctorIfExistText(current_user.conversation_user,SymptomQuestionSet.objects.get(pk=108))
            
            return "Cinsellikle ilgili bir kısım sorularınızı cevaplıyamıyorum😥 Erken boşalma, sertleşme cinsel organınla ilgili şikayetlerini yazarsan cevaplayabilirim."

        elif first_element == "ilac":
            return "İlaç tavsiyesi veremem veya hakkında soru yanıtlayamam. Çok özür dilerim"
                                         
        elif first_element == "sigara":
            return "Sigrayı bırakma konusunda bilgim yok. Çok özür dilerim"
                                          
        elif first_element == "uykusuzluk":
            return "Uykusuzluk konusunda bilgim yok. Çok özür dilerim"
        elif first_element == "hamile":
           return "Hamilelik konusunda bilgim yok. Çok özür dilerim"
                                        
 
        elif first_element == "kanser":
                return "Kanser ve kanserojen ürünler konusunda bilgim yok. Çok özür dilerim"
        
        elif first_element == "estetik":
                return "Estetik hizmetleri konusunda bilgim yok. Çok özür dilerim"
                                         
        elif first_element == "psikoloji":
            return "Henüz psikoloji ile ilgili soruları cevaplıyamıyorum. Ama vücüdunuzla ilgili şikayetleriniz ve hastalıklar ile ilgili sorularınız hakkında yardımcı olmak isterim. Çok üzgünüm😥"

        elif first_element == "hakaret":
            return "Hakaret etmen beni üzüyor😥"        
        elif first_element == "cild":
                return "Cildin ve dermatoloji hakkında soruları cevaplayamıyorum😥"
        return ""
    return ""


def recordAnswer(user,answer):

    current_answer_form = json.loads(user.answers_to_questions)

    current_answer_form[user.conversation_question_no] = answer.answer_id

    user.answers_to_questions = json.dumps(current_answer_form)

    user.save()



def checkQuestionIfItCanBePassable(current_user,question):
    print(question)
    my_pass_que = current_user.passed_questions.all()
    for element in my_pass_que:
        if element.que == question.que:
            try:
                return question.answers.get(answer_content=element.answer.answer_content)
            except:
                return question.answers.all()[0]
    return False

def terminateEverything(user):

    current_status_json_form = json.loads(user.conversation_current_status)

    current_answers_form = json.loads(user.answers_to_questions)
    current_answers_form = {}


    current_status_json_form["question_sets"] = []
    current_status_json_form["all_question_sets"] = []

    user.conversation_current_status = json.dumps(current_status_json_form)
    user.answers_to_questions = json.dumps(current_answers_form)
    user.passed_questions.remove()
    user.passed_questions.clear()

    user.conversation_question_set = None
    user.conversation_question_no = 0
    user.save()



def makeDiseasesDict(diseases):
                diseases_dict_list = []
                for current_disease in diseases:
                    diseases_dict = {}
                    diseases_dict["Title"] = current_disease.page_title

                    
                    diseases_dict["Link"] = "https://www.lenia.ist/loadWikiPage?page_no=" + str(current_disease.page_id)
                    diseases_dict["Explanation"] = current_disease.page_summary

                    diseases_dict_list.append(diseases_dict)
                return diseases_dict_list


def moveToNextQuestion(user,current_question,answer):

    answer_text = SymptomAnswerKit.objects.filter(question_belongs=current_question, answer_content=answer)


    current_status_json_form = json.loads(user.conversation_current_status)

    if answer_text.count() > 0:

        current_answer = answer_text.first()

        if current_question.que != None:
            current_passed_question = FacebookPassedQuestions()
            current_passed_question.que = current_question.que
            current_passed_question.answer = current_answer
            current_passed_question.save()
            user.passed_questions.add(current_passed_question)
            user.save()

        recordAnswer(user, current_answer)

        """
        Cevaba gore aktivasyon
        """
        activation_list = current_answer.activate.values_list()

        for temp_set in activation_list:
            current_status_json_form["question_sets"] = current_status_json_form["question_sets"] + [temp_set[0]]
            current_status_json_form["all_question_sets"] = current_status_json_form["all_question_sets"] + [
                temp_set[0]]

        user.conversation_current_status = json.dumps(current_status_json_form)
        user.save()

        next_question = current_answer.selected_que

        if next_question == None:

            if len(current_status_json_form["question_sets"]) < 1:

                diagnosis_results = Diagnoser.diagnoseUser(user, "")
                """
                facebookfunctions.sendResults(diagnosis_results, user.conversation_user)

                facebookfunctions.sendQuickReplyRaw("You can take my advice!", user.conversation_user,
                                                    ["Lenia, advices 🌱"])
                """

                
                diseases_dict_list = []
                for current_disease in list(diagnosis_results.keys()):
                    diseases_dict = {}
                    diseases_dict["Title"] = current_disease.disease_name

                    if current_disease.disease_wiki != None:
                        diseases_dict["Link"] = "https://www.lenia.ist/loadWikiPage?page_no=" + str(current_disease.disease_wiki.page_id)
                        diseases_dict["Explanation"] = current_disease.disease_wiki.page_summary
                    else:
                        diseases_dict["Link"] = "noempty"
                        diseases_dict["Explanation"] = "Açıklama yok"

                    diseases_dict_list.append(diseases_dict)

                neiyigelir_link = "https://www.lenia.ist/neiyigelir?sym_no=" + str(user.conversation_question_set.question_set_id)
                terminateEverything(user)
                return {"text":"Set bitti, teşhis gelecek","answers":[],"diseases":diseases_dict_list,"neiyigelir":neiyigelir_link}




            else:

                facebookfunctions.activateNewQuestionSet(user)
                current_question = SymptomQuestions.objects.get(pk=user.conversation_question_no)

                current_answers_formatted = list(current_question.answers.values_list("answer_content",flat=True))
                return {"text":current_question.question_content,"answers":current_answers_formatted}

            return

        is_it_passable = checkQuestionIfItCanBePassable(user, next_question)

        if is_it_passable != False:
            next_question = is_it_passable.selected_que

        if len(next_question.answers.all())>0:
            """
            facebookfunctions.sendQuickReply(next_question.question_content_en, user.conversation_user,
                                             next_question.answers.all())
            """



            

            user.conversation_question_no = next_question.question_id

            user.save()

            current_answers_formatted = list(next_question.answers.values_list("answer_content",flat=True))

            return {"text":next_question.question_content,"answers":current_answers_formatted}
        else:
            #facebookfunctions.sendMessage(next_question.question_content_en,user.conversation_user,"en")
            terminateEverything(user)



def nextQuestion(user,answer):

    
    current_question = SymptomQuestions.objects.get(pk=user.conversation_question_no)

    answer_text = SymptomAnswerKit.objects.filter(question_belongs=current_question,answer_content=answer)


    if answer_text.count() > 0:

        first_ans = answer_text.first()

        return moveToNextQuestion(user,current_question,first_ans.answer_content)



    elif current_question.question_choices == "D":

        possible_answer = facebookfunctions.checkDate(current_question.answers.all(),answer,"en")


        if possible_answer == None or possible_answer == "":


            current_answers_formatted = list(current_question.answers.values_list("answer_content",flat=True))
            return {"text":"Cevabını anlamadım. " + current_question.question_content,"answers":current_answers_formatted}

        else:
            moveToNextQuestion(user, current_question, possible_answer)


    else:
          current_answers_formatted = list(current_question.answers.values_list("answer_content",flat=True))
          return {"text":"Cevabını anlamadım. " + current_question.question_content,"answers":current_answers_formatted}


def prepareSetForJsonReturn(my_user,organ,affect,classifier_result=None,current_sentence=""):


        try:

            question_text =  addTekliSetAndReturnQuestion(my_user,organ,affect)
            
            current_question = SymptomQuestions.objects.get(question_id = my_user.conversation_question_no)

            answers_as_list = list(SymptomAnswerKit.objects.filter(question_belongs=current_question).values_list("answer_content",flat=True))

            return {"text":question_text,"answers":answers_as_list}
        except:
            non_existing_data_sentence = "{} {} için verim yok.".format(organ,affect)

            sets_flat = list(SymptomQuestionSet.objects.filter(question_set_body_part=organ).values_list("question_set_name",flat=True))

            if classifier_result == "cinsel":
                my_key = nlp_helper.checkFixedAnswerSexuality(current_sentence)
                if str(my_key) != "-1":
                    try:
                        my_fixed_ans = FixedAnswers.objects.get(fixedanswer_keyword=my_key)
                        return {"text":my_fixed_ans.fixedanswer_answercontent, "answers":[]}
                    except:
                        return {"text":"Cinsel eşleşme sağlanamadı","answers":[]} 

            if len(sets_flat) > 0:
                return {"text":non_existing_data_sentence,"answers":sets_flat}
            
            else:
                return {"text":non_existing_data_sentence,"answers":[]}



def addTekliSetAndReturnQuestion(user,organ,affect):
       if SymptomQuestionSet.objects.filter(question_set_body_part=organ,question_set_body_affect=affect).count() > 0:
            possible_set =  SymptomQuestionSet.objects.filter(question_set_body_part=organ,question_set_body_affect=affect)
       else:
            possible_set =  SymptomQuestionSet.objects.filter(question_set_body_part=organ,question_set_body_affect=affect[:len(affect)-1])


       slackutils.sendTextToChannel(organ + " " + affect,"#ios")

       if possible_set.count() > 0:

            possible_set = possible_set.first()

            slackutils.sendTextToChannel(possible_set.question_set_name,"#ios")

            user.conversation_question_set = possible_set

            current_question =  SymptomQuestions.objects.filter(question_set=possible_set).first()

            user.conversation_question_no = current_question.question_id

            user.save()


            return  current_question.question_content
       else:
            return "Verim yok"


def addNextSetToTheOrder(user, current_organ,current_affect):
    
    try:

        
        current_set = SymptomQuestionSet.objects.get(question_set_body_part=current_organ,
                                                     question_set_body_affect=current_affect)


        current_status_json_form = json.loads(user.conversation_current_status)

        current_status_json_form["all_question_sets"].append(current_set.question_set_id)
            
        current_status_json_form["question_sets"].append(current_set.question_set_id)

        user.conversation_current_status = json.dumps(current_status_json_form)

        user.save()

    except:
        pass

def returnDiseaseText(possible_diseases,sentence):
                      possible_text = ""

                      words_tedavi_semptom = ["semptom","belirti"]
                      words_semptom_boolean = False


                      for word in words_tedavi_semptom:
                          if word in sentence:
                              words_semptom_boolean = True
                      possible_result = ""
                      
                      if words_semptom_boolean:
                          possible_text = possible_diseases[0][0].page_symtpoms
                      else:
                          possible_text = possible_diseases[0][0].page_prevention
                          
                            
                      return possible_text


def processAndroidUserInput(request):

    my_sentence = ""

    my_data = json.loads(request.body.decode("utf-8"))



    if "sentence" in my_data and "id" in my_data:
        my_sentence = my_data["sentence"]
        android_user_id = my_data["id"]
    else:
        return JsonResponse({"text":"Eksik veri","answers":[]})

    my_user = facebookfunctions.construct_or_get_user(android_user_id,source="en")

    my_user.page_location = "en"
    my_user.save()

    
    if my_sentence == "Lenia, advices 🌱":
        current_advice = my_user.last_set_entry.question_set_whateatdrink
        return JsonResponse({"text":current_advice,"answers":[]})
    elif my_sentence == "Bitir":
        terminateEverything(my_user) 
        return JsonResponse({"text":"Bitti","answers":[]})


    if my_user.conversation_question_set != None:
        return JsonResponse(nextQuestion(my_user,my_sentence))
    else:

        query_output = SampleText.objects.filter(sample_text__icontains=my_sentence).all()

        if query_output.count() > 0:
            return JsonResponse({"text":query_output.first().simplecontexttype.simple_context_match_text,"answers":[]})
            


        current_sentence_fixed = prespellchecker.djangoFixSentence(my_sentence)


        possible_diseases = nlp_helper.getTrueHealthPage(current_sentence_fixed)

        current_sentence_fixed, _ = prespellchecker.stemSentence(current_sentence_fixed)


        res = interaction.symptom_extraction_connection.sendDataGetResponse(current_sentence_fixed)

        res_json = json.loads(res)

        # Dictionary icinde semptomlara bakiyor
        keys_of_res = list(res_json.keys())
        keys_of_res.remove("genel")


        classifier_result = prespellchecker.current_classifier.predict(current_sentence_fixed)


        tekli_keys = list(res_json["tekli"].keys())

            #facebookfunctions.sendMessage(classifier_result,current_user.conversation_user)
                                

                                

        if classifier_result == "adet":
                        del res_json["tekli"]
                        res_json["tekli"] = {classifier_result:{}}

        elif classifier_result in ["estetik","psikoloji","hakaret","sigara","cinsel","kanser","yardim"]:
                        res_json["genel"] = {classifier_result:{}}
        elif len(tekli_keys) > 0 and tekli_keys[0] in ["adet"] and tekli_keys != classifier_result:
                        
                        if classifier_result != "semptom":
                            res_json["tekli"] = {classifier_result:{}}
                        else:
                            res_json["tekli"] = {}



        genel_json = copy.deepcopy(res_json["genel"])
        message_genel = applyGenel(res_json["genel"],current_sentence_fixed)

        slackutils.sendTextToChannel("Classifier result" + classifier_result,"#ios")
        slackutils.sendTextToChannel("Mesaj:" + message_genel,"#ios")
        del res_json["genel"]


        


        
        
        symptom_or_disease_send = False

        


        #sadece tekli ve genel var ise

        if len(keys_of_res) < 2:

            if len(res_json["tekli"])>0:

                first_affect = list(res_json["tekli"].keys())[0]
                symptom_or_disease_send =  True
                return JsonResponse(prepareSetForJsonReturn(my_user,"tekli",first_affect))
            
            else:
                  if len(possible_diseases[0]) > 0:

                      diseases_dict = makeDiseasesDict([possible_diseases[0][0]])
                      
                      possible_text = returnDiseaseText(possible_diseases,my_sentence)

                      return JsonResponse({"text":possible_text,"answers":["Semptom","Tedavi"],"diseases":diseases_dict})

                  elif message_genel != "":
                      return JsonResponse({"text":message_genel,"answers":[]})
                      
                  else:
                    anlamadim_text = "Anlamadım. Tekrarlar mısın? Bana başım ağrıyor gibi şikayetlerinizi söyleyip, bu şikayetlerin veya hastalıklar hakkında bilgi alabilirsiniz ⌛😟"
                    return JsonResponse({"text":anlamadim_text,"answers":[]})



        else:

            initial_set_added_flag = False
            dict_should_be_returned = None
            for current_organ_key in keys_of_res:
                for current_affect_key in res_json[current_organ_key]:

                    if initial_set_added_flag == False:

                        dict_should_be_returned = prepareSetForJsonReturn(my_user,current_organ_key,current_affect_key,classifier_result=classifier_result,current_sentence=current_sentence_fixed)

                        initial_set_added_flag = True

                    else:
                        addNextSetToTheOrder(my_user,current_organ_key,current_affect_key)
            

            symptom_or_disease_send = True
            return JsonResponse(dict_should_be_returned, safe = False)





        if symptom_or_disease_send == False and message_genel != "" :


            return JsonResponse({"text":message_genel,"answers":[]})
        else:
            anlamadim_text = "Anlamadım. Tekrarlar mısın? Bana başım ağrıyor gibi şikayetlerinizi söyleyip, bu şikayetlerin veya hastalıklar hakkında bilgi alabilirsiniz ⌛😟"
            return JsonResponse({"text":anlamadim_text,"answers":[]})


    