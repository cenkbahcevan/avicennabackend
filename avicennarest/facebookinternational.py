from celery.decorators import task
from .facebookmodels import FacebookUserModel,FacebookPassedQuestions,FacebookSymptomComplaint,FacebookNonExistingSetComplaint,FacebookNumbersCollected
from . import facebookfunctions
from .models import SymptomQuestionSet,SymptomQuestions,SymptomAnswerKit,Question,HealthWikiPage
import json
from . import Diagnoser
from . import nlp_helper




synonym_map = {"abdominal pain":"single stomachache"}

def checkQuestionIfItCanBePassable(current_user,question):
    print(question)
    my_pass_que = current_user.passed_questions.all()
    for element in my_pass_que:
        if element.que == question.que:
            try:
                return question.answers.get(answer_content=element.answer.answer_content)
            except:
                return question.answers.all()[0]
    return False



def dontUnderStandMode(user,current_question):

    current_answers = current_question.answers.all()

    if len(current_answers) > 0:
        facebookfunctions.sendQuickReply("I could not understand your response⌛😟  You can choose the answer from options below.", user.conversation_user,current_question.answers.all(),anlamadim=True)
        facebookfunctions.sendQuickReply(current_question.question_content_en, user.conversation_user,current_question.answers.all(),anlamadim=True)
    else:
        facebookfunctions.sendMessage(current_question.question_content_en, user.conversation_user, "en")
        terminateEverything(user)


def addNewSetToUser(user,current_organ,current_affect):
    try:
        current_set = SymptomQuestionSet.objects.get(question_set_body_part_en=current_organ,
                                                     question_set_body_affect_en=current_affect)


        current_status_json_form = json.loads(user.conversation_current_status)

        if user.conversation_question_set == None:

            current_status_json_form["all_question_sets"] = []

            user.conversation_question_set = current_set

            first_question = SymptomQuestions.objects.filter(question_set=current_set).first()

            user.conversation_question_no = first_question.question_id




            current_status_json_form["all_question_sets"].append(current_set.question_set_id)


            #current_status_json_form["all_question_sets"].append(current_set.question_set_id)

            #current_status_json_form["question_sets"].append(current_set.question_set_id)


            user.conversation_current_status = json.dumps(current_status_json_form)

            user.last_set_entry = current_set

            user.save()

            facebookfunctions.sendMessage("I will ask a few questions", user.conversation_user, "en")



            facebookfunctions.sendQuickReply(first_question.question_content_en, user.conversation_user,
                                             first_question.answers.all())

        else:

            current_status_json_form["all_question_sets"].append(current_set.question_set_id)
            
            current_status_json_form["question_sets"].append(current_set.question_set_id)

            user.conversation_current_status = json.dumps(current_status_json_form)

            user.save()



    except:
        facebookfunctions.sendMessage("There is no data for {} {}".format(current_organ,current_affect),user.conversation_user,"en")


def moveToNextQuestion(user,current_question,answer):

    answer_text = SymptomAnswerKit.objects.filter(question_belongs=current_question, answer_content_en=answer)


    current_status_json_form = json.loads(user.conversation_current_status)

    if answer_text.count() > 0:

        current_answer = answer_text.first()

        if current_question.que != None:
            current_passed_question = FacebookPassedQuestions()
            current_passed_question.que = current_question.que
            current_passed_question.answer = current_answer
            current_passed_question.save()
            user.passed_questions.add(current_passed_question)
            user.save()

        recordAnswer(user, current_answer)

        """
        Cevaba gore aktivasyon
        """
        activation_list = current_answer.activate.values_list()

        for temp_set in activation_list:
            current_status_json_form["question_sets"] = current_status_json_form["question_sets"] + [temp_set[0]]
            current_status_json_form["all_question_sets"] = current_status_json_form["all_question_sets"] + [
                temp_set[0]]

        user.conversation_current_status = json.dumps(current_status_json_form)
        user.save()

        next_question = current_answer.selected_que

        if next_question == None:

            if len(current_status_json_form["question_sets"]) < 1:

                diagnosis_results = Diagnoser.diagnoseUser(user, "")
                facebookfunctions.sendResults(diagnosis_results, user.conversation_user)

                facebookfunctions.sendQuickReplyRaw("You can take my advice!", user.conversation_user,
                                                    ["Lenia, advices 🌱"])

                terminateEverything(user)




            else:

                activateNewSet(user)

            return

        is_it_passable = checkQuestionIfItCanBePassable(user, next_question)

        if is_it_passable != False:
            next_question = is_it_passable.selected_que

        if len(next_question.answers.all())>0:

            facebookfunctions.sendQuickReply(next_question.question_content_en, user.conversation_user,
                                             next_question.answers.all())

            user.conversation_question_no = next_question.question_id

            user.save()
        else:
            facebookfunctions.sendMessage(next_question.question_content_en,user.conversation_user,"en")
            terminateEverything(user)



def nextQuestion(user,answer):

    current_question = SymptomQuestions.objects.get(pk=user.conversation_question_no)

    answer_text = SymptomAnswerKit.objects.filter(question_belongs=current_question,answer_content_en=answer)


    if answer_text.count() > 0:

        first_ans = answer_text.first()

        moveToNextQuestion(user,current_question,first_ans.answer_content_en)


    elif current_question.question_choices == "D":

        possible_answer = facebookfunctions.checkDate(current_question.answers.all(),answer,"en")

        #facebookfunctions.sendMessage("possible answer aldim{}".format(possible_answer),user.conversation_user,"en")

        if possible_answer == None or possible_answer == "":

            dontUnderStandMode(user, current_question)

        else:
            moveToNextQuestion(user, current_question, possible_answer)


    else:
            dontUnderStandMode(user,current_question)










def terminateEverything(user):

    current_status_json_form = json.loads(user.conversation_current_status)

    current_answers_form = json.loads(user.answers_to_questions)
    current_answers_form = {}


    current_status_json_form["question_sets"] = []
    current_status_json_form["all_question_sets"] = []

    user.conversation_current_status = json.dumps(current_status_json_form)
    user.answers_to_questions = json.dumps(current_answers_form)
    user.passed_questions.remove()
    user.passed_questions.clear()

    user.conversation_question_set = None
    user.conversation_question_no = 0
    user.save()


def activateNewSet(user):




    current_status_json_form = json.loads(user.conversation_current_status)



    current_set = current_status_json_form["question_sets"][0]

    current_status_json_form["question_sets"].pop(0)


    current_set = SymptomQuestionSet.objects.get(pk=current_set)

    user.conversation_question_set = current_set


    first_question = SymptomQuestions.objects.filter(question_set=current_set).first()

    is_it_passable = checkQuestionIfItCanBePassable(user, first_question)

    user.conversation_question_no = first_question.question_id

    user.conversation_current_status = json.dumps(current_status_json_form)




    #aktivasyon sirasinda varsa bitir
    if is_it_passable != False and is_it_passable.selected_que != None:
        first_question = is_it_passable.selected_que

    elif is_it_passable != False:
        if len(current_status_json_form["question_sets"]) < 1:
            diagnosis_results = Diagnoser.diagnoseUser(user, "")
            facebookfunctions.sendMessage("My questions are over!",user.conversation_user,"en")
            facebookfunctions.sendResults(diagnosis_results, user.conversation_user)

            facebookfunctions.sendQuickReplyRaw("You can take my advice!", user.conversation_user,
                                                ["Lenia, advices 🌱"])

            terminateEverything(user)
            return

        user.save()
        return activateNewSet(user)


    user.conversation_question_no = first_question.question_id

    user.conversation_current_status = json.dumps(current_status_json_form)

    user.save()
    if len(first_question.answers.all())>0:
        facebookfunctions.sendQuickReply(first_question.question_content_en, user.conversation_user,
                                     first_question.answers.all())
    else:
        facebookfunctions.sendMessage(first_question.question_content_en, user.conversation_user,"en")
        terminateEverything(user)






def recordAnswer(user,answer):

    current_answer_form = json.loads(user.answers_to_questions)

    current_answer_form[user.conversation_question_no] = answer.answer_id

    user.answers_to_questions = json.dumps(current_answer_form)

    user.save()



def checkEverythingIsOver(id,answer):
    pass


def applyClassifierSend(label,user):
    if label == "cinsel":
        facebookfunctions.sendMessage("I am unable to answer some of your questions regarding sexuality. If you were to complain about premature ejaculation and erectile dysfunction I would be able to provide answers.",user.conversation_user,"en")
    elif label == "ilac":
        facebookfunctions.sendMessage("It is illegal for me to provide information on specific medicationn",user.conversation_user,"en")
    elif label == "yardim":
        facebookfunctions.sendMessage("You may make use of Lenia in the way listed below🤖\n\n By typing in your complaint you may be able to create a reading list about relatable illnesses. For example, I have a headache.",user.conversation_user,"en")
    elif label == "kilo":
        addNewSetToUser(user,"single","kilo")
    elif label == "muscle":
        facebookfunctions.sendMessage("I cannot help you for bodybuilding",user.conversation_user,"en")
    else:
        return "nothingsend"

def sendDisease(my_disease,my_disease_value,my_user):
    try:
               my_disease_in_wiki = HealthWikiPage.objects.filter(page_title_en__icontains=my_disease).first()
               my_user.last_disease_entered = my_disease_in_wiki
               my_user.save()

               #facebookfunctions.sendMessage("I will send value {}".format(my_disease_value),my_user.conversation_user,"en")

               if my_disease_value == "":
                    facebookfunctions.sendRealResultPage("Detailed information",my_user.conversation_user,my_disease_in_wiki.page_id)
               elif  "symptom" in my_disease_value.lower():
                        facebookfunctions.sendMessage(my_disease_in_wiki.page_symptoms_en,my_user.conversation_user,"en")
               elif my_disease_value == "meaning":
                   facebookfunctions.sendMessage(my_disease_in_wiki.page_summary_en, my_user.conversation_user, "en")
               else:
                        facebookfunctions.sendMessage(my_disease_in_wiki.page_prevention_en, my_user.conversation_user, "en")

               my_answer_list = ["Symptoms", "Treatment"]

               facebookfunctions.sendQuickReplyRaw(my_user.last_disease_entered.page_title_en + " get info",
                                 my_user.conversation_user,
                                 my_answer_list)
    except:
        facebookfunctions.sendMessage("Bulamadim",my_user.conversation_user,"en")


def processEnglishSentence(id,sentence):

    my_user = facebookfunctions.construct_or_get_user(id,source="en")

    my_user.page_location = "en"
    my_user.save()
    

    disease_send = False

    #facebookfunctions.updateNames(id)

    my_user = facebookfunctions.construct_or_get_user(id,source="en")


    facebookfunctions.sendTypingView(id,"en")


    if sentence == "Lenia, advices 🌱":
        current_advice = my_user.last_set_entry.question_set_whateatdrink_en
        facebookfunctions.sendMessage(current_advice,my_user.conversation_user,"en")
        return
    elif sentence == "End conversation":
        terminateEverything(my_user)
        facebookfunctions.sendQuickReplyRaw("You can take my advice!", my_user.conversation_user,
                                            ["Lenia, advices 🌱"])
        return

    elif sentence in ["Meaning", "Symptoms", "Treatment"] and my_user.last_disease_entered != None:
        manipulated_sentence = my_user.last_disease_entered.page_title_en + " " + sentence
        #facebookfunctions.sendMessage(sentence,my_user.conversation_user,"en")
        sendDisease(my_user.last_disease_entered.page_title_en,sentence,my_user)
        return




    if my_user.conversation_question_set != None:

        nextQuestion(my_user,sentence)
        return

    else:

        sentence_modified = sentence.replace("."," ")
        processed_sentence = nlp_helper.checkEnglishNLPResult(sentence_modified.lower())

        that_key = list(processed_sentence["classifier_result"].keys())[0]
        #if_sended = applyClassifierSend(that_key,my_user)


        if_sended = "nothingsend"

        diseases = processed_sentence["diseases"]

        del processed_sentence["diseases"]

        greeting = False


        apply_sent = applyClassifierSend(that_key,my_user)
        if apply_sent != "nothingsend":
            if_sended = "send"

        if "hello" in processed_sentence:
            hello_text = "Hello {}, I am here to provide answers to your health issues, feel free to pose questions about your headache problems and etc."

            name_splitted = my_user.name.split(" ")[0]
            facebookfunctions.sendMessage(hello_text.format(name_splitted),my_user.conversation_user,"en")
            del processed_sentence["hello"]
            greeting = True
            if_sended = "send"

        elif "bye" in processed_sentence:
            facebookfunctions.sendMessage("See you 🖐",my_user.conversation_user,"en")
            del processed_sentence["bye"]
            greeting = True
            if_sended = "send"

        elif "thanks" in processed_sentence:
            facebookfunctions.sendMessage("It makes me happy to be able to satisfy your queries😊!",my_user.conversation_user,"en")
            del processed_sentence["thanks"]
            greeting = True
            if_sended = "send"





        del processed_sentence["classifier_result"]

        symptom_sent = False

        if len(processed_sentence)>0:
            for organ in processed_sentence:
                if len(processed_sentence[organ])>0:
                    for affect in processed_sentence[organ]:
                            symptom_sent = True
                            my_comb_organ_affect = organ + " " + affect
                            if my_comb_organ_affect in synonym_map:

                                map_result = synonym_map[my_comb_organ_affect]
                                list_of_synonym = map_result.split(" ")

                                addNewSetToUser(my_user,list_of_synonym[0],list_of_synonym[1])
                            else:
                                addNewSetToUser(my_user,organ,affect)
                elif len(diseases) < 1 and len(processed_sentence) < 2:
                    facebookfunctions.sendMessage("I could not understand {} part complaint".format(organ),id,"en")

                    my_sets = SymptomQuestionSet.objects.filter(question_set_body_part_en=organ).all()
                    my_sets_affect = SymptomQuestionSet.objects.filter(question_set_body_affect_en__contains=organ).all()

                    
                    #my_affects = SymptomQuestionSet.objects.filter(question_set_body_affect_en=affect).all()
                    my_raw_list = []

                    for current_set in my_sets:
                        if "single" == current_set.question_set_body_part_en:
                            my_raw_list.append(current_set.question_set_body_affect_en)
                        else:
                            my_raw_list.append(current_set.question_set_body_part_en +  " " +   current_set.question_set_body_affect_en)

                    
                    for possible_affect in my_sets_affect:
                        my_raw_list.append(possible_affect.question_set_body_affect_en)
                        


                    if len(my_raw_list) > 0:
                        facebookfunctions.sendQuickReplyRaw("Select from options",id,my_raw_list)
                    


        elif  my_user.conversation_question_set == None and len(diseases)>0 and symptom_sent == False:#ustten classifier seti eklenebilir

           my_disease = list(diseases.keys())[0]
           my_disease_value = diseases[my_disease]
           sendDisease(my_disease,my_disease_value,my_user)
           disease_send = True


        elif if_sended == "nothingsend" and greeting == False:
            facebookfunctions.sendMessage("I did not understand😟. Could you repeat it using diffrent words?. You can tell me about your health complaints such as 'I have a headache'",my_user.conversation_user,"en")
    if symptom_sent == False and len(diseases) > 0 and disease_send == False:
             my_disease = list(diseases.keys())[0]
             my_disease_value = diseases[my_disease]
             sendDisease(my_disease,my_disease_value,my_user)

        






    #facebookfunctions.sendMessage("Thank you" + str(id),id,"en")