
from .extractor import Extractor
from ..lang.en import constants


class EnglishExtractor(Extractor):

    def whitespace_tokenize(self,sentence):
        return sentence.split(" ")

    def organContains(self,word):
        return word in constants.EnglishConstants.organ

    def affectContains(self,word):
        return word in constants.EnglishConstants.affect

    def singleWordContains(self,word):
        return word in constants.EnglishConstants.single_word_organ_affect

    def preprocess_raw_sentence(self,sentence):



        result_dict = {}

        organ_found = False

        last_organ = None
        affects = []

        sentence_splitted = self.whitespace_tokenize(sentence)

        for i,word in enumerate(sentence_splitted):

            if self.organContains(word):
                organ_found = True
                last_organ = word
                result_dict[last_organ] = {}
                for affect in affects:
                    result_dict[last_organ][affect] = {}
                affects = []

            if self.affectContains(word) and organ_found:
                result_dict[last_organ][word] = {}


            elif self.affectContains(word):
                affects.append(word)

            if self.singleWordContains(word):
                if "single" not in result_dict:
                    result_dict["single"] = {}
                result_dict["single"][word] = {}

        return result_dict




    def checkIfOrganIn(self):
        pass

    def checkIfAffectIn(self):
        pass
