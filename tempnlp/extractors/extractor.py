from abc import ABC, abstractmethod


class Extractor(ABC):
    @abstractmethod
    def checkIfOrganIn(self):
        pass

    @abstractmethod
    def checkIfAffectIn(self):
        pass

    @abstractmethod
    def preprocess_raw_sentence(self,sentence):
        pass

