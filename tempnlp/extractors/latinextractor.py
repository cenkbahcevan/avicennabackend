
from lang.sp import constants
from stemmers.mapstemmer import MapStemmer
from lang.sp.spanishlookupmap import LOOKUP
from .extractor import Extractor



class LatinExtractor(Extractor):



    def checkIfOrganIn(self,word,raw_word):
         return self.my_sp_constants.organContains(word) or self.my_sp_constants.organContains(raw_word)

    def checkIfAffectIn(self,word,raw_word):
         return self.my_sp_constants.affectContains(word) or self.my_sp_constants.affectContains(raw_word)

    def __init__(self,lang='sp',organFirst=False):


        self.my_sp_constants = constants.SpanishConstants()
        self.my_stemmer = MapStemmer(LOOKUP)

    def preprocess_raw_sentence(self,sentence):

        result_dict = {}


        stemmed_sentence = self.my_stemmer.stemSentence(sentence)

        tokenized_sentence = self.my_stemmer.white_space_tokenize(stemmed_sentence)

        tokenized_raw_sentence = self.my_stemmer.white_space_tokenize(sentence)

        print(tokenized_sentence)

        organ_found = False
        affect_found = False

        last_organ = None
        last_affect = None

        affects = []




        for i in range(0,len(tokenized_sentence)):

            raw_word = tokenized_raw_sentence[i]
            word = tokenized_sentence[i]

            if self.checkIfOrganIn(word,raw_word) and organ_found==False:
                print("Organ:{}".format(word))
                if word not in result_dict:
                    result_dict[word] = {}
                    last_organ = word
                    organ_found = True
                    for affect in affects:
                        result_dict[word][affect] = {}


            elif self.checkIfAffectIn(word,raw_word) and organ_found:
                print("Affect:{}".format(word))
                result_dict[last_organ][word] = {}

            elif self.checkIfAffectIn(word,raw_word):
                affects.append(word)
                affect_found = True

        #"Synonym organlari degistirdik"
        for key in list(result_dict.keys()):
            if key in constants.SpanishConstants.synonym_map:
                result_dict[constants.SpanishConstants.synonym_map[key]] = result_dict[key]
                del result_dict[key]

        for key in list(result_dict.keys()):
            for affect in list(result_dict[key].keys()):
                if affect in constants.SpanishConstants.synonym_map:
                    result_dict[key][constants.SpanishConstants.synonym_map[affect]] = result_dict[key][affect]
                    del result_dict[key][affect]

        return result_dict








