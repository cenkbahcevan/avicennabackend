



class SpanishConstants:

    synonym_map = {
        "estómago":"abdomen",
        "doler":"dolor",
        "hinchazon":"hinchar"
    }



    organs = [

        "cabeza",#bas
        "abdomen",#karın
        "ojo",#goz
        "nariz",#burun
        "mano",#el
        "pie",#ayak
        "dedo",#parmak
        "pelo",#saç
        "pene",#penis
        "pierna",#bacak
        "aliento",#nefes
        "pecho",#chest
        "espalda",#sırt
        "estomagar",#mide karın
        "eyaculación",#erken
        "boca",#ağız
        "garganta"#bogaz,
        "cintura",#bel
        "cuello",#boyun,
        "brazo" #kol
    ]
    affects = [

        "doler",#agri
        "dolor",#agri
        "arder",#yanma
        "fluir",#akıyor
        "sangrar",#kan
        "hinchado",#sisme
        "hinchazon",#sislik,
        "hinchazón",#ispanyolca hlai
        "prurito",#
        "precoz",#bosalma

    ]

    single_affects = [
        "mareo",#bas donmesi
        "debilidad",#halsizlik
        "toser"#"öksürük"
    ]

    multiValueOrgans = [

    ]

    dateValues = [

    ]

    def organContains(self,word):
        return word in self.organs

    def affectContains(self,word):
        return word in self.affects

    def multiValueOrgansContain(self,word1,word2):
        return word1 + " " + word2 in self.multiValueOrgans
    
        

